/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.domain.enumerate;

import com.iwindplus.boot.web.exception.CommonException;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 业务编码返回值枚举.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
@Getter
@Accessors(fluent = true)
public enum WechatCodeEnum implements CommonException {
    /**
     * 频率限制.
     */
    FREQUENCY_LIMIT("frequency_limit", "频率限制"),

    /**
     * page路径不存在.
     */
    PAGE_NOT_EXIST("page_not_exist", "page路径不存在"),

    /**
     * code只能使用一次.
     */
    CODE_USERD("code_used", "code只能使用一次"),

    /**
     * 无效code.
     */
    INVALID_CODE("invalid_code", "无效code"),

    /**
     * 用户信息不完整.
     */
    USER_INFO_INCOMPLETE("user_info_incomplete", "用户信息不完整"),
    ;

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 构造方法.
     *
     * @param value 值
     * @param desc  描述
     */
    WechatCodeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return WechatCodeEnum
     */
    public static WechatCodeEnum valueOfDesc(String desc) {
        for (WechatCodeEnum val : WechatCodeEnum.values()) {
            if (Objects.equals(desc, val.desc())) {
                return val;
            }
        }
        return null;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return WechatCodeEnum
     */
    public static WechatCodeEnum valueOfValue(String value) {
        for (WechatCodeEnum val : WechatCodeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.value;
    }

    @Override
    public String getMessage() {
        return this.desc;
    }
}
