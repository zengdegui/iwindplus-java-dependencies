/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service.impl;

import com.iwindplus.boot.wechat.domain.WechatOpenProperty;
import com.iwindplus.boot.wechat.service.WechatOpenService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.open.api.impl.WxOpenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 微信公众号相关业务层接口实现类.
 *
 * @author zengdegui
 * @since 2020/4/10
 */
@Slf4j
public class WechatOpenServiceImpl extends WxOpenServiceImpl implements WechatOpenService {
    /**
     * WechatOpenProperty.
     */
    @Autowired
    protected WechatOpenProperty wechatOpenProperty;

    @Override
    public WechatOpenProperty getConfig() {
        return this.wechatOpenProperty;
    }
}
