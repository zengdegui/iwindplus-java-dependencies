/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.iwindplus.boot.wechat.domain.WechatMaProperty;
import com.iwindplus.boot.wechat.domain.WechatMpProperty;
import com.iwindplus.boot.wechat.domain.dto.WechatMaQrCodeDTO;
import com.iwindplus.boot.wechat.domain.vo.WechatMaPhoneNumberInfoVO;
import com.iwindplus.boot.wechat.domain.vo.WechatMaUserInfoVO;

/**
 * 微信小程序相关业务层接口类.
 *
 * @author zengdegui
 * @since 2019/10/10
 */
public interface WechatMaService extends WxMaService {
    /**
     * 获取配置.
     *
     * @return WechatMaProperty
     */
    WechatMaProperty getConfig();

    /**
     * 获取小程序二维码字节码.
     *
     * @param entity 对象
     * @return byte[]
     */
    byte[] getQrCode(WechatMaQrCodeDTO entity);

    /**
     * 获取手机号信息.
     *
     * @param code          code码
     * @param encryptedData 加密用户数据
     * @param iv            加密算法的初始向量
     * @return WxMaPhoneNumberInfo
     */
    WechatMaPhoneNumberInfoVO getPhoneNumberInfo(String code, String encryptedData, String iv);

    /**
     * 获取用户信息.
     *
     * @param code          code码
     * @param encryptedData 加密用户数据
     * @param iv            加密算法的初始向量
     * @param signature     用户信息签名
     * @param rawData       用户原始数据字符串
     * @return WxMaPhoneNumberInfo
     */
    WechatMaUserInfoVO getUserInfo(String code, String encryptedData, String iv, String signature, String rawData);
}
