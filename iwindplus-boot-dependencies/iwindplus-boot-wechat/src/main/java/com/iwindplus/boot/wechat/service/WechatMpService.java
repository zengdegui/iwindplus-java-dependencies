/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service;

import com.iwindplus.boot.wechat.domain.WechatMpProperty;
import com.iwindplus.boot.wechat.domain.dto.WechatMpQrCodeDTO;
import me.chanjar.weixin.mp.api.WxMpService;

/**
 * 微信公众号相关业务层接口类.
 *
 * @author zengdegui
 * @since 2020/4/10
 */
public interface WechatMpService extends WxMpService {
    /**
     * 获取配置.
     *
     * @return WechatMpProperty
     */
    WechatMpProperty getConfig();

    /**
     * 获取微信公众号二维码.
     *
     * @param entity 对象
     * @return String
     */
    String getQrCode(WechatMpQrCodeDTO entity);
}
