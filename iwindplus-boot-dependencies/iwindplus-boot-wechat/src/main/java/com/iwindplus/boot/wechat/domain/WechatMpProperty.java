/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信公众号相关属性.
 *
 * @author zengdegui
 * @since 2018/10/10
 */
@Data
@ConfigurationProperties(prefix = "wechat.mp", ignoreUnknownFields = true)
public class WechatMpProperty {
    /**
     * 设置微信公众号的appid.
     */
    private String appId;

    /**
     * 设置微信公众号的Secret.
     */
    private String secret;

    /**
     * 设置微信公众号的token.
     */
    private String token;

    /**
     * 设置微信公众号的EncodingAESKey.
     */
    private String aesKey;
}
