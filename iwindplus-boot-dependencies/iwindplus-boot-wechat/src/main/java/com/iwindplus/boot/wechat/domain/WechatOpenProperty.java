/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信开放平台相关属性.
 *
 * @author zengdegui
 * @since 2018/10/10
 */
@Data
@ConfigurationProperties(prefix = "wechat.open", ignoreUnknownFields = true)
public class WechatOpenProperty {
    /**
     * 设置微信开放平台的appid.
     */
    private String appId;

    /**
     * 设置微信开放平台的Secret.
     */
    private String secret;

    /**
     * 设置微信开放平台的token.
     */
    private String token;

    /**
     * 设置微信开放平台的EncodingAESKey.
     */
    private String aesKey;
}
