/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service;

import com.iwindplus.boot.wechat.domain.WechatOpenProperty;
import me.chanjar.weixin.open.api.WxOpenService;

/**
 * 微信开放平台相关业务层接口类.
 *
 * @author zengdegui
 * @since 2020/4/10
 */
public interface WechatOpenService extends WxOpenService {
    /**
     * 获取配置.
     *
     * @return WechatOpenProperty
     */
    WechatOpenProperty getConfig();
}
