/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.domain.vo;

import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import lombok.Data;

/**
 * 微信小程序手机信息视图对象.
 *
 * @author zengdegui
 * @since 2019/10/10
 */
@Data
public class WechatMaPhoneNumberInfoVO extends WxMaPhoneNumberInfo {
    private static final long serialVersionUID = 7088480415033756556L;

    /**
     * 用户session key.
     */
    private String sessionKey;

    /**
     * 用户唯一标识.
     */
    private String openid;

    /**
     * 用户在开放平台的唯一标识符.
     */
    private String unionid;
}
