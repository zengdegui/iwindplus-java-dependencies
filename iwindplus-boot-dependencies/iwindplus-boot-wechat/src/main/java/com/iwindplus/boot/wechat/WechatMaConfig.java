/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat;

import cn.binarywang.wx.miniapp.config.impl.WxMaRedisBetterConfigImpl;
import com.iwindplus.boot.wechat.domain.WechatMaProperty;
import com.iwindplus.boot.wechat.domain.constant.WechatConstant;
import com.iwindplus.boot.wechat.service.WechatMaService;
import com.iwindplus.boot.wechat.service.impl.WechatMaServiceImpl;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.redis.RedisTemplateWxRedisOps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Objects;

/**
 * 微信小程序配置管理.
 *
 * @author zengdegui
 * @since 2019/7/16
 */
@Slf4j
public class WechatMaConfig {
	@Autowired
	private WechatMaProperty wechatMaProperty;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	/**
	 * 创建 WechatMaService.
	 *
	 * @return WechatMaService
	 */
	@Bean
	public WechatMaService wechatMaService() {
		WechatMaServiceImpl wechatMaService = new WechatMaServiceImpl();
		if (Objects.nonNull(this.wechatMaProperty)) {
			if (StringUtils.isNotBlank(this.wechatMaProperty.getAppId())
					&& StringUtils.isNotBlank(this.wechatMaProperty.getSecret())) {
				RedisTemplateWxRedisOps wxRedisOps = new RedisTemplateWxRedisOps(this.stringRedisTemplate);
				WxMaRedisBetterConfigImpl config = new WxMaRedisBetterConfigImpl(wxRedisOps, WechatConstant.WECHAT_MA_PREFIX);
				config.setAppid(this.wechatMaProperty.getAppId());
				config.setSecret(this.wechatMaProperty.getSecret());
				config.setToken(this.wechatMaProperty.getToken());
				config.setAesKey(this.wechatMaProperty.getAesKey());
				config.setMsgDataFormat(this.wechatMaProperty.getMsgDataFormat());
				wechatMaService.setWxMaConfig(config);
			}
		}
		log.info("WechatMaService [{}]", wechatMaService);
		return wechatMaService;
	}
}