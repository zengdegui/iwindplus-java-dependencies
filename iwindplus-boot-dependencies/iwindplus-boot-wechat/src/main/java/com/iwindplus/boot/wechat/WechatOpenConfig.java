/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat;

import com.iwindplus.boot.wechat.domain.WechatOpenProperty;
import com.iwindplus.boot.wechat.domain.constant.WechatConstant;
import com.iwindplus.boot.wechat.service.WechatOpenService;
import com.iwindplus.boot.wechat.service.impl.WechatOpenServiceImpl;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.redis.RedisTemplateWxRedisOps;
import me.chanjar.weixin.open.api.impl.WxOpenInRedisTemplateConfigStorage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Objects;

/**
 * 微信开放平台配置管理.
 *
 * @author zengdegui
 * @since 2020/4/10
 */
@Slf4j
public class WechatOpenConfig {
    @Autowired
    private WechatOpenProperty wechatOpenProperty;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 创建 WechatOpenService.
     *
     * @return WechatOpenService
     */
    @Bean
    public WechatOpenService wechatOpenService() {
        WechatOpenServiceImpl wechatOpenService = new WechatOpenServiceImpl();
        if (Objects.nonNull(this.wechatOpenProperty)) {
            if (StringUtils.isNotBlank(this.wechatOpenProperty.getAppId())
                    && StringUtils.isNotBlank(this.wechatOpenProperty.getSecret())) {
                RedisTemplateWxRedisOps wxRedisOps = new RedisTemplateWxRedisOps(this.stringRedisTemplate);
                WxOpenInRedisTemplateConfigStorage config = new WxOpenInRedisTemplateConfigStorage(wxRedisOps, WechatConstant.WECHAT_OPEN_PREFIX);
                config.setComponentAppId(this.wechatOpenProperty.getAppId());
                config.setComponentAppSecret(this.wechatOpenProperty.getSecret());
                config.setComponentToken(this.wechatOpenProperty.getToken());
                config.setComponentAesKey(this.wechatOpenProperty.getAesKey());
                wechatOpenService.setWxOpenConfigStorage(config);
            }
        }
        log.info("WechatOpenService [{}]", wechatOpenService);
        return wechatOpenService;
    }
}
