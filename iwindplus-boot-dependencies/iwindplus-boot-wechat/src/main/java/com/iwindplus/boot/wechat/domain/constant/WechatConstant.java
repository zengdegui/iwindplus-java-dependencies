/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2018/12/27
 */
public class WechatConstant {
    /**
     * 微信开放平台redis存储前缀.
     */
    public static final String WECHAT_OPEN_PREFIX = "wechat_open";

    /**
     * 微信公众号redis存储前缀.
     */
    public static final String WECHAT_MP_PREFIX = "wechat_mp";

    /**
     * 微信小程序redis存储前缀.
     */
    public static final String WECHAT_MA_PREFIX = "wechat_ma";
}
