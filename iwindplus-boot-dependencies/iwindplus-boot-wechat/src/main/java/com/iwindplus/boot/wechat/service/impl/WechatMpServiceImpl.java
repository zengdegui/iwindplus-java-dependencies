/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service.impl;

import com.iwindplus.boot.web.exception.BaseException;
import com.iwindplus.boot.wechat.domain.WechatMpProperty;
import com.iwindplus.boot.wechat.domain.dto.WechatMpQrCodeDTO;
import com.iwindplus.boot.wechat.domain.enumerate.WechatCodeEnum;
import com.iwindplus.boot.wechat.service.WechatMpService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpQrcodeService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * 微信公众号相关业务层接口实现类.
 *
 * @author zengdegui
 * @since 2020/4/10
 */
@Slf4j
public class WechatMpServiceImpl extends WxMpServiceImpl implements WechatMpService {
    /**
     * WechatMpProperty.
     */
    @Autowired
    protected WechatMpProperty wechatMpProperty;

    @Override
    public WechatMpProperty getConfig() {
        return this.wechatMpProperty;
    }

    @Override
    public String getQrCode(WechatMpQrCodeDTO entity) {
        try {
            WxMpQrcodeService qrcodeService = this.getQrcodeService();
            WxMpQrCodeTicket data = qrcodeService.qrCodeCreateLastTicket(entity.getScene());
            if (Objects.nonNull(data)) {
                return qrcodeService.qrCodePictureUrl(data.getTicket());
            }
        } catch (WxErrorException ex) {
            int errorCode = ex.getError().getErrorCode();
            String errorMsg = ex.getError().getErrorMsg();
            log.error("WxErrorException [code:{}, messsage:{}]", errorCode, errorMsg);
            if (errorCode == 45009) {
                throw new BaseException(WechatCodeEnum.FREQUENCY_LIMIT);
            } else if (errorCode == 41030) {
                throw new BaseException(WechatCodeEnum.PAGE_NOT_EXIST);
            }
        }
        return null;
    }
}
