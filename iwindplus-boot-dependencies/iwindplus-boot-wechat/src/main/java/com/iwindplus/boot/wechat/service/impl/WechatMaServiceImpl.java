/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.wechat.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.iwindplus.boot.util.BeanCopierUtil;
import com.iwindplus.boot.web.domain.enumerate.WebCodeEnum;
import com.iwindplus.boot.web.exception.BaseException;
import com.iwindplus.boot.wechat.domain.WechatMaProperty;
import com.iwindplus.boot.wechat.domain.dto.WechatMaQrCodeDTO;
import com.iwindplus.boot.wechat.domain.enumerate.WechatCodeEnum;
import com.iwindplus.boot.wechat.domain.vo.WechatMaPhoneNumberInfoVO;
import com.iwindplus.boot.wechat.domain.vo.WechatMaUserInfoVO;
import com.iwindplus.boot.wechat.service.WechatMaService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * 微信相关业务接口实现类.
 *
 * @author zengdegui
 * @since 2019/10/10
 */
@Slf4j
public class WechatMaServiceImpl extends WxMaServiceImpl implements WechatMaService {
    /**
     * WechatMaProperty.
     */
    @Autowired
    protected WechatMaProperty wechatMaProperty;

    @Override
    public WechatMaProperty getConfig() {
        return this.wechatMaProperty;
    }

    @Override
    public byte[] getQrCode(WechatMaQrCodeDTO entity) {
        try {
            byte[] data = this.getQrcodeService().createWxaCodeUnlimitBytes(entity.getScene(),
                    entity.getPage(), entity.getWidth(), entity.isAutoColor(), entity.getLineColor(),
                    entity.isHyaline());
            return data;
        } catch (WxErrorException ex) {
            int errorCode = ex.getError().getErrorCode();
            String errorMsg = ex.getError().getErrorMsg();
            log.error("Get QR code of small program [code:{}, message:{}]", errorCode, errorMsg);
            if (errorCode == 45009) {
                throw new BaseException(WechatCodeEnum.FREQUENCY_LIMIT);
            } else if (errorCode == 41030) {
                throw new BaseException(WechatCodeEnum.PAGE_NOT_EXIST);
            }
        }
        return null;
    }

    @Override
    public WechatMaPhoneNumberInfoVO getPhoneNumberInfo(String code, String encryptedData, String iv) {
        WxMaJscode2SessionResult sessionInfo = this.getSessionInfo(code);
        if (Objects.nonNull(sessionInfo)) {
            // 解密用户手机号信息
            WxMaPhoneNumberInfo phoneNoInfo = this.getUserService().getPhoneNoInfo(sessionInfo.getSessionKey(), encryptedData, iv);
            if (Objects.nonNull(phoneNoInfo)) {
                WechatMaPhoneNumberInfoVO data = BeanCopierUtil.copy(phoneNoInfo, WechatMaPhoneNumberInfoVO.class);
                data.setSessionKey(sessionInfo.getSessionKey());
                data.setOpenid(sessionInfo.getOpenid());
                data.setUnionid(sessionInfo.getUnionid());
                return data;
            }
        }
        return null;
    }

    @Override
    public WechatMaUserInfoVO getUserInfo(String code, String encryptedData, String iv, String signature, String rawData) {
        WxMaJscode2SessionResult sessionInfo = this.getSessionInfo(code);
        if (Objects.nonNull(sessionInfo)) {
            boolean checkUserInfo = this.getUserService().checkUserInfo(sessionInfo.getSessionKey(), rawData, signature);
            if (!checkUserInfo) {
                throw new BaseException(WechatCodeEnum.USER_INFO_INCOMPLETE);
            }
            // 解密用户敏感数据
            WxMaUserInfo userInfo = this.getUserService().getUserInfo(sessionInfo.getSessionKey(), encryptedData, iv);
            if (Objects.nonNull(userInfo)) {
                WechatMaUserInfoVO data = BeanCopierUtil.copy(userInfo, WechatMaUserInfoVO.class);
                data.setSessionKey(sessionInfo.getSessionKey());
                data.setOpenid(sessionInfo.getOpenid());
                data.setUnionid(sessionInfo.getUnionid());
                return data;
            }
        }
        return null;
    }

    /**
     * 获取openid等.
     *
     * @param code code码
     * @return WxMaJscode2SessionResult
     */
    private WxMaJscode2SessionResult getSessionInfo(String code) {
        WxMaUserService wxMaUserService = this.getUserService();
        try {
            WxMaJscode2SessionResult sessionInfo = wxMaUserService.getSessionInfo(code);
            return sessionInfo;
        } catch (WxErrorException ex) {
            log.error("Wechat authorization exception [{}]", ex.getMessage());
            int errorCode = ex.getError().getErrorCode();
            if (errorCode == 40029) {
                throw new BaseException(WechatCodeEnum.INVALID_CODE);
            } else if (errorCode == 45011) {
                throw new BaseException(WechatCodeEnum.FREQUENCY_LIMIT);
            } else if (errorCode == 40163) {
                throw new BaseException(WechatCodeEnum.CODE_USERD);
            } else if (errorCode == -1) {
                throw new BaseException(WebCodeEnum.FAILED);
            }
        }
        return null;
    }
}
