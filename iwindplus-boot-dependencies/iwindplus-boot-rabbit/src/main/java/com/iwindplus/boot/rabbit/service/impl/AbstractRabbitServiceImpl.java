/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.rabbit.service.impl;

import com.iwindplus.boot.rabbit.service.RabbitService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;

import java.io.IOException;

/**
 * mq客户端抽象类.
 *
 * @author zengdegui
 * @since 2021/9/18
 */
@Slf4j
public abstract class AbstractRabbitServiceImpl implements RabbitService {
    /**
     * 停止标记.
     */
    private volatile boolean end = false;

    /**
     * 容器.
     */
    private SimpleMessageListenerContainer container;

    /**
     * 是否自动应答.
     */
    private boolean autoAck;

    @Override
    public void setContainer(SimpleMessageListenerContainer container) {
        this.container = container;
        this.autoAck = container.getAcknowledgeMode().isAutoAck();
    }

    @Override
    public void shutdown() {
        this.end = true;
    }

    protected void autoAck(Message message, Channel channel, boolean success) throws IOException {
        if (this.autoAck) {
            return;
        }
        if (success) {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } else {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        }
    }

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        try {
            autoAck(message, channel, process(message, channel));
        } catch (Exception e) {
            autoAck(message, channel, false);
            throw e;
        } finally {
            if (this.end) {
                this.container.stop();
            }
        }
    }

    /**
     * 消息处理.
     *
     * @param message 消息
     * @param channel 通道
     * @return boolean
     */
    public abstract boolean process(Message message, Channel channel);
}
