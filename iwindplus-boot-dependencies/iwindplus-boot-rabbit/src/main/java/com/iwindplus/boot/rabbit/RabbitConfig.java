/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMq配置。
 *
 * @author zengdegui
 * @since 2020/4/28
 */
@Slf4j
@Configuration
public class RabbitConfig {
	@Autowired
	private ConnectionFactory connectionFactory;

	/**
	 * 如果需要在生产者需要消息发送后的回调，
	 * 需要对rabbitTemplate设置ConfirmCallback对象，
	 * 由于不同的生产者需要对应不同的ConfirmCallback，
	 * 如果rabbitTemplate设置为单例bean，
	 * 则所有的rabbitTemplate实际的ConfirmCallback为最后一次申明的ConfirmCallback。
	 *
	 * @return RabbitTemplate
	 */
	@Bean
	@ConditionalOnMissingBean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate rabbitTemplate = new RabbitTemplate(this.connectionFactory);
		rabbitTemplate.setMandatory(true);
		rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
		log.info("RabbitTemplate [{}]", rabbitTemplate);
		return rabbitTemplate;
	}

	/**
	 * RabbitAdmin .
	 *
	 * @return RabbitAdmin
	 */
	@Bean
	@ConditionalOnMissingBean
	public RabbitAdmin rabbitAdmin() {
		RabbitAdmin rabbitAdmin = new RabbitAdmin(this.connectionFactory);
		log.info("RabbitAdmin [{}]", rabbitAdmin);
		return rabbitAdmin;
	}
}
