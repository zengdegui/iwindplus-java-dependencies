/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.rabbit.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2020/10/17
 */
public class RabbitConstant {
	/**
	 * 延时队列前缀.
	 */
	public static final String DELAY_QUEUE_PREFIX = "delay_queue_";

	/**
	 * 延时消费的exchange.
	 */
	public static final String DELAY_EXCHANGE_PREFIX = "delay_exchange_";

	/**
	 * 延时消费的routing-key名称.
	 */
	public static final String DELAY_ROUTING_KEY_PREFIX = "delay_routingkey_";
}
