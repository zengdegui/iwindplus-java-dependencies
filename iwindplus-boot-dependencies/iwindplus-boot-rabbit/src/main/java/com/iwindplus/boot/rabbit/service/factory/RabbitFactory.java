/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.rabbit.service.factory;

import com.iwindplus.boot.rabbit.service.RabbitService;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.FactoryBean;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 动态客户端容器工厂.
 *
 * @author zengdegui
 * @since 2021/9/18
 */
@Data
@Builder
public class RabbitFactory implements FactoryBean<SimpleMessageListenerContainer> {
    /**
     * direct消息交换机.
     */
    private String directExchange;

    /**
     * topic消息交换机.
     */
    private String topicExchange;

    /**
     * fanout消息交换机.
     */
    private String fanoutExchange;

    /**
     * delay消息交换机.
     */
    private String delayExchange;

    /**
     * 队列名称.
     */
    private String queue;

    /**
     * 路由key.
     */
    private String routingKey;

    /**
     * 是否自动删除队列.
     */
    private Boolean autoDeleted;

    /**
     * 队列是否持久化.
     */
    private Boolean durable;

    /**
     * 是否自动应答.
     */
    private Boolean autoAck;

    /**
     * 连接工厂.
     */
    private ConnectionFactory connectionFactory;

    /**
     * RabbitAdmin.
     */
    private RabbitAdmin rabbitAdmin;

    /**
     * 并发消费者数量.
     */
    private Integer concurrentNum;

    /**
     * 客户端监听.
     */
    private RabbitService consumer;

    private Exchange buildExchange() {
        if (StringUtils.isNotBlank(this.directExchange)) {
            return new DirectExchange(this.directExchange);
        } else if (StringUtils.isNotBlank(this.topicExchange)) {
            return new TopicExchange(this.topicExchange);
        } else if (StringUtils.isNotBlank(this.fanoutExchange)) {
            return new FanoutExchange(this.fanoutExchange);
        } else if (StringUtils.isNotBlank(this.delayExchange)) {
            Map<String, Object> args = new HashMap<>(16);
            args.put("x-delayed-type", "direct");
            return new CustomExchange(this.delayExchange, "x-delayed-message", true, false, args);
        }
        return null;
    }

    private Queue buildQueue() {
        return new Queue(this.queue, Objects.nonNull(this.durable) ? this.durable : false, false,
                Objects.nonNull(this.autoDeleted) ? this.autoDeleted : false);
    }

    private Binding bind(Queue queue, Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(this.routingKey).noargs();
    }

    @Override
    public SimpleMessageListenerContainer getObject() throws Exception {
        Queue queue = buildQueue();
        Exchange exchange = buildExchange();
        Binding binding = bind(queue, exchange);

        this.rabbitAdmin.declareQueue(queue);
        this.rabbitAdmin.declareExchange(exchange);
        this.rabbitAdmin.declareBinding(binding);

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setAmqpAdmin(this.rabbitAdmin);
        container.setConnectionFactory(this.connectionFactory);
        container.setQueues(queue);
        container.setPrefetchCount(20);
        container.setConcurrentConsumers(Objects.nonNull(this.concurrentNum) ? this.concurrentNum : 1);
        container.setAcknowledgeMode(this.autoAck ? AcknowledgeMode.AUTO : AcknowledgeMode.MANUAL);
        if (Objects.nonNull(this.consumer)) {
            container.setMessageListener(this.consumer);
        }
        return container;
    }

    @Override
    public Class<?> getObjectType() {
        return SimpleMessageListenerContainer.class;
    }
}
