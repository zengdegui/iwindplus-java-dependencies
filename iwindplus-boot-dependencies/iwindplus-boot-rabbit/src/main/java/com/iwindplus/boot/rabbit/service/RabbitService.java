/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.rabbit.service;

import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

/**
 * 动态客户端.
 *
 * @author zengdegui
 * @since 2021/9/18
 */
public interface RabbitService extends ChannelAwareMessageListener {
	/**
	 * 添加容器.
	 *
	 * @param container 容器
	 */
	void setContainer(SimpleMessageListenerContainer container);

	/**
	 * 关闭.
	 */
	default void shutdown() {
	}
}
