rabbit模块

一、对接流程
   1、在配置文件（yml,properties）中配置rabbitmq相关属性
   2、程序中调用RabbitFactory方法去构建 
   3、程序中调用rabbitTemplate发送mq消息
   
示例：
String routingKey = new StringBuilder(RabbitConstant.DELAY_ROUTING_KEY_PREFIX).append(entity.getDelayTime()).toString();
String queueName = new StringBuilder(RabbitConstant.DELAY_QUEUE_PREFIX).append(entity.getDelayTime()).toString();
String exchangeName = new StringBuilder(RabbitConstant.DELAY_EXCHANGE_PREFIX).append(entity.getDelayTime()).toString();
RabbitFactory fac =
        RabbitFactory.builder().delayExchange(exchangeName).queue(queueName).autoDeleted(false)
                .autoAck(true).durable(true).routingKey(routingKey).rabbitAdmin(this.rabbitAdmin)
                .connectionFactory(this.connectionFactory)
                .consumer(new AbstractRabbitServiceImpl() {
                    @Override
                    public boolean process(Message message, Channel channel) {
                        distributionConsumerMsg(message, channel);
                        return true;
                    }
                }).build();
SimpleMessageListenerContainer container = fac.getObject();
container.start();
MessagePostProcessor messagePostProcessor = msg -> {
    MessageProperties messageProperties = msg.getMessageProperties();
    if (Objects.nonNull(entity.getDelayTime())) {
        // 设置延时时间
        messageProperties.setDelay(entity.getDelayTime() * 1000);
    }
    return msg;
};
CorrelationData correlationData = new CorrelationData(entity.getLogId());
this.rabbitTemplate.convertAndSend(exchangeName,
        routingKey, entity, messagePostProcessor, correlationData);
