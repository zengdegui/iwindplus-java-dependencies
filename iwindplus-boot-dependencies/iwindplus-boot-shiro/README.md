shiro权限模块
ReloadPermissionManager实现了ShiroFilterFactoryBean权限过滤链热加载，当更新了数据，调用该类方法updatePermission()进行重载
一、有session对接
   1、程序中引用EnableShiroSession开启功能
   2、在配置文件（yml,properties）中配置ShiroSessionProperty类中相关属性
   3、程序中实现ShiroService
   4、登录接口中加入以下代码:
   Subject subject = ShiroUtil.getSubject();
   ShiroSessionTokenDTO usernamePasswordToken = new ShiroSessionTokenDTO(username, password, rememberMe);
   subject.login(usernamePasswordToken);
   5、程序中获取用户信息，调用ShiroUtil相关方法
   6、退出，subject.logout();
一、无session对接
   1、程序中引用@EnableShiro
   2、在配置文件（yml,properties）中配置ShiroProperty类中相关属性
   3、程序中实现ShiroService
   4、登录接口中注入ShiroRealm，并调用getAccessTokenByUsername方法生成访问token
   5、在需要登录的接口中请求头加入ShiroConstant.AUTHORIZATION参数
   6、程序中获取用户信息，调用ShiroUtil相关方法
   7、访问token失效后刷新token，注入ShiroRealm，并调用getAccessTokenByRefreshToken方法生成访问token
   8、退出，注入ShiroRealm，并调用logout方法
