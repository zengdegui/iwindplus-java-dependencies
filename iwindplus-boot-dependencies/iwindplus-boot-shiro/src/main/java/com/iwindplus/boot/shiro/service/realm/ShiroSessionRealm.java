/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.service.realm;

import com.iwindplus.boot.shiro.domain.dto.ShiroSessionTokenDTO;
import com.iwindplus.boot.shiro.domain.enumerate.ShiroCodeEnum;
import com.iwindplus.boot.shiro.domain.vo.LoginVO;
import com.iwindplus.boot.shiro.domain.vo.PermissionVO;
import com.iwindplus.boot.shiro.service.ShiroService;
import com.iwindplus.boot.web.exception.BaseShiroAuthcException;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Objects;
import java.util.Set;

/**
 * 有状态方式relam（用户密码方式）.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Setter
public class ShiroSessionRealm extends AuthorizingRealm {
    private ShiroService shiroService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof ShiroSessionTokenDTO;
    }

    /**
     * 认证.
     *
     * @param authcToken token
     * @return AuthenticationInfo
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
            throws AuthenticationException {
        ShiroSessionTokenDTO param = (ShiroSessionTokenDTO) authcToken;
        String username = param.getUsername();
        // 检查账号.
        LoginVO data = this.shiroService.getByUsername(username);
        // 没找到帐号.
        if (Objects.isNull(data)) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_NOT_EXIST);
        }
        Integer status = data.getStatus();
        if (0 == status) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_DISABLED);
        } else if (2 == status) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_LOCKED);
        }
        // 放入shiro.调用CredentialsMatcher检验.
        return new SimpleAuthenticationInfo(data, data.getPassword(), this.getName());
    }

    /**
     * 鉴权.
     *
     * @param principals 用户信息
     * @return AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        LoginVO data = (LoginVO) principals.getPrimaryPrincipal();
        String username = data.getUsername();
        PermissionVO permissionVO = this.shiroService.listOperatePermissionByUsername(username);
        if (Objects.nonNull(permissionVO)) {
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
            Set<String> rolePerms = permissionVO.getRolePerms();
            if (CollectionUtils.isNotEmpty(rolePerms)) {
                // 角色加入AuthorizationInfo认证对象
                info.setRoles(rolePerms);
            }
            Set<String> operatePerms = permissionVO.getOperatePerms();
            if (CollectionUtils.isNotEmpty(operatePerms)) {
                // 权限加入AuthorizationInfo认证对象
                info.setStringPermissions(operatePerms);
            }
            return info;
        }
        return null;
    }

    /**
     * 检验密码.
     *
     * @param token token
     * @param info  认证信息
     * @throws AuthenticationException
     */
    @Override
    protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info)
            throws AuthenticationException {
        CredentialsMatcher cm = getCredentialsMatcher();
        if (!cm.doCredentialsMatch(token, info)) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.PASSWORD_ERROR);
        }
    }

    /**
     * 重写方法,清除当前用户的授权缓存
     *
     * @param principals
     */
    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    /**
     * 重写方法，清除当前用户的认证缓存
     *
     * @param principals
     */
    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    /**
     * 自定义方法：清除所有授权缓存
     */
    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    /**
     * 自定义方法：清除所有认证缓存
     */
    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    /**
     * 自定义方法：清除所有的认证缓存和授权缓存
     */
    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }
}
