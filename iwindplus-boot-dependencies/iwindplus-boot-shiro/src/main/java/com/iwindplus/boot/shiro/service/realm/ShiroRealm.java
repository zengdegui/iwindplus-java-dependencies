/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.service.realm;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.iwindplus.boot.shiro.domain.ShiroProperty;
import com.iwindplus.boot.shiro.domain.constant.ShiroConstant;
import com.iwindplus.boot.shiro.domain.dto.ShiroTokenDTO;
import com.iwindplus.boot.shiro.domain.enumerate.ShiroCodeEnum;
import com.iwindplus.boot.shiro.domain.vo.LoginVO;
import com.iwindplus.boot.shiro.domain.vo.PermissionVO;
import com.iwindplus.boot.shiro.domain.vo.UserDetailVO;
import com.iwindplus.boot.shiro.service.ShiroService;
import com.iwindplus.boot.web.exception.BaseException;
import com.iwindplus.boot.web.exception.BaseShiroAuthcException;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 无状态方式relam.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Setter
public class ShiroRealm extends AuthorizingRealm {
    private ShiroService shiroService;

    private RedisTemplate<String, Object> redisTemplate;

    private ShiroProperty shiroProperty;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof ShiroTokenDTO;
    }

    /**
     * 认证.
     *
     * @param authcToken token
     * @return AuthenticationInfo
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
            throws AuthenticationException {
        ShiroTokenDTO param = (ShiroTokenDTO) authcToken;
        String accessToken = param.getAccessToken();
        String keyPrefix = getKeyPrefix(ShiroConstant.ACCESS_TOKEN_PREFIX);
        String accessTokenKey = new StringBuilder(keyPrefix).append(accessToken).toString();
        Object obj = this.redisTemplate.opsForValue().get(accessTokenKey);
        if (Objects.isNull(obj)) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.INVALID_ACCESS_TOKEN);
        }
        String username = obj.toString();
        // 检查账号.
        LoginVO data = this.shiroService.getByUsername(username);
        // 没找到帐号.
        if (Objects.isNull(data)) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_NOT_EXIST);
        }
        Integer status = data.getStatus();
        if (0 == status) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_DISABLED);
        } else if (2 == status) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.ACCOUNT_LOCKED);
        }
        // 放入shiro.调用CredentialsMatcher检验.
        return new SimpleAuthenticationInfo(data, accessToken, this.getName());
    }

    /**
     * 鉴权.
     *
     * @param principals 用户信息
     * @return AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        LoginVO data = (LoginVO) principals.getPrimaryPrincipal();
        String username = data.getUsername();
        PermissionVO permissionVO = this.shiroService.listOperatePermissionByUsername(username);
        if (Objects.nonNull(permissionVO)) {
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
            Set<String> rolePerms = permissionVO.getRolePerms();
            if (CollectionUtils.isNotEmpty(rolePerms)) {
                // 角色加入AuthorizationInfo认证对象
                info.setRoles(rolePerms);
            }
            Set<String> operatePerms = permissionVO.getOperatePerms();
            if (CollectionUtils.isNotEmpty(operatePerms)) {
                // 权限加入AuthorizationInfo认证对象
                info.setStringPermissions(operatePerms);
            }
            return info;
        }
        return null;
    }

    /**
     * 检验访问token.
     *
     * @param token token
     * @param info  认证信息
     * @throws AuthenticationException
     */
    @Override
    protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info)
            throws AuthenticationException {
        CredentialsMatcher cm = getCredentialsMatcher();
        if (!cm.doCredentialsMatch(token, info)) {
            throw new BaseShiroAuthcException(ShiroCodeEnum.INVALID_ACCESS_TOKEN);
        }
    }

    /**
     * 重写方法,清除当前用户的授权缓存
     *
     * @param principals
     */
    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    /**
     * 重写方法，清除当前用户的认证缓存
     *
     * @param principals
     */
    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    /**
     * 自定义方法：清除所有授权缓存
     */
    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    /**
     * 自定义方法：清除所有认证缓存
     */
    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    /**
     * 自定义方法：清除所有的认证缓存和授权缓存
     */
    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

    /**
     * 通过用户名密码，生成访问token和刷新token.
     *
     * @param username 用户名
     * @param password 密码
     * @return UserDetailVO
     */
    public UserDetailVO getAccessTokenByUsername(String username, String password) {
        LoginVO data = this.shiroService.getByUsername(username);
        // 没找到帐号.
        if (Objects.isNull(data)) {
            throw new BaseException(ShiroCodeEnum.ACCOUNT_NOT_EXIST);
        }
        Integer status = data.getStatus();
        if (0 == status) {
            throw new BaseException(ShiroCodeEnum.ACCOUNT_DISABLED);
        } else if (2 == status) {
            throw new BaseException(ShiroCodeEnum.ACCOUNT_LOCKED);
        }
        if (!StringUtils.equals(data.getPassword(), SecureUtil.md5(password))) {
            throw new BaseException(ShiroCodeEnum.PASSWORD_ERROR);
        }
        UserDetailVO result = getUserDetailVO(data);
        return result;
    }

    /**
     * 通过刷新token，生成访问token和刷新token.
     *
     * @param refreshToken 刷新token
     * @return UserDetailVO
     */
    public UserDetailVO getAccessTokenByRefreshToken(String refreshToken) {
        String keyPrefix = getKeyPrefix(ShiroConstant.REFRESH_TOKEN_PREFIX);
        String refreshTokenKey = new StringBuilder(keyPrefix).append(refreshToken).toString();
        Object obj = this.redisTemplate.opsForValue().get(refreshTokenKey);
        if (Objects.isNull(obj)) {
            throw new BaseException(ShiroCodeEnum.INVALID_REFRESH_TOKEN);
        }
        LoginVO data = (LoginVO) obj;
        UserDetailVO result = getUserDetailVO(data);
        return result;
    }

    /**
     * 退出.
     *
     * @param accessToken 访问token
     */
    public void logout(String accessToken) {
        this.clearAllCache();
        String accessKeyPrefix = getKeyPrefix(ShiroConstant.ACCESS_TOKEN_PREFIX);
        String accessTokenKey = new StringBuilder(accessKeyPrefix).append(accessToken).toString();
        Object obj = this.redisTemplate.opsForValue().get(accessTokenKey);
        if (Objects.nonNull(obj)) {
            // 删除访问token
            this.redisTemplate.delete(accessTokenKey);
            String username = obj.toString();
            String accessUserInfoKey = new StringBuilder(accessKeyPrefix).append(username).toString();
            if (Objects.nonNull(this.redisTemplate.opsForValue().get(accessUserInfoKey))) {
                this.redisTemplate.delete(accessUserInfoKey);
            }
            // 删除刷新token
            String refreshKeyPrefix = getKeyPrefix(ShiroConstant.REFRESH_TOKEN_PREFIX);
            String refreshUserInfoKey = new StringBuilder(refreshKeyPrefix).append(username).toString();
            Object refreshToken = this.redisTemplate.opsForValue().get(refreshUserInfoKey);
            if (Objects.nonNull(refreshToken)) {
                this.redisTemplate.delete(refreshUserInfoKey);
                String refreshTokenStr = refreshToken.toString();
                String refreshTokenKey = new StringBuilder(refreshKeyPrefix).append(refreshTokenStr).toString();
                if (Objects.nonNull(this.redisTemplate.opsForValue().get(refreshTokenKey))) {
                    this.redisTemplate.delete(refreshTokenKey);
                }
            }
        }
    }

    private UserDetailVO getUserDetailVO(LoginVO data) {
        UserDetailVO result = new UserDetailVO();
        result.setOpenid(data.getId());
        result.setUsername(data.getUsername());
        result.setAccessToken(getAccessToken(data));
        result.setRefreshToken(getRefreshToken(data));
        result.setExpiresIn(this.shiroProperty.getAccessTokenExpireTime());
        return result;
    }

    /**
     * 生成访问token.
     *
     * @param data 用户信息
     * @return String
     */
    private String getAccessToken(LoginVO data) {
        String accessToken;
        String username = data.getUsername();
        String keyPrefix = getKeyPrefix(ShiroConstant.ACCESS_TOKEN_PREFIX);
        String accessUserInfoKey = new StringBuilder(keyPrefix).append(username).toString();
        Object obj = this.redisTemplate.opsForValue().get(accessUserInfoKey);
        if (Objects.isNull(obj)) {
            // 生成访问token
            accessToken = IdUtil.objectId();
            String accessTokenKey = new StringBuilder(keyPrefix).append(accessToken).toString();
            this.redisTemplate.opsForValue().set(accessUserInfoKey, accessToken, this.shiroProperty.getAccessTokenExpireTime(), TimeUnit.SECONDS);
            this.redisTemplate.opsForValue().set(accessTokenKey, username, this.shiroProperty.getAccessTokenExpireTime(), TimeUnit.SECONDS);
        } else {
            accessToken = obj.toString();
            String accessTokenKey = new StringBuilder(keyPrefix).append(accessToken).toString();
            // 重新更新访问token过期时间
            this.redisTemplate.expire(accessUserInfoKey, this.shiroProperty.getAccessTokenExpireTime(), TimeUnit.SECONDS);
            this.redisTemplate.expire(accessTokenKey, this.shiroProperty.getAccessTokenExpireTime(), TimeUnit.SECONDS);
        }
        return accessToken;
    }

    /**
     * 生成刷新token.
     *
     * @param data 用户信息
     * @return String
     */
    private String getRefreshToken(LoginVO data) {
        String refreshToken;
        String username = data.getUsername();
        String keyPrefix = getKeyPrefix(ShiroConstant.REFRESH_TOKEN_PREFIX);
        String refreshUserInfoKey = new StringBuilder(keyPrefix).append(username).toString();
        Object obj = this.redisTemplate.opsForValue().get(refreshUserInfoKey);
        if (Objects.isNull(obj)) {
            // 生成刷新token
            refreshToken = IdUtil.objectId();
            String refreshTokenKey = new StringBuilder(keyPrefix).append(refreshToken).toString();
            this.redisTemplate.opsForValue().set(refreshUserInfoKey, refreshToken, this.shiroProperty.getRefreshTokenExpireTime(), TimeUnit.SECONDS);
            this.redisTemplate.opsForValue().set(refreshTokenKey, data, this.shiroProperty.getRefreshTokenExpireTime(), TimeUnit.SECONDS);
        } else {
            refreshToken = obj.toString();
            String refreshTokenKey = new StringBuilder(keyPrefix).append(refreshToken).toString();
            // 重新更新刷新token过期时间
            this.redisTemplate.expire(refreshUserInfoKey, this.shiroProperty.getRefreshTokenExpireTime(), TimeUnit.SECONDS);
            this.redisTemplate.expire(refreshTokenKey, this.shiroProperty.getRefreshTokenExpireTime(), TimeUnit.SECONDS);
        }
        return refreshToken;
    }

    /**
     * 拼接key前缀.
     *
     * @param key key
     * @return String
     */
    private String getKeyPrefix(String key) {
        return new StringBuilder(this.shiroProperty.getCacheKeyPrefix()).append(key).toString();
    }
}