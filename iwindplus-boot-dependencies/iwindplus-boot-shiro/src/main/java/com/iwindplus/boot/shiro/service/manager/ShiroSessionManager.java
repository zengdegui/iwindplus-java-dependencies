/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.service.manager;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.session.mgt.WebSessionKey;

import javax.servlet.ServletRequest;
import java.io.Serializable;
import java.util.Objects;

/**
 * 优化单次请求需要多次访问redis的问题.
 *
 * @author zengdegui
 * @since 2018/9/27
 */
public class ShiroSessionManager extends DefaultWebSessionManager {
    /**
     * 减少多次从redis中读取session.
     *
     * @param sessionKey sessionKey
     * @return Session
     * @throws UnknownSessionException
     */
    @Override
    protected Session retrieveSession(SessionKey sessionKey) throws UnknownSessionException {
        Serializable sessionId = getSessionId(sessionKey);
        ServletRequest request = null;
        Session session = null;
        if (sessionKey instanceof WebSessionKey) {
            request = ((WebSessionKey) sessionKey).getServletRequest();
        }
        if (Objects.nonNull(request) && Objects.nonNull(sessionId)) {
            session = (Session) request.getAttribute(sessionId.toString());
            if (Objects.nonNull(session)) {
                return session;
            }
            session = super.retrieveSession(sessionKey);
            request.setAttribute(sessionId.toString(), session);
        }
        return session;
    }
}
