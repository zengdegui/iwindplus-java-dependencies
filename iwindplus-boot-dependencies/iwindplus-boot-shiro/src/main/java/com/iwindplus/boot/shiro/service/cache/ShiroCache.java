/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.service.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis实现shiro缓存.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public class ShiroCache<K, V> implements Cache<K, V> {
    private RedisTemplate<K, V> redisTemplate;

    /**
     * 名称.
     */
    private String name;

    /**
     * 前缀.
     */
    private String keyPrefix;

    /**
     * 失效时间，单位秒.
     */
    private Long timeout;

    public ShiroCache(String name, RedisTemplate<K, V> redisTemplate, String keyPrefix, Long timeout) {
        super();
        this.name = name;
        this.redisTemplate = redisTemplate;
        this.keyPrefix = keyPrefix;
        this.timeout = timeout;
    }

    @Override
    public void clear() throws CacheException {
        this.redisTemplate.delete(this.keys());
    }

    @Override
    public V get(K key) throws CacheException {
        return this.redisTemplate.opsForValue().get(this.getRedisCacheKey(key));
    }

    @Override
    public Set<K> keys() {
        return this.redisTemplate.keys(this.getRedisCacheKey("*"));
    }

    @Override
    public V put(K key, V value) throws CacheException {
        this.redisTemplate.opsForValue().set(this.getRedisCacheKey(key), value, this.timeout, TimeUnit.SECONDS);
        return value;
    }

    @Override
    public V remove(K key) throws CacheException {
        V old = this.get(key);
        this.redisTemplate.delete(this.getRedisCacheKey(key));
        return old;
    }

    @Override
    public int size() {
        return this.keys().size();
    }

    @Override
    public Collection<V> values() {
        Set<K> set = keys();
        List<V> list = new ArrayList<>(10);
        for (K s : set) {
            list.add(this.get(s));
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private K getRedisCacheKey(Object key) {
        Object redisKey = this.getStringRedisKey(key);
        StringBuilder sb = new StringBuilder();
        sb.append(this.keyPrefix).append(this.name).append(":").append(redisKey);
        return (K) sb.toString();
    }

    private String getStringRedisKey(Object key) {
        String redisKey;
        if (key instanceof PrincipalCollection) {
            redisKey = this.getRedisKeyFromPrincipalCollection((PrincipalCollection) key);
        } else {
            redisKey = key.toString();
        }
        return redisKey;
    }

    private String getRedisKeyFromPrincipalCollection(PrincipalCollection key) {
        List<String> realmNames = this.getRealmNames(key);
        Collections.sort(realmNames);
        String redisKey = this.joinRealmNames(realmNames);
        return redisKey;
    }

    private List<String> getRealmNames(PrincipalCollection key) {
        List<String> realmArr = new ArrayList<>(10);
        Set<String> realmNames = key.getRealmNames();
        Iterator<String> it = realmNames.iterator();
        while (it.hasNext()) {
            String realmName = it.next();
            realmArr.add(realmName);
        }
        return realmArr;
    }

    private String joinRealmNames(List<String> realmArr) {
        StringBuilder redisKeyBuilder = new StringBuilder();
        for (int i = 0; i < realmArr.size(); ++i) {
            String s = realmArr.get(i);
            redisKeyBuilder.append(s);
        }
        String redisKey = redisKeyBuilder.toString();
        return redisKey;
    }

    @Override
    public String toString() {
        return "ShiroCacheDTO{" +
                "redisTemplate=" + redisTemplate +
                ", name='" + name + '\'' +
                ", keyPrefix='" + keyPrefix + '\'' +
                ", timeout=" + timeout +
                '}';
    }
}
