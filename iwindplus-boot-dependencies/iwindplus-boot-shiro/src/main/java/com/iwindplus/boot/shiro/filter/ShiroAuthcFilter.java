/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.shiro.domain.ShiroProperty;
import com.iwindplus.boot.shiro.domain.constant.ShiroConstant;
import com.iwindplus.boot.shiro.domain.dto.ShiroTokenDTO;
import com.iwindplus.boot.util.HttpUtil;
import com.iwindplus.boot.web.domain.vo.ResultVO;
import com.iwindplus.boot.web.exception.BaseShiroAuthcException;
import com.iwindplus.boot.web.i18n.I18nConfig;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * 无状态认证过滤器.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class ShiroAuthcFilter extends BasicHttpAuthenticationFilter {
    private ShiroProperty shiroProperty;

    private I18nConfig i18nConfig;

    private ObjectMapper objectMapper;

    /**
     * 构造方法.
     *
     * @param shiroProperty 配置
     * @param i18nConfig    国际化
     * @param objectMapper  json转换
     */
    public ShiroAuthcFilter(ShiroProperty shiroProperty, I18nConfig i18nConfig, ObjectMapper objectMapper) {
        this.shiroProperty = shiroProperty;
        this.i18nConfig = i18nConfig;
        this.objectMapper = objectMapper;
    }

    /**
     * 判断用户是否想要登入.
     * 检测 header里面是否包含Authorization字段
     *
     * @param request  请求
     * @param response 响应
     * @return boolean
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String token = req.getHeader(ShiroConstant.AUTHORIZATION);
        return Objects.nonNull(token);
    }

    /**
     * 如果带有 token，则对 token 进行检查，否则直接通过.
     *
     * @param request     请求
     * @param response    响应
     * @param mappedValue mappedValue
     * @return boolean
     * @throws UnauthorizedException
     */
    @SneakyThrows
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
            throws UnauthorizedException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        // 如果请求头存在token,则执行登陆操作,直接返回true
        if (isLoginAttempt(request, response)) {
            //进行Shiro的登录UserRealm
            return executeLogin(request, response);
        } else {
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            String code = StringUtils.uncapitalize(status.getReasonPhrase());
            String message = this.i18nConfig.getMessage(code);
            this.getResponse(httpServletResponse, status.value(), code, message);
        }
        return false;
    }

    private void getResponse(HttpServletResponse response, Integer status, String code, String message) throws JsonProcessingException {
        ResultVO build = ResultVO.builder()
                .status(status)
                .code(code)
                .message(message)
                .build();
        String result = this.objectMapper.writeValueAsString(build);
        HttpServletResponse httpServletResponse = response;
        HttpUtil.getJson(HttpStatus.UNAUTHORIZED.value(), result, httpServletResponse);
    }

    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String token = httpServletRequest.getHeader(ShiroConstant.AUTHORIZATION);
        ShiroTokenDTO shiroTokenDTO = new ShiroTokenDTO(token);
        try {
            getSubject(request, response).login(shiroTokenDTO);
        } catch (Exception ee) {
            if (ee instanceof BaseShiroAuthcException) {
                BaseShiroAuthcException ex = (BaseShiroAuthcException) ee;
                this.getResponse(httpServletResponse, ex.getStatus().value(), ex.getCode(), ex.getMessage());
            } else {
                HttpStatus status = HttpStatus.UNAUTHORIZED;
                String code = StringUtils.uncapitalize(status.getReasonPhrase());
                String message = this.i18nConfig.getMessage(code);
                this.getResponse(httpServletResponse, status.value(), code, message);
            }
            return false;
        }
        // 如果没有抛出异常则代表登入成功，返回true
        return true;
    }

    /**
     * 对跨域提供支持.
     *
     * @param request  请求
     * @param response 响应
     * @return boolean
     * @throws Exception
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers",
                httpServletRequest.getHeader("Access-Control-Request-Headers"));
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }

    @Override
    public boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        return false;
    }
}
