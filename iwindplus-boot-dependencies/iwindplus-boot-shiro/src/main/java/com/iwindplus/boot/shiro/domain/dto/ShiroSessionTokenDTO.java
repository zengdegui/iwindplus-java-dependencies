/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.shiro.authc.UsernamePasswordToken;

import java.util.Objects;

/**
 * 用户和密码（包含验证码）令牌类数据传输对象（有状态）.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShiroSessionTokenDTO extends UsernamePasswordToken {
    private static final long serialVersionUID = 622321361554522354L;

    /**
     * 构造方法.
     *
     * @param username   用户名（必填）
     * @param password   密码 必填）
     * @param rememberMe 是否记住我（为null默认false,否则true）
     */
    public ShiroSessionTokenDTO(final String username, final String password, final Boolean rememberMe) {
        super(username, password, Objects.isNull(rememberMe) ? false : true);
    }
}
