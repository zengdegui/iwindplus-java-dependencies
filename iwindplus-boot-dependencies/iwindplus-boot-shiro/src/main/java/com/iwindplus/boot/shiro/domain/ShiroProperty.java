/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * shiro权限相关属性.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@ConfigurationProperties(prefix = "shiro", ignoreUnknownFields = true)
public class ShiroProperty {
    /**
     * 缓存key前缀.
     */
    private String cacheKeyPrefix = "shiro_cache:";

    /**
     * 认证过滤器key（ShiroAuthcFilter）.
     */
    private String filterAuthc = "authc";

    /**
     * 满足任一角色过滤器key（RolesAuthzFilter）.
     */
    private String filterRoles = "roles";

    /**
     * 满足任一权限过滤器key（PermsAuthzFilter）.
     */
    private String filterPerms = "perms";

    /**
     * 访问token过期时间，单位秒
     */
    private Long accessTokenExpireTime = 5 * 60L;

    /**
     * 更新令牌时间，单位秒
     */
    private Long refreshTokenExpireTime = 7 * 24 * 60 * 60L;
}
