/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.util;

import com.iwindplus.boot.shiro.domain.vo.LoginVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import java.util.Objects;

/**
 * shiro工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public class ShiroUtil {
	/**
	 * Subject：主体，代表了当前“用户”
	 *
	 * @return Subject
	 */
	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}

	/**
	 * 获取用户信息.
	 *
	 * @return LoginVO
	 */
	public static LoginVO getUserInfo() {
		Subject subject = SecurityUtils.getSubject();
		Object principal = subject.getPrincipal();
		if (Objects.nonNull(principal)) {
			return (LoginVO) principal;
		}
		return null;
	}

	/**
	 * 获取用户主键
	 *
	 * @return String
	 */
	public static String getUserId() {
		LoginVO data = getUserInfo();
		if (Objects.nonNull(data)) {
			return data.getId();
		}
		return null;
	}
}
