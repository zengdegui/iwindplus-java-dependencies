/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2018/12/27
 */
public class ShiroConstant {
    /**
     * 访问token.
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * 缓存中保存accessToken key的前缀.
     */
    public static final String ACCESS_TOKEN_PREFIX = "access_token:";

    /**
     * 缓存中保存refreshToken key的前缀.
     */
    public static final String REFRESH_TOKEN_PREFIX = "refresh_token:";

    /**
     * 认证缓存名.
     */
    public static final String AUTHENTICATION_CACHE_NAME = "shiro_authenticationCache";

    /**
     * 鉴权缓存名.
     */
    public static final String AUTHORIZATION_CACHE_NAME = "shiro_authorizationCache";

    /**
     * 活动当前session缓存名.
     */
    public static final String ACTIVE_SESSION_CACHE_NAME = "shiro_activeSessionCache";
}
