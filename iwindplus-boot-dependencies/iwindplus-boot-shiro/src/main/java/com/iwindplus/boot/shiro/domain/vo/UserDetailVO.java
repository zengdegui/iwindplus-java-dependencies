/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 登录用户信息视图对象.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailVO implements Serializable {
    private static final long serialVersionUID = -2852699111159844963L;

    /**
     * 用户唯一标识.
     */
    private String openid;

    /**
     * 用户名.
     */
    private String username;

    /**
     * 访问token.
     */
    private String accessToken;

    /**
     * 刷新token.
     */
    private String refreshToken;

    /**
     * 访问token过期时间，单位秒.
     */
    private Long expiresIn;
}
