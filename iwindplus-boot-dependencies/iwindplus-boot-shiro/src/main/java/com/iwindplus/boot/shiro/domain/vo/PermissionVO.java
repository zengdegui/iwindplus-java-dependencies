/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * 权限视图对象.
 *
 * @author zengdegui
 * @since 2021/10/15
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PermissionVO implements Serializable {
	/**
	 * 角色权限.
	 */
	private Set<String> rolePerms;

	/**
	 * 操作权限.
	 */
	private Set<String> operatePerms;
}
