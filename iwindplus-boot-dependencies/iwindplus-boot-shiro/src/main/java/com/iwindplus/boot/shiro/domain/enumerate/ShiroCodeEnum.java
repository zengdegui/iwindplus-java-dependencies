/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.enumerate;

import com.iwindplus.boot.web.exception.CommonException;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 业务编码返回值枚举.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
@Getter
@Accessors(fluent = true)
public enum ShiroCodeEnum implements CommonException {
    /**
     * 账号不存在.
     */
    ACCOUNT_NOT_EXIST("account_not_exist", "账号不存在"),

    /**
     * 账号被锁定.
     */
    ACCOUNT_LOCKED("account_locked", "账号被锁定"),

    /**
     * 账号被禁用.
     */
    ACCOUNT_DISABLED("account_disabled", "账号被禁用"),

    /**
     * 无效访问token.
     */
    INVALID_ACCESS_TOKEN("invalid_access_token", "无效访问token"),

    /**
     * 访问token过期.
     */
    ACCESS_TOKEN_EXPIRED("access_token_expired", "访问token过期"),

    /**
     * 密码错误.
     */
    PASSWORD_ERROR("password_error", "密码错误"),

    /**
     * 无效刷新token.
     */
    INVALID_REFRESH_TOKEN("invalid_refresh_token", "无效刷新token"),
    ;

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 构造方法.
     *
     * @param value 值
     * @param desc  描述
     */
    ShiroCodeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return ShiroCodeEnum
     */
    public static ShiroCodeEnum valueOfDesc(String desc) {
        for (ShiroCodeEnum val : ShiroCodeEnum.values()) {
            if (Objects.equals(desc, val.desc())) {
                return val;
            }
        }
        return null;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return ShiroCodeEnum
     */
    public static ShiroCodeEnum valueOfValue(String value) {
        for (ShiroCodeEnum val : ShiroCodeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.value;
    }

    @Override
    public String getMessage() {
        return this.desc;
    }
}
