/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain.dto;

import lombok.Builder;
import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * 无状态令牌类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@Builder
public class ShiroTokenDTO implements AuthenticationToken {
    private static final long serialVersionUID = -7838912794581842158L;

    /**
     * 访问token.
     */
    private String accessToken;

    public ShiroTokenDTO(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public Object getPrincipal() {
        return this.accessToken;
    }

    @Override
    public Object getCredentials() {
        return this.accessToken;
    }
}