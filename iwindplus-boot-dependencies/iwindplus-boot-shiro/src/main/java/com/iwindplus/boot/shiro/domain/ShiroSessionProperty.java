/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.domain;

import lombok.Data;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * shiro权限相关属性.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@ConfigurationProperties(prefix = "shiro.session", ignoreUnknownFields = true)
public class ShiroSessionProperty {
    /**
     * 登录地址.
     */
    private String loginUrl = "/login";

    /**
     * 登录成功地址.
     */
    private String successUrl = "/index";

    /**
     * 无权限地址.
     */
    private String unauthorizedUrl = "/unauthorized";

    /**
     * 缓存超时时间，单位秒.
     */
    private Long cacheTimeout = 2 * 60 * 60L;

    /**
     * 缓存key前缀.
     */
    private String cacheKeyPrefix = "shiro_cache:";

    /**
     * session缓存key前缀.
     */
    private String sessionCacheKeyPrefix = "shiro_session:";

    /**
     * sessionId cookie名称.
     */
    private String sessionIdCookieName = "sid";

    /**
     * 记住密码cookie名称.
     */
    private String rememberName = "rememberMe";

    /**
     * 记住密码cookie失效时间（一周），单位:秒.
     */
    private Long rememberMeTimeout = 7 * 24 * 60 * 60L;

    /**
     * 记住密码cookie加密密匙.
     */
    private String rememberCipherKey = "3AvVhmFLUs0KTA3Kprsdag==";

    /**
     * 认证过滤器key（FormAuthcFilter）.
     */
    private String filterAuthc = "authc";

    /**
     * 满足任一角色过滤器key（RolesAuthzFilter）.
     */
    private String filterRoles = "roles";

    /**
     * 满足任一权限过滤器key（PermsAuthzFilter）.
     */
    private String filterPerms = "perms";
}
