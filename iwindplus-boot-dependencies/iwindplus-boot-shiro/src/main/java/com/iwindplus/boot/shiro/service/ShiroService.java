/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.shiro.service;

import com.iwindplus.boot.shiro.domain.vo.AccessPermsVO;
import com.iwindplus.boot.shiro.domain.vo.LoginVO;
import com.iwindplus.boot.shiro.domain.vo.PermissionVO;

import java.util.List;

/**
 * shiro业务层接口.
 *
 * @author zengdegui
 * @since 2019/4/17
 */
public interface ShiroService {
	/**
	 * 通过用户名查找.
	 *
	 * @param username 用户名
	 * @return LoginVO
	 */
	LoginVO getByUsername(String username);

	/**
	 * 查询用户权限（角色权限，操作权限等）.
	 *
	 * @param username 用户名
	 * @return PermissionVO
	 */
	PermissionVO listOperatePermissionByUsername(String username);

	/**
	 * 查找访问权限.
	 *
	 * @return List<AccessPermsVO>
	 */
	List<AccessPermsVO> listAccessPerms();
}
