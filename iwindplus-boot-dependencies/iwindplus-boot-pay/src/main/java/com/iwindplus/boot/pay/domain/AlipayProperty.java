/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.pay.domain;

import com.alipay.api.AlipayConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 支付宝支付相关属性.
 *
 * @author zengdegui
 * @since 2018/10/10
 */
@Data
@ConfigurationProperties(prefix = "alipay", ignoreUnknownFields = true)
public class AlipayProperty extends AlipayConfig {
}
