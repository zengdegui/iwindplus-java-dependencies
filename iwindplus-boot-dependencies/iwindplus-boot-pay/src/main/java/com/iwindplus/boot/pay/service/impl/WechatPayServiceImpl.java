/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.pay.service.impl;

import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.notify.WxPayRefundNotifyResult;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.iwindplus.boot.pay.service.WechatPayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信支付相关业务层接口实现类.
 *
 * @author zengdegui
 * @since 2020/11/29
 */
@Slf4j
public class WechatPayServiceImpl extends WxPayServiceImpl implements WechatPayService {
    @Override
    public WxPayOrderNotifyResult orderNotify(HttpServletRequest request) {
        try {
            String xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            return this.parseOrderNotifyResult(xmlResult);
        } catch (Exception e) {
            log.error("Wechat order notify Exception [{}]", e.getMessage());
        }
        return null;
    }

    @Override
    public WxPayRefundNotifyResult refundNotify(HttpServletRequest request) {
        try {
            String xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            return this.parseRefundNotifyResult(xmlResult);
        } catch (Exception e) {
            log.error("Wechat refund notify Exception [{}]", e.getMessage());
        }
        return null;
    }
}
