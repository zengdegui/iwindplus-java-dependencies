/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.pay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.DefaultAlipayClient;
import com.iwindplus.boot.pay.domain.AlipayProperty;
import com.iwindplus.boot.pay.service.AlipayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 支付宝支付相关业务层接口实现类.
 *
 * @author zengdegui
 * @since 2020/11/29
 */
@Slf4j
public class AlipayServiceImpl extends DefaultAlipayClient implements AlipayService {
    /**
     * AlipayProperty.
     */
    @Autowired
    protected AlipayProperty alipayProperty;

    /**
     * 构造方法
     *
     * @param alipayProperty 配置参数
     */
    public AlipayServiceImpl(AlipayProperty alipayProperty) throws AlipayApiException {
        super(alipayProperty);
    }

    @Override
    public AlipayProperty getConfig() {
        return this.alipayProperty;
    }
}
