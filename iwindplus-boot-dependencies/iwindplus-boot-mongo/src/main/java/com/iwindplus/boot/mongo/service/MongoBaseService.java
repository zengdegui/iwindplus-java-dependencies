/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mongo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.iwindplus.boot.mongo.domain.MongoBaseDO;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Collection;
import java.util.List;

/**
 * monggo基础操作类.
 *
 * @param <T> 对象
 * @author zengdegui
 * @since 2019/8/9
 */
public interface MongoBaseService<T extends MongoBaseDO> {
	/**
	 * 查询所有.
	 *
	 * @return List<T>
	 */
	List<T> list();

	/**
	 * 根据主键查询.
	 *
	 * @param id 主键
	 * @return T
	 */
	T getById(String id);

	/**
	 * 保存.
	 *
	 * @param entity 对象
	 * @return boolean
	 */
	boolean save(T entity);

	/**
	 * 批量保存.
	 *
	 * @param entities
	 * @return boolean
	 */
	boolean saveBatch(Collection<T> entities);

	/**
	 * 通过主键删除.
	 *
	 * @param id 主键
	 * @return boolean
	 */
	boolean removeById(String id);

	/**
	 * 分页查询.
	 *
	 * @param page  分页
	 * @param query 条件
	 * @param <E>   对象
	 * @return Page<T>
	 */
	<E extends IPage<T>> E page(E page, Query query);
}
