/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mongo.domain;

import com.iwindplus.boot.mongo.domain.validation.MongoEditIntFc;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * mongo数据库基础实体类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MongoBaseDO<T> implements Serializable {
    private static final long serialVersionUID = -407100883907464557L;

    /**
     * 主键.
     */
    @NotBlank(message = "{id.notBlank}", groups = {MongoEditIntFc.class})
    protected String id;

    /**
     * 创建时间（添加时自动维护）.
     */
    protected LocalDateTime createTime;

    /**
     * 创建人（添加时自动维护）.
     */
    protected String createBy;

    /**
     * 更新时间（添加，编辑时自动维护）.
     */
    protected LocalDateTime updateTime;

    /**
     * 更新人（编辑时自动维护）.
     */
    protected String updateBy;

    /**
     * 是否删除（0：未删除，1：已删除）（添加时自动维护）.
     */
    protected Boolean flagDelete;
}
