/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mongo.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2020/10/17
 */
public class MongoConstant {
    /**
     * 创建时间.
     */
    public static final String CREATE_TIME = "createTime";

    /**
     * 更新时间.
     */
    public static final String UPDATE_TIME = "updateTime";

    /**
     * 创建人.
     */
    public static final String CREATE_BY = "createBy";

    /**
     * 更新人.
     */
    public static final String UPDATE_BY = "updateBy";

    /**
     * 是否删除.
     */
    public static final String FLAG_DELETE = "flagDelete";
}
