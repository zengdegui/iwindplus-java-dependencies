/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mongo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.iwindplus.boot.mongo.domain.MongoBaseDO;
import com.iwindplus.boot.mongo.domain.constant.MongoConstant;
import com.iwindplus.boot.mongo.service.MongoBaseService;
import com.mongodb.client.result.DeleteResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * monggo基础操作实现类.
 *
 * @param <T> 对象
 * @author zengdegui
 * @since 2019/8/9
 */
@Slf4j
public class MongoBaseServiceImpl<T extends MongoBaseDO> implements MongoBaseService<T> {
    /**
     * 反射实体.
     */
    protected Class<T> entityClass;

    /**
     * MongoTemplate.
     */
    @Autowired
    protected MongoTemplate mongoTemplate;

    /**
     * 构造方法.
     */
    public MongoBaseServiceImpl() {
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) pt.getActualTypeArguments()[0];
    }

    @Override
    public List<T> list() {
        return this.mongoTemplate.findAll(this.entityClass);
    }

    @Override
    public T getById(String id) {
        return this.mongoTemplate.findById(id, this.entityClass);
    }

    @Override
    public boolean save(T entity) {
        LocalDateTime now = LocalDateTime.now();
        entity.setCreateTime(now);
        entity.setUpdateTime(now);
        entity.setFlagDelete(false);
        T result = this.mongoTemplate.save(entity);
        return Objects.nonNull(result) && StringUtils.isNotBlank(result.getId());
    }

    @Override
    public boolean saveBatch(Collection<T> entities) {
        LocalDateTime now = LocalDateTime.now();
        entities.forEach(entity -> {
            entity.setCreateTime(now);
            entity.setUpdateTime(now);
            entity.setFlagDelete(false);
        });
        Collection<T> result = this.mongoTemplate.insertAll(entities);
        if (CollectionUtils.isNotEmpty(result)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean removeById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        DeleteResult result = this.mongoTemplate.remove(query, this.entityClass);
        return Objects.nonNull(result) && result.getDeletedCount() >= 1;
    }

    @Override
    public <E extends IPage<T>> E page(E page, Query query) {
        // 排序
        List<Sort.Order> orders = new ArrayList<>(10);
        List<OrderItem> orderList = page.orders();
        if (CollectionUtils.isEmpty(orderList)) {
            Sort.Order item = Sort.Order.desc(MongoConstant.UPDATE_TIME);
            orders.add(item);
        } else {
            orderList.forEach(order -> {
                Sort.Order item;
                if (order.isAsc()) {
                    item = Sort.Order.asc(order.getColumn());
                } else {
                    item = Sort.Order.desc(order.getColumn());
                }
                orders.add(item);
            });
        }
        long count = this.mongoTemplate.count(query, this.entityClass);
        if (count > 0) {
            PageRequest pageRequest = PageRequest.of((int) page.getCurrent() - 1, (int) page.getSize());
            Sort sort = Sort.by(orders);
            List<T> list = this.mongoTemplate.find(query.with(pageRequest).with(sort), this.entityClass);
            page.setRecords(list);
            page.setTotal(count);
        }
        return page;
    }
}
