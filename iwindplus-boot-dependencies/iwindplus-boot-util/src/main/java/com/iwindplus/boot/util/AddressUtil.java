/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.util.domain.vo.IpVo;
import com.luues.util.ip.IpUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 通过ip获取所在省市信息工具类.
 *
 * @author zengdegui
 * @since 2021/1/11
 */
@Slf4j
public class AddressUtil {
    /**
     * 太平洋网络的接口.
     */
    public static final String PCONLINE_URL_STR = "http://whois.pconline.com.cn/ipJson.jsp";

    /**
     * 高德云图的接口.
     */
    public static final String GAODEYUNTU_URL_STR = "http://iploc.market.alicloudapi.com/v3/ip";

    /**
     * ip138的接口（准确）.
     */
    public static final String IP138_URL_STR = "https://api.ip138.com/ip";

    /**
     * 调用太平洋网络接口查询.
     *
     * @param ip ip（必填）
     * @return IpVo
     */
    public static IpVo getIpVo(String ip) {
        ObjectMapper objectMapper = new ObjectMapper();
        String url = new StringBuilder(PCONLINE_URL_STR).append("?json=true").append("&ip=").append(ip).toString();
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).build();
        Call call = client.newCall(request);
        try {
            Response response = call.execute();
            if (Objects.nonNull(response)) {
                String result = response.body().string();
                if (response.isSuccessful()) {
                    JsonNode data = objectMapper.readTree(result);
                    IpVo ipVo = IpVo.builder()
                            .ip(ip)
                            .province(data.get("pro").asText())
                            .city(data.get("city").asText())
                            .build();
                    return ipVo;
                }
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * 调用高德云图查询.
     *
     * @param appCode 应用code（必填）
     * @param ip      ip（必填）
     * @return IpVo
     */
    public static IpVo getIpVoTwo(String appCode, String ip) {
        ObjectMapper objectMapper = new ObjectMapper();
        String url = new StringBuilder(GAODEYUNTU_URL_STR).append("?ip=").append(ip).toString();
        Map<String, String> headerMap = new HashMap<>(16);
        String authorization = new StringBuilder("APPCODE ").append(appCode).toString();
        headerMap.put("Authorization", authorization);
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().headers(Headers.of(headerMap)).url(url).build();
        Call call = client.newCall(request);
        try {
            Response response = call.execute();
            if (Objects.nonNull(response)) {
                String result = response.body().string();
                if (response.isSuccessful()) {
                    JsonNode data = objectMapper.readTree(result);
                    IpVo ipVo = IpVo.builder()
                            .ip(ip)
                            .province(data.get("province").asText())
                            .city(data.get("city").asText())
                            .build();
                    return ipVo;
                }
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * 调用ip138查询.
     *
     * @param token token（必填）
     * @param ip    ip（必填）
     * @return IpVo
     */
    public static IpVo getIpVoThree(String token, String ip) {
        ObjectMapper objectMapper = new ObjectMapper();
        String url = new StringBuilder(IP138_URL_STR).append("?datatype=jsonp")
                .append("&token=").append(token)
                .append("&ip=").append(ip).toString();
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).build();
        Call call = client.newCall(request);
        try {
            Response response = call.execute();
            if (Objects.nonNull(response)) {
                String result = response.body().string();
                if (response.isSuccessful()) {
                    JsonNode data = objectMapper.readTree(result);
                    JsonNode arrNode = data.get("data");
                    if (arrNode.isArray()) {
                        IpVo ipVo = IpVo.builder()
                                .ip(ip)
                                .province(arrNode.get(1).asText())
                                .city(arrNode.get(2).asText())
                                .build();
                        return ipVo;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * 根据访问者的Request，返回ip、地理位置（太平洋网络）.
     *
     * @param request 请求（必填）
     * @return IpVo
     */
    public static IpVo getIpVoByRequest(HttpServletRequest request) {
        return getIpVo(IpUtil.getIpAddress(request));
    }

    /**
     * 根据访问者的Request，返回ip、地理位置（高德云图）.
     *
     * @param appCode 应用code（必填）
     * @param request 请求（必填）
     * @return IpVo
     */
    public static IpVo getIpVoTwoByRequest(String appCode, HttpServletRequest request) {
        return getIpVoTwo(appCode, IpUtil.getIpAddress(request));
    }

    /**
     * 根据访问者的Request，返回ip、地理位置（IP138）.
     *
     * @param token   token（必填）
     * @param request 请求（必填）
     * @return IpVo
     */
    public static IpVo getIpVoThreeByRequest(String token, HttpServletRequest request) {
        return getIpVoThree(token, IpUtil.getIpAddress(request));
    }
}
