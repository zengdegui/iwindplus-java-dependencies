/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import cn.hutool.jwt.signers.JWTSignerUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWTUtil加解密工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public class JwtUtil extends JWTSignerUtil {
    /**
     * 生成token.
     *
     * @param claims     数据
     * @param secret     密钥
     * @param expireTime 过期时间点
     * @return String
     */
    public static String genToken(Map<String, String> claims, String secret, Long expireTime) {
        // 使用HMAC256进行加密
        Algorithm algorithm = Algorithm.HMAC256(secret);
        Date expireDatePoint = new Date(System.currentTimeMillis() + expireTime * 1000);
        // 创建jwt
        JWTCreator.Builder builder = JWT.create().
                withExpiresAt(expireDatePoint);
        // 传入参数
        claims.forEach((key, value) -> {
            builder.withClaim(key, value);
        });
        // 签名加密
        return builder.sign(algorithm);
    }

    /**
     * 验证jwt.
     *
     * @param token  token
     * @param secret 密钥
     * @return boolean
     */
    public static boolean verifyToken(String token, String secret) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        // 解密
        JWTVerifier verifier = JWT.require(algorithm).build();
        verifier.verify(token);
        return true;
    }

    /**
     * 解密jwt.
     *
     * @param token token
     * @return Map<String, String>
     */
    public static Map<String, String> decryptToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        Map<String, Claim> map = jwt.getClaims();
        Map<String, String> resultMap = new HashMap<>(16);
        map.forEach((key, val) -> resultMap.put(key, val.asString()));
        return resultMap;
    }
}
