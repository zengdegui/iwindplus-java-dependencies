/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import cn.hutool.core.io.FileUtil;
import com.sun.jna.Platform;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemHeaders;
import org.apache.commons.fileupload.util.FileItemHeadersImpl;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Objects;

/**
 * html转pdf工具类.
 *
 * @author zengdegui
 * @since 2021/1/11
 */
@Slf4j
public class HtmlToPdfUtil {
    private static final File WK_HOME_DIR = FileUtil.file(FileUtil.getUserHomePath() + "/wkHome");
    private static final File WK_TMP_DIR = FileUtil.file(FileUtil.getTmpDirPath() + "/wkTemp");
    private static final File SIM_SUN_FONT_DIR = Platform.isLinux() ? FileUtil.file("/usr/share/fonts/chinese/TrueType")
            : FileUtil.file("C:\\Windows\\Fonts");
    private static File wkTool;
    private static File simSunFont;
    private static boolean canUse = true;
    private static boolean able = true;

    static {
        log.info("Tools are only  available for Windows 64 and Linux 64 platforms !!!");
        boolean init = forceInit();
        log.info("Tools init result: {}", init);
    }

    /**
     * 初始化.
     *
     * @return 是否初始化成功
     */
    public static boolean forceInit() {
        long initc = 0L;
        if (!WK_HOME_DIR.exists()) {
            able = WK_HOME_DIR.mkdirs();
        }
        log.info("{},check wkHomeDir ,result:{}", ++initc, able);
        if (!WK_TMP_DIR.exists()) {
            able = WK_TMP_DIR.mkdirs();
        }
        log.info("{},check wkTmpDir ,result:{}", ++initc, able);
        if (!SIM_SUN_FONT_DIR.exists()) {
            able = SIM_SUN_FONT_DIR.mkdirs();
        }
        log.info("{},check simsunFontDir ,result:{}", ++initc, able);
        InputStream wkHtmlToxAsStream = null;
        InputStream simSunAsStream = null;
        if (able) {
            wkHtmlToxAsStream = Platform.isLinux() ? HtmlToPdfUtil.class.getResourceAsStream("/wkhtmltox") : HtmlToPdfUtil.class.getResourceAsStream("/wkhtmltopdf.exe");
            simSunAsStream = HtmlToPdfUtil.class.getResourceAsStream("/simsun.ttc");
        }
        if (Objects.isNull(wkHtmlToxAsStream) || Objects.isNull(simSunAsStream)) {
            able = false;
        }
        log.info("{},load wktool and font source ,result:{}", ++initc, able);
        if (able) {
            File font = new File(SIM_SUN_FONT_DIR, "simsun.ttc");
            File wk = new File(WK_HOME_DIR, Platform.isLinux() ? "wkhtmltox" : "wkhtmltopdf.exe");
            try {
                if (!font.exists()) {
                    assert Objects.nonNull(simSunAsStream);
                    able = 1 < Files.copy(simSunAsStream, font.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                log.info("{},copy font source to {},result:{}", ++initc, font.toPath(), able);
                if (!wk.exists()) {
                    assert Objects.nonNull(wkHtmlToxAsStream);
                    able = 1 < Files.copy(wkHtmlToxAsStream, wk.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                log.info("{},copy wktools source to {},result:{}", ++initc, font.toPath(), able);
                if (able) {
                    wkTool = wk;
                    simSunFont = font;
                }
            } catch (IOException e) {
                able = false;
                log.error("{}, error when copy source : {} ", ++initc, e.getMessage());
            }
        }
        if (able) {
            if (Platform.isLinux()) {
                boolean canExe = exePermissionCheck();
                log.info("{},check run permission,result: {}  ", ++initc, canExe ? "has permission" : "no permission");
                if (!canExe) {
                    simpleExecCommand("chmod +x " + wkTool.getPath());
                    if (!exePermissionCheck()) {
                        log.error("{},add permission failed", ++initc);
                        able = false;
                    }
                }
            }
        }
        if (able) {
            able = cleanTempDir();
        }
        if (able) {
            log.info("{},init success!", ++initc);
        } else {
            log.info("{},init failed!", ++initc);
            canUse = false;
        }
        return able;
    }

    /**
     * 获取字体路径.
     *
     * @return String
     */
    public String getSimsunPath() {
        log.info("world path:" + simSunFont.getPath());
        return simSunFont.getPath();
    }

    /**
     * 构建实例.
     *
     * @return Html2PdfUtil
     */
    public static HtmlToPdfUtil build() {
        return new HtmlToPdfUtil();
    }

    private static boolean exePermissionCheck() {
        String permissionLog = simpleExecCommand("ls -l " + wkTool.getPath());
        return StringUtils.isNotBlank(permissionLog) && permissionLog.length() >= 10 && 120 == permissionLog.charAt(9);
    }

    private static boolean cleanTempDir() {
        if (WK_TMP_DIR.exists()) {
            canUse = deleteFiles(WK_TMP_DIR) ? WK_TMP_DIR.mkdirs() : canUse;
            log.info("cleanTempDir,result:{} ", canUse);
        } else {
            canUse = WK_TMP_DIR.mkdirs();
        }
        return canUse;
    }

    /**
     * 转换为文本.
     *
     * @param text     文本
     * @param fileName pdf缓存文件名
     * @param charset  编码
     * @return FileItem
     */
    public synchronized FileItem convertPdfFromText(String text, String fileName, String charset) {
        cleanTempDir();
        if (!canUse) {
            log.info("tools crash,can invoke forceInit() method see reason !!!");
            return null;
        }
        File html = new File(WK_TMP_DIR, fileName + ".html");
        File pdf = new File(WK_TMP_DIR, fileName + ".pdf");
        // 将html字符串写入到临时的html文件
        if (StringUtils.isBlank(charset)) {
            charset = "UTF-8";
        }
        FileUtil.writeString(text, html, charset);
        if (html.exists() && html.isFile()) {
            log.info("exec html to  pdf ，wktoolPath=>{}", wkTool.getPath());
            simpleExecCommand(wkTool.getPath() + " " + html.getPath() + " " + pdf.getPath());
        }
        if (pdf.exists() && pdf.isFile()) {
            try (FileInputStream fileInputStream = new FileInputStream(pdf); ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
                byte[] buffer = new byte[2014];
                while (fileInputStream.read(buffer) != -1) {
                    byteArrayOutputStream.write(buffer);
                }
                byteArrayOutputStream.flush();
                byte[] data = byteArrayOutputStream.toByteArray();
                if (data.length > 0) {
                    log.info("html to  pdf  success ");
                    SimplePdfFileItem file = new SimplePdfFileItem(pdf, data, Files.probeContentType(pdf.toPath()), "file");
                    log.info("pdf size :{}", file.getSize());
                    return file;
                }
            } catch (IOException e) {
                log.error("html to  pdf  failed [{}]", e.getMessage());
                return null;
            }
        }
        log.info("html to  pdf  failed,no data");
        return null;
    }

    private static boolean deleteFiles(File file) {
        if (!file.exists()) {
            log.info("del the file:{},is not exists", file.getPath());
            return false;
        }
        if (file.isFile()) {
            return file.delete();
        }
        File[] subFiles = file.listFiles();
        if (Objects.nonNull(subFiles) && subFiles.length > 0) {
            Arrays.asList(subFiles).forEach(HtmlToPdfUtil::deleteFiles);
        }
        return file.delete();
    }

    private static String simpleExecCommand(String cmd) {
        try {
            String[] linux = {"/bin/sh", "-c", cmd};
            String[] windows = {"cmd", "/c", cmd};
            String[] cmdA = Platform.isLinux() ? linux : windows;
            Process process = Runtime.getRuntime().exec(cmdA);
            LineNumberReader br = new LineNumberReader(new InputStreamReader(process.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while (Objects.nonNull(line = br.readLine())) {
                log.info(line);
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            return null;
        }
    }

    @ToString
    static class SimplePdfFileItem implements FileItem {
        private static final long serialVersionUID = 2237570099615271025L;
        public static final String DEFAULT_CHARSET = "ISO-8859-1";
        private String fieldName;
        private final String fileName;
        private boolean isFormField;
        private final byte[] cachedContent;
        private final String contentType;
        private final File dFosFile;
        private FileItemHeaders headers;

        /**
         * 构造函数.
         *
         * @param dFosFile      文件
         * @param cachedContent 内容
         * @param contentType   类型
         * @param fieldName     字段名
         */
        public SimplePdfFileItem(File dFosFile, byte[] cachedContent, String contentType, String fieldName) {
            this.fieldName = fieldName;
            this.fileName = dFosFile.getName();
            this.isFormField = false;
            this.cachedContent = Objects.isNull(cachedContent) || cachedContent.length < 1 ? new byte[0] : cachedContent;
            this.contentType = contentType;
            this.dFosFile = dFosFile;
            this.headers = new FileItemHeadersImpl();
        }

        /**
         * 构造函数.
         *
         * @param fieldName     字段名
         * @param fileName      文件名
         * @param isFormField   是否表单字段
         * @param cachedContent 内容
         * @param contentType   类型
         * @param dFosFile      文件
         * @param headers       头部
         */
        public SimplePdfFileItem(String fieldName, String fileName, boolean isFormField, byte[] cachedContent
                , String contentType, File dFosFile, FileItemHeaders headers) {
            this.fieldName = fieldName;
            this.fileName = fileName;
            this.isFormField = isFormField;
            this.cachedContent = cachedContent;
            this.contentType = contentType;
            this.dFosFile = dFosFile;
            this.headers = headers;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (Objects.isNull(this.dFosFile)) {
                return new ByteArrayInputStream(this.cachedContent);
            }
            return new FileInputStream(dFosFile);
        }

        @Override
        public String getContentType() {
            return this.contentType;
        }

        @Override
        public String getName() {
            return this.fileName;
        }

        @Override
        public boolean isInMemory() {
            return this.cachedContent.length > 0;
        }

        @Override
        public long getSize() {
            return this.cachedContent.length;
        }

        @Override
        public byte[] get() {
            return this.cachedContent;
        }

        @Override
        public String getString(String s) throws UnsupportedEncodingException {
            return getString();
        }

        @Override
        public String getString() {
            return new String(cachedContent, StandardCharsets.UTF_8);
        }

        @Override
        public void write(File file) throws Exception {
            Files.write(file.toPath(), cachedContent, StandardOpenOption.CREATE);
        }

        @Override
        public void delete() {
            boolean delete = dFosFile.delete();
        }

        @Override
        public String getFieldName() {
            return this.fieldName;
        }

        @Override
        public void setFieldName(String s) {
            this.fieldName = s;
        }

        @Override
        public boolean isFormField() {
            return this.isFormField;
        }

        @Override
        public void setFormField(boolean b) {
            this.isFormField = b;
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            if (Objects.isNull(this.dFosFile)) {
                return new ByteArrayOutputStream(1024);
            }
            return new FileOutputStream(this.dFosFile);
        }

        @Override
        public FileItemHeaders getHeaders() {
            return this.headers;
        }

        @Override
        public void setHeaders(FileItemHeaders fileItemHeaders) {
            this.headers = fileItemHeaders;
        }
    }
}
