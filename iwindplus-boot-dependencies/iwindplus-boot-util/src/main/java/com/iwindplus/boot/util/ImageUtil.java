/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import cn.hutool.core.img.ImgUtil;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 拼接图片工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class ImageUtil extends ImgUtil {
    /**
     * 图片的间隙.
     */
    private static final int SIDE = 6;
    /**
     * 画板尺寸宽.
     */
    private static final int CANVANS_W = 112;

    /**
     * 生成群组头像.
     *
     * @param paths    图片链接
     * @param dir      输出路径
     * @param fileName 文件名
     * @return String
     * @throws IOException
     */
    public static String generate(List<String> paths, String dir, String fileName) throws IOException {
        return generate(paths, CANVANS_W, CANVANS_W, SIDE, dir, fileName);
    }

    /**
     * 生成群组头像.
     *
     * @param paths    图片链接
     * @param width    画布宽
     * @param height   画布高
     * @param side     图片的间隙
     * @param dir      输出路径
     * @param fileName 文件名
     * @return String
     * @throws IOException
     */
    public static String generate(List<String> paths, int width, int height, int side, String dir, String fileName)
            throws IOException {
        List<BufferedImage> bufferedImages = new ArrayList<>(10);
        int imageSize = 0;
        if (paths.size() <= 1) {
            //若为一张图片
            imageSize = height - (2 * side);
        } else if (paths.size() > 1 && paths.size() < 5) {
            //若为2-4张图片
            imageSize = (height - (3 * side)) / 2;
        } else {
            //若>=5张图片
            imageSize = (height - (4 * side)) / 3;
        }
        for (int i = 0; i < paths.size(); i++) {
            BufferedImage resize = resize(paths.get(i), imageSize, imageSize, true);
            bufferedImages.add(resize);
        }
        BufferedImage outImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 生成画布
        Graphics g = outImage.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        // 设置背景色
        g2d.setBackground(new Color(0.906f, 0.906f, 0.906f, 0.5f));
        // 通过使用当前绘图表面的背景色进行填充来清除指定的矩形。
        g2d.clearRect(0, 0, width, height);
        // 开始拼凑 根据图片的数量判断该生成那种样式的组合头像目前为九种
        patchworkPic(width, side, bufferedImages, imageSize, g2d);
        StringBuilder outPath = new StringBuilder().append(dir)
                .append(File.separatorChar)
                .append(fileName).append(".jpg");
        String format = "JPG";
        File file = new File(outPath.toString());
        if (!file.exists()) {
            file.mkdirs();
        }
        boolean data = ImageIO.write(outImage, format, file);
        if (data) {
            return outPath.toString();
        }
        return null;
    }

    private static void patchworkPic(int width, int side, List<BufferedImage> bufferedImages, int imageSize, Graphics2D g2d) {
        for (int i = 1; i <= bufferedImages.size(); i++) {
            Integer size = bufferedImages.size();
            switch (size) {
                case 1:
                    g2d.drawImage(bufferedImages.get(i - 1), side, side, null);
                    break;
                case 2:
                    if (i == 1) {
                        g2d.drawImage(bufferedImages.get(i - 1), side, (width - imageSize) / 2, null);
                    } else {
                        g2d.drawImage(bufferedImages.get(i - 1), 2 * side + imageSize, (width - imageSize) / 2, null);
                    }
                    break;
                case 3:
                    if (i == 1) {
                        g2d.drawImage(bufferedImages.get(i - 1), (width - imageSize) / 2, side, null);
                    } else {
                        g2d.drawImage(bufferedImages.get(i - 1), (i - 1) * side + (i - 2) * imageSize, imageSize + (2 * side), null);
                    }
                    break;
                case 4:
                    if (i <= 2) {
                        g2d.drawImage(bufferedImages.get(i - 1), i * side + (i - 1) * imageSize, side, null);
                    } else {
                        g2d.drawImage(bufferedImages.get(i - 1), (i - 2) * side + (i - 3) * imageSize, imageSize + 2 * side, null);
                    }
                    break;
                case 5:
                    if (i <= 2) {
                        g2d.drawImage(bufferedImages.get(i - 1), (width - 2 * imageSize - side) / 2 + (i - 1) * imageSize + (i - 1) * side, (width - 2 * imageSize - side) / 2, null);
                    } else {
                        g2d.drawImage(bufferedImages.get(i - 1), (i - 2) * side + (i - 3) * imageSize, ((width - 2 * imageSize - side) / 2) + imageSize + side, null);
                    }
                    break;
                case 6:
                    if (i <= 3) {
                        g2d.drawImage(bufferedImages.get(i - 1), side * i + imageSize * (i - 1), (width - 2 * imageSize - side) / 2, null);
                    } else {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 3) * side) + ((i - 4) * imageSize), ((width - 2 * imageSize - side) / 2) + imageSize + side, null);
                    }
                    break;
                case 7:
                    if (i <= 1) {
                        g2d.drawImage(bufferedImages.get(i - 1), 2 * side + imageSize, side, null);
                    }
                    if (i <= 4 && i > 1) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 1) * side) + ((i - 2) * imageSize), 2 * side + imageSize, null);
                    }
                    if (i <= 7 && i > 4) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 4) * side) + ((i - 5) * imageSize), 3 * side + 2 * imageSize, null);
                    }
                    break;
                case 8:
                    if (i <= 2) {
                        g2d.drawImage(bufferedImages.get(i - 1), (width - 2 * imageSize - side) / 2 + (i - 1) * imageSize + (i - 1) * side, side, null);
                    }
                    if (i <= 5 && i > 2) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 2) * side) + ((i - 3) * imageSize), 2 * side + imageSize, null);
                    }
                    if (i <= 8 && i > 5) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 5) * side) + ((i - 6) * imageSize), 3 * side + 2 * imageSize, null);
                    }
                    break;
                case 9:
                    if (i <= 3) {
                        g2d.drawImage(bufferedImages.get(i - 1), (i * side) + ((i - 1) * imageSize), side, null);
                    }
                    if (i <= 6 && i > 3) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 3) * side) + ((i - 4) * imageSize), 2 * side + imageSize, null);
                    }
                    if (i <= 9 && i > 6) {
                        g2d.drawImage(bufferedImages.get(i - 1), ((i - 6) * side) + ((i - 7) * imageSize), 3 * side + 2 * imageSize, null);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 图片缩放.
     *
     * @param filePath 图片路径
     * @param height   高度
     * @param width    宽度
     * @param bb       比例不对时是否需要补白
     * @return BufferedImage
     */
    public static BufferedImage resize(String filePath, int height, int width, boolean bb) {
        try {
            // 缩放比例
            double ratio = 0;
            BufferedImage bi = null;
            if (filePath.indexOf("http://") == 0 || filePath.indexOf("https://") == 0) {
                bi = ImageIO.read(new URL(filePath));
            } else {
                bi = ImageIO.read(new File(filePath));
            }
            Image itemp = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            // 计算比例
            if ((bi.getHeight() > height) || (bi.getWidth() > width)) {
                if (bi.getHeight() > bi.getWidth()) {
                    ratio = (new Integer(height)).doubleValue() / bi.getHeight();
                } else {
                    ratio = (new Integer(width)).doubleValue() / bi.getWidth();
                }
                AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(ratio, ratio), null);
                itemp = op.filter(bi, null);
            }
            if (bb) {
                BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = image.createGraphics();
                g.setColor(Color.white);
                g.fillRect(0, 0, width, height);
                if (width == itemp.getWidth(null)) {
                    g.drawImage(itemp, 0, (height - itemp.getHeight(null)) / 2,
                            itemp.getWidth(null), itemp.getHeight(null),
                            Color.white, null);
                } else {
                    g.drawImage(itemp, (width - itemp.getWidth(null)) / 2, 0,
                            itemp.getWidth(null), itemp.getHeight(null),
                            Color.white, null);
                }
                g.dispose();
                itemp = image;
            }
            return (BufferedImage) itemp;
        } catch (IOException e) {
            log.error("IO exception [{}]", e.getMessage());
        }
        return null;
    }
}
