/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * okhttp工具类.
 *
 * @author zengdegui
 * @since 2021/11/23
 */
@Slf4j
public class OkhttpUtil {
    /**
     * post同步请求.
     *
     * @param url         路径
     * @param headers     请求头
     * @param requestBody 请求体
     * @return String
     */
    public static Response sendPost(String url, Headers headers, RequestBody requestBody) throws IOException {
        Call call = getCall(url, headers, requestBody);
        Response response = call.execute();
        return response;
    }

    /**
     * post异步请求.
     *
     * @param url         路径
     * @param headers     请求头
     * @param requestBody 请求体
     * @param callback    回调接口
     */
    public static void sendAsyncPost(String url, Headers headers, RequestBody requestBody, ICallback callback) {
        Call call = getCall(url, headers, requestBody);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                if (Objects.nonNull(response) && response.isSuccessful()) {
                    callback.onSuccessful(call, response.body().toString());
                }
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e.getMessage());
            }
        });
    }

    /**
     * 请求body为表单.
     *
     * @param bodyMap  body
     * @param fileList 文件集合（可选）
     * @return RequestBody
     */
    @NotNull
    public static RequestBody getRequestFromBody(@NotNull Map<String, Object> bodyMap, List<MultipartFile> fileList) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        for (Map.Entry<String, Object> entry : bodyMap.entrySet()) {
            builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
        }
        if (CollectionUtils.isNotEmpty(fileList)) {
            fileList.forEach(file -> {
                try {
                    RequestBody fileBody = RequestBody.Companion.create(file.getBytes());
                    builder.addFormDataPart("file", file.getOriginalFilename(), fileBody);
                } catch (IOException e) {
                    log.error("RequestBody IOException [{}]", e.getMessage());
                }
            });
        }
        return builder.build();
    }

    /**
     * 请求body为json.
     *
     * @param json json
     * @return RequestBody
     */
    @NotNull
    public static RequestBody getRequestJsonBody(@NotNull String json) {
        RequestBody requestBody = FormBody.create(json,
                MediaType.parse("application/json; charset=utf-8"));
        return requestBody;
    }

    @NotNull
    private static Call getCall(String url, Headers headers, RequestBody requestBody) {
        Request.Builder builder = new Request.Builder();
        if (Objects.nonNull(headers)) {
            builder.headers(headers);
        }
        builder.url(url).post(requestBody);
        Request request = builder.build();
        OkHttpClient client = new OkHttpClient.Builder().build();
        return client.newCall(request);
    }

    /**
     * 自定义一个接口回调.
     */
    public interface ICallback {
        /**
         * 成功回调.
         *
         * @param call call
         * @param data 数据
         */
        void onSuccessful(Call call, String data);

        /**
         * 失败回调.
         *
         * @param call
         * @param errorMsg
         */
        void onFailure(Call call, String errorMsg);
    }
}
