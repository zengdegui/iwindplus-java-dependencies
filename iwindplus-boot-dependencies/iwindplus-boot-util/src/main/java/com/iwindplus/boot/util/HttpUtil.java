/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

/**
 * HttpUtil操作工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class HttpUtil extends cn.hutool.http.HttpUtil {
    /**
     * iso-8859-1.
     */
    public static final String ISO_8859_1_CHARSET = "iso-8859-1";

    /**
     * utf-8.
     */
    public static final String UTF8_CHARSET = "utf-8";

    /**
     * 获取服务器url.
     *
     * @param request 请求
     * @return String
     */
    public static String getServerUrl(HttpServletRequest request) {
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + request.getContextPath();
        return url;
    }

    /**
     * http获取请求参数.
     *
     * @param request 请求
     * @return String
     */
    public static String readData(HttpServletRequest request) {
        BufferedReader br = null;
        try {
            StringBuilder result = new StringBuilder();
            String line;
            for (br = request.getReader(); (line = br.readLine()) != null; result.append(line)) {
                if (result.length() > 0) {
                    result.append("\n");
                }
            }
            line = result.toString();
            return line;
        } catch (IOException var12) {
            throw new RuntimeException(var12);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException var11) {
                    var11.printStackTrace();
                }
            }
        }
    }

    /**
     * http获取请求参数.
     *
     * @param request 请求
     * @return Map<String, String>
     */
    public static Map<String, String> getParams(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>(16);
        Map<String, String[]> requestParams = request.getParameterMap();
        requestParams.entrySet().stream().forEach(entity -> {
            String name = entity.getKey();
            String[] values = entity.getValue();
            String valueStr = StringUtils.join(values, ",");
            try {
                valueStr = new String(valueStr.getBytes(ISO_8859_1_CHARSET), UTF8_CHARSET);
            } catch (UnsupportedEncodingException e) {
                log.error("Unsupported encoding exception [{}]", e.getMessage());
            }
            params.put(name, valueStr);
        });
        return params;
    }

    /**
     * http获取请求参数.
     *
     * @param request 请求
     * @return Map<String, String>
     */
    public static Map<String, String> toMap(HttpServletRequest request) {
        Map<String, String> params = new HashMap(16);
        Map<String, String[]> requestParams = request.getParameterMap();
        Iterator it = requestParams.keySet().iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int ii = 0; ii < values.length; ++ii) {
                valueStr = ii == values.length - 1 ? valueStr + values[ii] : valueStr + values[ii] + ",";
            }
            params.put(name, valueStr);
        }
        return params;
    }

    /**
     * url 参数转码
     *
     * @param value    值
     * @param encoding 编码
     * @return String
     */
    public static String urlEncode(String value, String encoding) {
        try {
            String encoded = URLEncoder.encode(value, encoding);
            return encoded.replace("+", "%20").replace("*", "%2A").replace("~", "%7E").replace("/", "%2F");
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding exception [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * url 参数解码
     *
     * @param value    值
     * @param encoding 编码
     * @return String
     */
    public static String urlDecode(String value, String encoding) {
        try {
            return URLDecoder.decode(value, encoding);
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding exception [{}]", e.getMessage());
        }
        return null;
    }

    /**
     * 参数转字符
     *
     * @param params  参数
     * @param charset 编码
     * @return String
     */
    public static String paramToQueryString(Map<String, String> params, String charset) {
        if (MapUtils.isNotEmpty(params)) {
            StringBuilder paramString = new StringBuilder();
            boolean first = true;
            for (Iterator it = params.entrySet().iterator(); it.hasNext(); first = false) {
                Map.Entry<String, String> p = (Map.Entry) it.next();
                String key = (String) p.getKey();
                String value = (String) p.getValue();
                if (!first) {
                    paramString.append("&");
                }
                paramString.append(urlEncode(key, charset));
                if (Objects.nonNull(value)) {
                    paramString.append("=").append(urlEncode(value, charset));
                }
            }
            return paramString.toString();
        }
        return null;
    }

    /**
     * 请求头iso88591转utf-8编码.
     *
     * @param headers 请求头
     */
    public static void convertHeaderCharsetFromIso88591(Map<String, String> headers) {
        convertHeaderCharset(headers, ISO_8859_1_CHARSET, UTF8_CHARSET);
    }

    /**
     * 请求头utf-8转iso88591编码.
     *
     * @param headers 请求头
     */
    public static void convertHeaderCharsetToIso88591(Map<String, String> headers) {
        convertHeaderCharset(headers, UTF8_CHARSET, ISO_8859_1_CHARSET);
    }

    private static void convertHeaderCharset(Map<String, String> headers, String fromCharset, String toCharset) {
        Iterator it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> header = (Map.Entry) it.next();
            if (Objects.nonNull(header.getValue())) {
                try {
                    header.setValue(new String(((String) header.getValue()).getBytes(fromCharset), toCharset));
                } catch (UnsupportedEncodingException ex) {
                    throw new IllegalArgumentException("Invalid charset name: " + ex.getMessage(), ex);
                }
            }
        }
    }

    /**
     * 判读是否是ajax请求.
     *
     * @param request 请求
     * @return boolean
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String requestedWith = request.getHeader("x-requested-with");
        if (StringUtils.isNotBlank(requestedWith) && StringUtils.equalsIgnoreCase(requestedWith, "XMLHttpRequest")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据请求头判断是否是手机
     *
     * @param request 请求
     * @return boolean
     */
    public static boolean isMobile(HttpServletRequest request) {
        String userAgent = request.getHeader("user-agent");
        UserAgent ua = UserAgentUtil.parse(userAgent);
        return ua.isMobile();
    }

    /**
     * http响应信息输出.
     *
     * @param status   状态码
     * @param result   结果
     * @param response 响应
     */
    public static void getJson(Integer status, String result, HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            response.setCharacterEncoding(UTF8_CHARSET);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(status);
            writer = response.getWriter();
            writer.write(result);
            writer.flush();
        } catch (IOException e) {
            log.error("Abnormal response output [{}]", e.getMessage());
        } finally {
            if (Objects.nonNull(writer)) {
                writer.close();
            }
        }
    }

    /**
     * 构建异常信息.
     *
     * @param ex      异常
     * @param request 请求
     * @return String
     */
    public static String buildMessage(Throwable ex, HttpServletRequest request) {
        StringBuilder message = new StringBuilder("Failed to handle request [");
        message.append(request.getMethod());
        message.append(" ");
        message.append(request.getRequestURI());
        message.append("]");
        if (Objects.nonNull(ex) && StringUtils.isNotBlank(ex.getMessage())) {
            message.append(": ");
            message.append(ex.getMessage());
        }
        return message.toString();
    }
}
