/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.objenesis.ObjenesisStd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 对象复制工具类.
 *
 * @author zengdegui
 * @since 2021/9/7
 */
public class BeanCopierUtil {
    private static ThreadLocal<ObjenesisStd> objenesisStdThreadLocal = ThreadLocal.withInitial(ObjenesisStd::new);
    private static ConcurrentHashMap<Class<?>, ConcurrentHashMap<Class<?>, BeanCopier>> cache = new ConcurrentHashMap<>(16);


    /**
     * 对象复制.
     *
     * @param source 源头
     * @param target 目标
     * @param <T>    泛型
     * @return T
     */
    public static <T> T copy(Object source, Class<T> target) {
        return copy(source, objenesisStdThreadLocal.get().newInstance(target));
    }

    /**
     * 对象复制.
     *
     * @param source 源头
     * @param target 目标
     * @param <T>    泛型
     * @return T
     */
    public static <T> T copy(Object source, T target) {
        BeanCopier beanCopier = getCacheBeanCopier(source.getClass(), target.getClass());
        beanCopier.copy(source, target, null);
        return target;
    }

    /**
     * 普通copy集合.
     *
     * @param sources 源头
     * @param target  目标
     * @param <T>     泛型
     * @return List<T>
     */
    public static <T> List<T> copyList(List<?> sources, Class<T> target) {
        if (sources.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<T> list = new ArrayList<>(sources.size());
        ObjenesisStd objenesisStd = objenesisStdThreadLocal.get();
        for (Object source : sources) {
            T newInstance = objenesisStd.newInstance(target);
            BeanCopier beanCopier = getCacheBeanCopier(source.getClass(), target);
            beanCopier.copy(source, newInstance, null);
            list.add(newInstance);
        }
        return list;
    }

    /**
     * map转bean.
     *
     * @param source 源头
     * @param target 目标
     * @param <T>    泛型
     * @return T
     */
    public static <T> T mapToBean(Map<String, ?> source, Class<T> target) {
        T bean = objenesisStdThreadLocal.get().newInstance(target);
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(source);
        return bean;
    }

    /**
     * bean转map.
     *
     * @param source 源头
     * @param <T>    泛型
     * @return Map<String, ?>
     */
    public static <T> Map<String, ?> beanToMap(T source) {
        Map<String, Object> map = new HashMap<>(16);
        BeanMap beanMap = BeanMap.create(source);
        for (Object key : beanMap.keySet()) {
            map.put(key + "", beanMap.get(key));
        }
        return map;
    }

    /**
     * 带缓存.
     *
     * @param source 源头
     * @param target 模板
     * @param <S>    泛型
     * @param <T>    泛型
     * @return BeanCopier
     */
    private static <S, T> BeanCopier getCacheBeanCopier(Class<S> source, Class<T> target) {
        ConcurrentHashMap<Class<?>, BeanCopier> copierConcurrentHashMap = cache.computeIfAbsent(source, aClass -> new ConcurrentHashMap<>(16));
        return copierConcurrentHashMap.computeIfAbsent(target, aClass -> BeanCopier.create(source, target, false));
    }
}
