/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util.domain.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * ip获取所在省市区信息对象.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Builder
@Data
public class IpVo implements Serializable {
    /**
     * IP地址.
     */
    private String ip;

    /**
     * 省份.
     */
    private String province;

    /**
     * 城市.
     */
    private String city;
}
