/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

/**
 * MultipartFile，File互转工具类.
 *
 * @author zengdegui
 * @since 2021/1/11
 */
@Slf4j
public class FileUtil extends cn.hutool.core.io.FileUtil {
    private static final int BSIZE = 8192;

    /**
     * File转MultipartFile.
     *
     * @param file 文件
     * @return MultipartFile
     */
    public static MultipartFile fileToMultipartFile(File file) {
        InputStream inputStream = null;
        MultipartFile multipartFile = null;
        try {
            inputStream = new FileInputStream(file);
            multipartFile = new MockMultipartFile(file.getName(), inputStream);
        } catch (IOException e) {
            log.error("IO Exception [{}]", e.getMessage());
        } finally {
            closeInputStream(inputStream);
        }
        return multipartFile;
    }

    /**
     * MultipartFile转File.
     *
     * @param file 文件
     * @return File
     */
    public static File multipartFileToFile(MultipartFile file) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        File toFile = null;
        try {
            inputStream = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            outputStream = new FileOutputStream(toFile);
            int bytesRead = 0;
            byte[] buffer = new byte[BSIZE];
            while ((bytesRead = inputStream.read(buffer, 0, BSIZE)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            log.error("IO Exception [{}]", e.getMessage());
        } finally {
            closeInputStream(inputStream);
            closeOutputStream(outputStream);
        }
        return toFile;
    }

    private static void closeInputStream(InputStream inputStream) {
        if (Objects.nonNull(inputStream)) {
            try {
                inputStream.close();
            } catch (IOException e) {
                log.error("Closing input stream exception [{}]", e.getMessage());
            }
        }
    }

    private static void closeOutputStream(OutputStream outputStream) {
        if (Objects.nonNull(outputStream)) {
            try {
                outputStream.close();
            } catch (IOException e) {
                log.error("Closing output stream exception [{}]", e.getMessage());
            }
        }
    }
}
