package com.iwindplus.boot.util;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Objects;

/**
 * 模板变量替换工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class TemplateUtil {

    /**
     * freemark模板变量替换.
     *
     * @param data            数据
     * @param templateContent 模板内容
     * @return String
     * @throws Exception
     */
    public static String getTemplateContent(Map<String, Object> data, String templateContent) throws Exception {
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        String template = "content";
        stringLoader.putTemplate(template, templateContent);
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
        cfg.setTemplateLoader(stringLoader);
        StringWriter writer = null;
        try {
            Template templateCon = cfg.getTemplate(template, "UTF-8");
            writer = new StringWriter();
            templateCon.process(data, writer);
            return writer.toString();
        } finally {
            if (Objects.nonNull(writer)) {
                try {
                    writer.close();
                } catch (IOException e) {
                    log.error("IOException [{}]", e.getMessage());
                }
            }
        }
    }
}
