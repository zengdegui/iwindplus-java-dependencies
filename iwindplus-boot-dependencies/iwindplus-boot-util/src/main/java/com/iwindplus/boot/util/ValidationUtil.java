/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import org.apache.commons.collections4.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * validate校验工具类.
 *
 * @author zengdegui
 * @since 2021/8/2
 */
public class ValidationUtil extends cn.hutool.extra.validation.ValidationUtil {
    /**
     * 校验对象.
     *
     * @param obj       对象
     * @param validator 国际化
     * @param groups    分组
     * @param <T>       对象
     * @return String
     */
    public static <T> String validateEntity(T obj, Validator validator, Class<?>... groups) {
        Set<ConstraintViolation<T>> validate = validator.validate(obj, groups);
        String message = validate.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(","));
        return CollectionUtils.isNotEmpty(validate) ? message : null;
    }
}
