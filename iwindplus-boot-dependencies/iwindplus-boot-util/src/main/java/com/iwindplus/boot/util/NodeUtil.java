/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.util;

import cn.hutool.core.lang.tree.TreeUtil;
import com.iwindplus.boot.util.domain.vo.BaseNodeVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * 树形无限极工具类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public class NodeUtil extends TreeUtil {
    /**
     * 封装树形结构.
     *
     * @param <TT>  泛型
     * @param nodes 节点集合
     * @return List<TT>
     */
    public static <TT extends BaseNodeVO<TT>> List<TT> listTree(List<TT> nodes) {
        return listRootNodes(nodes);
    }

    /**
     * 找出根节点.
     *
     * @param <TT>  泛型
     * @param nodes 节点集合
     * @return List<TT>
     */
    private static <TT extends BaseNodeVO<TT>> List<TT> listRootNodes(List<TT> nodes) {
        if (CollectionUtils.isEmpty(nodes)) {
            return null;
        }
        if (nodes.size() == 1) {
            return nodes;
        }
        // 返回的节点树
        List<TT> rootNodes = new ArrayList<>(10);
        Iterator<TT> it = nodes.iterator();
        while (it.hasNext()) {
            TT node = it.next();
            // parent（上级Id）为空或0的是根节点
            if (Objects.isNull(node.getParentId()) || StringUtils.equalsIgnoreCase("0",
                    node.getParentId())) {
                rootNodes.add(node);
                it.remove();
            }
        }
        if (CollectionUtils.isNotEmpty(rootNodes)) {
            rootNodes.stream().forEach(node -> {
                List<TT> childNodes = listChildNodes(node, nodes);
                node.setNodes(childNodes);
            });
            // 对子节点进行排序
            Collections.sort(rootNodes, Comparator.comparing(TT::getSeq));
            return rootNodes;
        }
        return rootNodes;
    }

    /**
     * 找出子节点.
     *
     * @param parentNode 父节点对象
     * @param nodes      节点集合
     * @param <TT>       泛型
     * @return List<TT>
     */
    private static <TT extends BaseNodeVO<TT>> List<TT> listChildNodes(TT parentNode, List<TT> nodes) {
        // 子节点列表
        List<TT> childList = new ArrayList<>(10);
        Iterator<TT> it = nodes.iterator();
        while (it.hasNext()) {
            TT node = it.next();
            if (Objects.nonNull(node.getParentId())
                    && Objects.nonNull(parentNode.getId())
                    && StringUtils.equalsIgnoreCase(node.getParentId(), parentNode.getId())) {
                childList.add(node);
                it.remove();
            }
        }
        if (CollectionUtils.isNotEmpty(childList)) {
            childList.stream().forEach(node -> {
                List<TT> childNodes = listChildNodes(node, nodes);
                node.setNodes(childNodes);
            });
            // 对子节点进行排序
            Collections.sort(childList, Comparator.comparing(TT::getSeq));
            return childList;
        }
        return childList;
    }
}
