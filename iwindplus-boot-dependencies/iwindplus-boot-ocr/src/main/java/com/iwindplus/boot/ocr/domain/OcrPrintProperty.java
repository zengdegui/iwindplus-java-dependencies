/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 印刷文字ocr相关属性.
 *
 * @author zengdegui
 * @since 2020/9/20
 */
@Data
@ConfigurationProperties(prefix = "ocr.print", ignoreUnknownFields = true)
public class OcrPrintProperty {
    /**
     * 认证code.
     */
    private String appCode;
}
