/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.domain.constant;

/**
 * ocr常数.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
public class OcrConstant {
    /**
     * 翔云身份证图片识别接口地址.
     */
    public static final String XIANGYUN_ID_CARD_URL = "https://netocr.com/api/recog.do";

    /**
     * 翔云营业执照图片识别接口地址.
     */
    public static final String XIANGYUN_BUSINESS_LICENSE_URL = "https://netocr.com/api/recoglen.do";

    /**
     * 印刷文字身份证图片识别接口地址.
     */
    public static final String PRINT_ID_CARD_URL = "https://cardnumber.market.alicloudapi.com/rest/160601/ocr/ocr_idcard.json";

    /**
     * 印刷文字营业执照图片识别接口地址.
     */
    public static final String PRINT_BUSINESS_LICENSE_URL = "https://bizlicense.market.alicloudapi.com/rest/160601/ocr/ocr_business_license.json";
}
