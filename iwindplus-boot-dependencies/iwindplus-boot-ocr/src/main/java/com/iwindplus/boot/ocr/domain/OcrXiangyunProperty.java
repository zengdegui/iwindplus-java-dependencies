/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 翔云ocr相关属性.
 *
 * @author zengdegui
 * @since 2020/9/20
 */
@Data
@ConfigurationProperties(prefix = "ocr.xiangyun", ignoreUnknownFields = true)
public class OcrXiangyunProperty {
    /**
     * 访问key.
     */
    private String accessKey;

    /**
     * 密匙.
     */
    private String secretKey;
}
