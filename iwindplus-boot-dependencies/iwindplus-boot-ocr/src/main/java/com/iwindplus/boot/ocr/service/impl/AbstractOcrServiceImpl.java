/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.service.impl;

import com.iwindplus.boot.ocr.domain.enumerate.OcrCodeEnum;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * ocr业务抽象类.
 *
 * @author zengdegui
 * @since 2020/3/13
 */
@Slf4j
public abstract class AbstractOcrServiceImpl {
    /**
     * png格式.
     */
    public static final String IMG_TYPE_PNG = "PNG";

    /**
     * jpg格式.
     */
    public static final String IMG_TYPE_JPG = "JPG";

    /**
     * jpeg格式.
     */
    public static final String IMG_TYPE_JPEG = "JPEG";

    /**
     * bmp格式.
     */
    public static final String IMG_TYPE_DMG = "BMP";

    /**
     * gif格式.
     */
    public static final String IMG_TYPE_GIF = "GIF";

    /**
     * svg格式.
     */
    public static final String IMG_TYPE_SVG = "SVG";

    /**
     * 文件最大大小.
     */
    public static final Long MAX_FILE_SIZE = 3L;

    /**
     * 校验文件是否是图片.
     *
     * @param file 文件
     */
    protected void checkIsImage(MultipartFile file) {
        String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        if (!(IMG_TYPE_DMG.equals(suffix.toUpperCase()) ||
                IMG_TYPE_GIF.equals(suffix.toUpperCase()) ||
                IMG_TYPE_JPEG.equals(suffix.toUpperCase()) ||
                IMG_TYPE_JPG.equals(suffix.toUpperCase()) ||
                IMG_TYPE_PNG.equals(suffix.toUpperCase()) ||
                IMG_TYPE_SVG.equals(suffix.toUpperCase()))) {
            throw new BaseException(OcrCodeEnum.FILE_IS_NOT_IMAGE);
        }
    }

    /**
     * 校验文件大小.
     *
     * @param fileSize    文件大小
     * @param maxFileSize 最大文件大小
     */
    protected void checkFile(long fileSize, long maxFileSize) {
        if (maxFileSize > 0 & fileSize > maxFileSize) {
            log.error("The file is too large,the file size [{}]", fileSize);
            String message = new StringBuilder("文件太大，不能超过").append(maxFileSize / (1024 * 1024)).append("兆").toString();
            throw new BaseException(OcrCodeEnum.FILE_TOO_BIG.value(), message);
        }
    }

    /**
     * 关闭Response.
     *
     * @param response 响应
     */
    protected void closeResponse(Response response) {
        if (Objects.nonNull(response)) {
            response.close();
        }
    }
}
