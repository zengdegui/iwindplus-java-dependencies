/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.iwindplus.boot.ocr.domain.enumerate.OcrPrintIdTypeEnum;
import org.springframework.web.multipart.MultipartFile;

/**
 * 印刷文字ocr业务层接口类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface OcrPrintService {
    /**
     * 识别身份证图片的内容.
     *
     * @param file   文件
     * @param idType 证件类型
     * @return JsonNode
     */
    JsonNode parseIdCardImage(MultipartFile file, OcrPrintIdTypeEnum idType);

    /**
     * 识别营业执照图片的内容.
     *
     * @param file 文件
     * @return JsonNode
     */
    JsonNode parseBusinessLicenseImage(MultipartFile file);
}
