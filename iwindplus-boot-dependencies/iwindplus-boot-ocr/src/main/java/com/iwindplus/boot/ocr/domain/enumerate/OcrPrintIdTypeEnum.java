/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.ocr.domain.enumerate;

import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 印刷文字ocr证件类型枚举.
 *
 * @author zengdegui
 * @since 2018/10/11
 */
@Getter
@Accessors(fluent = true)
public enum OcrPrintIdTypeEnum {
    /**
     * 二代身份证正面.
     */
    SECOND_ID_CARD_FRONT("face", "二代身份证正面"),

    /**
     * 二代身份证背面.
     */
    SECOND_ID_CARD_BACK("back", "二代身份证背面"),
    ;

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return OssQiniuZoneEnum
     */
    private OcrPrintIdTypeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return OcrXiangyunIdTypeEnum
     */
    public static OcrPrintIdTypeEnum valueOfValue(String value) {
        for (OcrPrintIdTypeEnum val : OcrPrintIdTypeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }
}
