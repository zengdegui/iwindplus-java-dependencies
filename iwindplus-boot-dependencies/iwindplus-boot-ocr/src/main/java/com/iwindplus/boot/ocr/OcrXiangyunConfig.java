/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr;

import com.iwindplus.boot.ocr.service.OcrXiangyunService;
import com.iwindplus.boot.ocr.service.impl.OcrXiangyunServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 翔云ocr配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OcrXiangyunConfig {
    /**
     * 创建 OcrXiangyunService.
     *
     * @return OcrXiangyunService
     */
    @Bean
    public OcrXiangyunService ocrXiangyunService() {
        OcrXiangyunServiceImpl ocrXiangyunService = new OcrXiangyunServiceImpl();
        log.info("OcrXiangyunService [{}]", ocrXiangyunService);
        return ocrXiangyunService;
    }
}
