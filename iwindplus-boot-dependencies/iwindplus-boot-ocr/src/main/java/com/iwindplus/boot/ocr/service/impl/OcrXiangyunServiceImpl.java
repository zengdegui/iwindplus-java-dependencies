/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.ocr.domain.OcrXiangyunProperty;
import com.iwindplus.boot.ocr.domain.constant.OcrConstant;
import com.iwindplus.boot.ocr.domain.enumerate.OcrCodeEnum;
import com.iwindplus.boot.ocr.domain.enumerate.OcrXiangyunIdTypeEnum;
import com.iwindplus.boot.ocr.service.OcrXiangyunService;
import com.iwindplus.boot.util.OkhttpUtil;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 翔云ocr业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OcrXiangyunServiceImpl extends AbstractOcrServiceImpl implements OcrXiangyunService {
    /**
     * 营业执照.
     */
    public static final Integer BUSINESS_LICENSE = 2008;

    /**
     * OcrXiangyunProperty.
     */
    @Autowired
    protected OcrXiangyunProperty ocrXiangyunProperty;

    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Override
    public JsonNode parseIdCardImage(MultipartFile file, OcrXiangyunIdTypeEnum idType) {
        Map<String, Object> bodyMap = new HashMap<>(16);
        bodyMap.put("key", this.ocrXiangyunProperty.getAccessKey());
        bodyMap.put("secret", this.ocrXiangyunProperty.getSecretKey());
        bodyMap.put("format", "json");
        bodyMap.put("typeId", idType.value());
        return getJsonNode(OcrConstant.XIANGYUN_ID_CARD_URL, file, bodyMap);
    }

    @Override
    public JsonNode parseBusinessLicenseImage(MultipartFile file) {
        Map<String, Object> bodyMap = new HashMap<>(16);
        bodyMap.put("key", this.ocrXiangyunProperty.getAccessKey());
        bodyMap.put("secret", this.ocrXiangyunProperty.getSecretKey());
        bodyMap.put("format", "json");
        bodyMap.put("typeId", BUSINESS_LICENSE);
        bodyMap.put("outvalue", 0);
        return getJsonNode(OcrConstant.XIANGYUN_BUSINESS_LICENSE_URL, file, bodyMap);
    }

    private JsonNode getJsonNode(String url, MultipartFile file, Map<String, Object> bodyMap) {
        if (Objects.isNull(file)) {
            throw new BaseException(OcrCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.getSize(), DataSize.of(MAX_FILE_SIZE, DataUnit.MEGABYTES).toBytes());
        super.checkIsImage(file);
        Response response = null;
        try {
            List<MultipartFile> fileList = new ArrayList<>(10);
            fileList.add(file);
            RequestBody requestBody = OkhttpUtil.getRequestFromBody(bodyMap, fileList);
            response = OkhttpUtil.sendPost(url, null, requestBody);
            if (Objects.nonNull(response)) {
                String result = response.body().string();
                if (response.isSuccessful()) {
                    JsonNode data = this.objectMapper.readTree(result);
                    int status = data.get("message").get("status").asInt();
                    if (status >= 0) {
                        return data;
                    }
                }
                log.error("识别错误 [{}]", this.objectMapper.writeValueAsString(result));
            }
        } catch (IOException e) {
            log.error("IOException [{}]", e.getMessage());
        } finally {
            super.closeResponse(response);
        }
        throw new BaseException(OcrCodeEnum.IMAGE_DISTINGUISH_FAILED);
    }
}
