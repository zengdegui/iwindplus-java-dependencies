/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.ocr.domain.enumerate;

import com.iwindplus.boot.web.exception.CommonException;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 业务编码返回值枚举.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
@Getter
@Accessors(fluent = true)
public enum OcrCodeEnum implements CommonException {
    /**
     * 文件找不到.
     */
    FILE_NOT_FOUND("file_not_found", "文件找不到"),

    /**
     * 文件不是图片.
     */
    FILE_IS_NOT_IMAGE("file_is_not_image", "文件不是图片"),

    /**
     * 文件太大.
     */
    FILE_TOO_BIG("file_too_big", "文件太大"),

    /**
     * 图片识别失败.
     */
    IMAGE_DISTINGUISH_FAILED("image_distinguish_failed", "图片识别失败"),
    ;

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 构造方法.
     *
     * @param value 值
     * @param desc  描述
     */
    OcrCodeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return OssCodeEnum
     */
    public static OcrCodeEnum valueOfDesc(String desc) {
        for (OcrCodeEnum val : OcrCodeEnum.values()) {
            if (Objects.equals(desc, val.desc())) {
                return val;
            }
        }
        return null;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return OssCodeEnum
     */
    public static OcrCodeEnum valueOfValue(String value) {
        for (OcrCodeEnum val : OcrCodeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.value;
    }

    @Override
    public String getMessage() {
        return this.desc;
    }
}
