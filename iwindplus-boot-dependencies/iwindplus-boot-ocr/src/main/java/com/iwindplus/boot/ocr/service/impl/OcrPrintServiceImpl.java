/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.ocr.domain.OcrPrintProperty;
import com.iwindplus.boot.ocr.domain.constant.OcrConstant;
import com.iwindplus.boot.ocr.domain.enumerate.OcrCodeEnum;
import com.iwindplus.boot.ocr.domain.enumerate.OcrPrintIdTypeEnum;
import com.iwindplus.boot.ocr.service.OcrPrintService;
import com.iwindplus.boot.util.OkhttpUtil;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 印刷文字ocr业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OcrPrintServiceImpl extends AbstractOcrServiceImpl implements OcrPrintService {
    /**
     * OcrPrintProperty.
     */
    @Autowired
    protected OcrPrintProperty ocrPrintProperty;

    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Override
    public JsonNode parseIdCardImage(MultipartFile file, OcrPrintIdTypeEnum idType) {
        Map<String, Object> bodyMap = new HashMap<>(16);
        Map<String, Object> childrenMap = new HashMap<>(16);
        childrenMap.put("side", idType.value());
        bodyMap.put("configure", childrenMap);
        return getJsonNode(OcrConstant.PRINT_ID_CARD_URL, file, bodyMap);
    }

    @Override
    public JsonNode parseBusinessLicenseImage(MultipartFile file) {
        Map<String, Object> bodyMap = new HashMap<>(16);
        return getJsonNode(OcrConstant.PRINT_BUSINESS_LICENSE_URL, file, bodyMap);
    }

    private JsonNode getJsonNode(String url, MultipartFile file, Map<String, Object> bodyMap) {
        if (Objects.isNull(file)) {
            throw new BaseException(OcrCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.getSize(), DataSize.of(MAX_FILE_SIZE, DataUnit.MEGABYTES).toBytes());
        super.checkIsImage(file);
        Response response = null;
        try {
            Map<String, String> headerMap = new HashMap<>(16);
            String authorization = new StringBuilder("APPCODE ").append(this.ocrPrintProperty.getAppCode()).toString();
            headerMap.put("Authorization", authorization);
            bodyMap.put("image", Base64.getEncoder().encodeToString(file.getBytes()));
            RequestBody requestBody = OkhttpUtil.getRequestJsonBody(this.objectMapper.writeValueAsString(bodyMap));
            response = OkhttpUtil.sendPost(url, Headers.of(headerMap), requestBody);
            if (Objects.nonNull(response)) {
                String result = response.body().string();
                if (response.isSuccessful()) {
                    JsonNode data = this.objectMapper.readTree(result);
                    boolean status = data.get("success").asBoolean();
                    if (status) {
                        return data;
                    }
                }
                log.error("识别错误 [{}]", this.objectMapper.writeValueAsString(result));
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
        } finally {
            super.closeResponse(response);
        }
        throw new BaseException(OcrCodeEnum.IMAGE_DISTINGUISH_FAILED);
    }
}
