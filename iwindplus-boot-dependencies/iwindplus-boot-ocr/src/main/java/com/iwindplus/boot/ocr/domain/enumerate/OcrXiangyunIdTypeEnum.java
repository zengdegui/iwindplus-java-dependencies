/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.ocr.domain.enumerate;

import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 翔云ocr证件类型枚举.
 *
 * @author zengdegui
 * @since 2018/10/11
 */
@Getter
@Accessors(fluent = true)
public enum OcrXiangyunIdTypeEnum {
    /**
     * 二代身份证正面.
     */
    SECOND_ID_CARD_FRONT(2, "二代身份证正面"),

    /**
     * 二代身份证背面.
     */
    SECOND_ID_CARD_BACK(3, "二代身份证背面"),
    ;

    /**
     * 值.
     */
    private final Integer value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return OssQiniuZoneEnum
     */
    private OcrXiangyunIdTypeEnum(final Integer value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return OcrXiangyunIdTypeEnum
     */
    public static OcrXiangyunIdTypeEnum valueOfValue(Integer value) {
        for (OcrXiangyunIdTypeEnum val : OcrXiangyunIdTypeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }
}
