/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.ocr;

import com.iwindplus.boot.ocr.service.OcrPrintService;
import com.iwindplus.boot.ocr.service.impl.OcrPrintServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 印刷文字ocr配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OcrPrintConfig {
    /**
     * 创建 OcrPrintService.
     *
     * @return OcrPrintService
     */
    @Bean
    public OcrPrintService ocrPrintService() {
        OcrPrintServiceImpl ocrPrintService = new OcrPrintServiceImpl();
        log.info("OcrPrintService [{}]", ocrPrintService);
        return ocrPrintService;
    }
}
