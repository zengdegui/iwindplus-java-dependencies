ocr模块
OcrConstant 定义了ocr相关地址
OcrXiangyunIdTypeEnum 定义了证件类型参数
一、翔云ocr对接流程
   1、启动类开启注解@EnableOcrXiangyun
   2、在配置文件（yml,properties）中配置OcrXiangyunProperty类中相关属性
   3、程序中注入OcrXiangyunService，调用该类中方法
