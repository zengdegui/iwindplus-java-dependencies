/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms;

import com.iwindplus.boot.sms.service.SmsMxtongService;
import com.iwindplus.boot.sms.service.impl.SmsMxtongServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 麦讯通短信配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsMxtongConfig {
    /**
     * 创建 SmsMxtongService.
     *
     * @return SmsMxtongService
     */
    @Bean
    public SmsMxtongService smsMxtongService() {
        SmsMxtongServiceImpl smsService = new SmsMxtongServiceImpl();
        log.info("SmsMxtongService [{}]", smsService);
        return smsService;
    }
}
