/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service;

import com.iwindplus.boot.sms.domain.vo.SmsLogVO;

import java.util.List;

/**
 * 七牛云短信业务层接口类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface SmsQiniuService {
    /**
     * 发送短信验证码.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param phoneNumber    手机（必填）
     * @param templateCode   短信模板（短信要发送的内容，可包含变量，变量示例：${name}，只能有一个参数）
     * @param captchaLength  短信验证码长度（默认：6）
     * @param captchaTimeout 短信验证码有效时间（单位：分钟，默认：10）
     * @return SmsLogVO
     */
    SmsLogVO smsSendCaptcha(String accessKey, String secretKey, String phoneNumber,
                            String templateCode, Integer captchaLength, Integer captchaTimeout);

    /**
     * 发送短信，成功返回流水号.
     *
     * @param accessKey            访问key（必填）
     * @param secretKey            密钥（必填）
     * @param phoneNumbers         手机号集合（必填）
     * @param templateCode         模板CODE（必填）
     * @param templateParamValue   模板参数的值（可选，短信模板如果包含变量则必填，按模板变量个数依次赋值）
     * @param phoneNumberGroupSize 每个分组的手机个数（可选，默认：100）
     * @return List<String>
     */
    List<String> smsSend(String accessKey, String secretKey, List<String> phoneNumbers, String templateCode,
                         List<String> templateParamValue, Integer phoneNumberGroupSize);
}
