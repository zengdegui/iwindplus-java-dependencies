/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.sms.domain.enumerate.SmsCodeEnum;
import com.iwindplus.boot.sms.domain.vo.SmsLogVO;
import com.iwindplus.boot.sms.service.SmsAliyunService;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 阿里云短信业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsAliyunServiceImpl extends AbstractSmsServiceImpl implements SmsAliyunService {
    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Override
    public SmsLogVO smsSendCaptcha(String accessKey, String secretKey, String phoneNumber, String signName,
                                   String templateCode, Integer captchaLength, Integer captchaTimeout) {
        List<String> phoneNumbers = new ArrayList<>(10);
        phoneNumbers.add(phoneNumber);
        List<String> templateParamValue = new ArrayList<>(10);
        String captcha = RandomUtil.randomNumbers(Objects.isNull(captchaLength) ? CAPTCHA_LENGTH : captchaLength);
        templateParamValue.add(captcha);
        String bizId = this.getBizId(accessKey, secretKey, phoneNumbers, signName, templateCode, templateParamValue, null);
        LocalDateTime expireTime = LocalDateTime.now().plusMinutes(Objects.isNull(captchaTimeout) ? CAPTCHA_TIMEOUT : captchaTimeout);
        return SmsLogVO
                .builder()
                .bizId(bizId)
                .mobile(phoneNumber)
                .captcha(captcha)
                .expireTime(expireTime)
                .build();
    }

    @Override
    public List<String> smsSend(String accessKey, String secretKey, List<String> phoneNumbers, String signName, String templateCode,
                                List<String> templateParamValue, String smsUpExtendCode, Integer phoneNumberGroupSize) {
        List<String> list = new ArrayList<>(10);
        int subLength = Objects.isNull(phoneNumberGroupSize) ? GROUP_SIZE : phoneNumberGroupSize;
        for (int i = 0; i < phoneNumbers.size(); i += subLength) {
            List<String> subPhoneNumbers = phoneNumbers.stream().skip(i).limit(subLength).collect(Collectors.toList());
            // 发送短信
            String bizId = this.getBizId(accessKey, secretKey, subPhoneNumbers, signName, templateCode, templateParamValue, smsUpExtendCode);
            list.add(bizId);
        }
        return list;
    }

    /**
     * 发送短信，成功返回流水号.
     *
     * @param accessKey          访问key
     * @param secretKey          密钥
     * @param phoneNumbers       手机号集合
     * @param signName           签名名称
     * @param templateCode       模板CODE
     * @param templateParamValue 模板参数的值（可选）
     * @param smsUpExtendCode    上行短信扩展码，上行短信，指发送给通信服务提供商的短信，用于定制某种服务、完成查询，或是办理某种业务等，需要收费的，按运营商普通短信资费进行扣费。（可选）
     * @return String
     */
    private String getBizId(String accessKey, String secretKey, List<String> phoneNumbers, String signName,
                            String templateCode, List<String> templateParamValue, String smsUpExtendCode) {
        CommonRequest commonRequest = new CommonRequest();
        commonRequest.setSysAction("SendSms");
        commonRequest.putQueryParameter("SignName", signName);
        commonRequest.putQueryParameter("TemplateCode", templateCode);
        String templateParamJson = this.getTemplateParamJson(accessKey, secretKey, templateCode, templateParamValue);
        if (StringUtils.isNotBlank(templateParamJson)) {
            commonRequest.putQueryParameter("TemplateParam", templateParamJson);
        }
        if (StringUtils.isNotBlank(smsUpExtendCode)) {
            commonRequest.putQueryParameter("SmsUpExtendCode", smsUpExtendCode);
        }
        commonRequest.putQueryParameter("PhoneNumbers", StringUtils.join(phoneNumbers.toArray(), ","));
        CommonResponse commonResponse = this.getCommonResponse(accessKey, secretKey, commonRequest);
        JsonNode sendSmsResponse = this.getJsonNode(commonResponse);
        if (Objects.isNull(sendSmsResponse)) {
            throw new BaseException(SmsCodeEnum.SEND_FAILED);
        }
        return sendSmsResponse.get("BizId").asText();
    }

    /**
     * 查询短信模板，并替换模板中的变量，返回模板参数json.
     *
     * @param accessKey          访问key
     * @param secretKey          密钥
     * @param templateCode       模板CODE
     * @param templateParamValue 模板参数的值
     * @return String
     */
    private String getTemplateParamJson(String accessKey, String secretKey, String templateCode, List<String> templateParamValue) {
        CommonRequest commonRequest = new CommonRequest();
        commonRequest.setSysAction("QuerySmsTemplate");
        commonRequest.putQueryParameter("TemplateCode", templateCode);
        CommonResponse commonResponse = this.getCommonResponse(accessKey, secretKey, commonRequest);
        JsonNode querySmsTemplateJson = this.getJsonNode(commonResponse);
        if (Objects.nonNull(querySmsTemplateJson)) {
            String templateContent = querySmsTemplateJson.get("TemplateContent").asText();
            Map<String, Object> templateParam = super.getTemplateParamMap(templateContent, templateParamValue);
            if (MapUtils.isNotEmpty(templateParam)) {
                try {
                    return this.objectMapper.writeValueAsString(templateParam);
                } catch (JsonProcessingException ex) {
                    log.error("JsonProcessingException [{}]", ex.getMessage());
                }
            }
        }
        return null;
    }

    private CommonResponse getCommonResponse(String accessKey, String secretKey, CommonRequest request) {
        DefaultProfile profile = DefaultProfile.getProfile("default", accessKey, secretKey);
        IAcsClient client = new DefaultAcsClient(profile);
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        try {
            CommonResponse response = client.getCommonResponse(request);
            log.info("Aliyun SMS return value [{}]", this.objectMapper.writeValueAsString(response));
            return response;
        } catch (Exception e) {
            log.error("Aliyun SMS Exception [{}]", e.getMessage());
        }
        return null;
    }

    private JsonNode getJsonNode(CommonResponse response) {
        if (Objects.nonNull(response) && response.getHttpStatus() == 200) {
            try {
                JsonNode data = this.objectMapper.readTree(response.getData());
                if (Objects.nonNull(data)) {
                    String code = data.get("Code").asText();
                    if (StringUtils.equals("OK", code)) {
                        return data;
                    } else if (StringUtils.equals("isv.MOBILE_NUMBER_ILLEGAL", code)) {
                        throw new BaseException(SmsCodeEnum.MOBILE_FORMAT_ERROR);
                    } else if (StringUtils.equals("isv.BUSINESS_LIMIT_CONTROL", code)) {
                        throw new BaseException(SmsCodeEnum.SEND_FREQUENCY_LIMIT);
                    } else if (StringUtils.equals("isv.PARAM_NOT_SUPPORT_URL", code)) {
                        throw new BaseException(SmsCodeEnum.TEMPLATE_PARAM_NOT_SUPPORT_URL);
                    } else if (StringUtils.equals("isv.AMOUNT_NOT_ENOUGH", code)) {
                        throw new BaseException(SmsCodeEnum.AMOUNT_NOT_ENOUGH);
                    }
                }
            } catch (JsonProcessingException e) {
                log.error("JsonProcessingException [{}]", e.getMessage());
            }
        }
        return null;
    }
}
