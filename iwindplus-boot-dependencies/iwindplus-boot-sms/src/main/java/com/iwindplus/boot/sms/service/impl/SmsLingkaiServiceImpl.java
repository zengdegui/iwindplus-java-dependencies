/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.iwindplus.boot.sms.domain.enumerate.SmsCodeEnum;
import com.iwindplus.boot.sms.domain.vo.SmsLogVO;
import com.iwindplus.boot.sms.service.SmsLingkaiService;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 凌凯短信业务层接口实现类
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsLingkaiServiceImpl extends AbstractSmsServiceImpl implements SmsLingkaiService {
    /**
     * 请求路径常数.
     */
    public static final String DEFAULT_URL = "https://mb345.com/ws/BatchSend2.aspx";

    @Override
    public SmsLogVO smsSendCaptcha(String username, String password, String phoneNumber, String templateCode, Integer captchaLength, Integer captchaTimeout) {
        List<String> phoneNumbers = new ArrayList<>(10);
        phoneNumbers.add(phoneNumber);
        List<String> templateParamValue = new ArrayList<>(10);
        String captcha = RandomUtil.randomNumbers(Objects.isNull(captchaLength) ? CAPTCHA_LENGTH : captchaLength);
        templateParamValue.add(captcha);
        this.getSmsResponse(username, password, phoneNumbers, templateCode, templateParamValue);
        LocalDateTime expireTime = LocalDateTime.now().plusMinutes(Objects.isNull(captchaTimeout) ? CAPTCHA_TIMEOUT : captchaTimeout);
        return SmsLogVO
                .builder()
                .bizId(null)
                .mobile(phoneNumber)
                .captcha(captcha)
                .expireTime(expireTime)
                .build();
    }

    @Override
    public boolean smsSend(String username, String password, List<String> phoneNumbers, String templateCode,
                           List<String> templateParamValue, Integer phoneNumberGroupSize) {
        boolean result = false;
        int subLength = Objects.isNull(phoneNumberGroupSize) ? GROUP_SIZE : phoneNumberGroupSize;
        for (int i = 0; i < phoneNumbers.size(); i += subLength) {
            List<String> subPhoneNumbers = phoneNumbers.stream().skip(i).limit(subLength).collect(Collectors.toList());
            result = this.getSmsResponse(username, password, subPhoneNumbers, templateCode, templateParamValue);
        }
        return result;
    }

    private boolean getSmsResponse(String username, String password, List<String> phoneNumbers,
                                   String templateCode, List<String> templateParamValue) {
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            String path = new StringBuilder(DEFAULT_URL)
                    .append("?CorpID=").append(username)
                    .append("&Pwd=").append(password)
                    .append("&Mobile=").append(StringUtils.join(phoneNumbers.toArray(), ","))
                    .append("&Content=").append(this.getTemplateContent(templateCode, templateParamValue))
                    .toString();
            isr = new InputStreamReader(new URL(path).openStream());
            br = new BufferedReader(isr);
            String data = br.readLine();
            if (StringUtils.isNotBlank(data) && Integer.valueOf(data).intValue() > 0) {
                log.info("lingkai SMS sending return value [{}]", data);
                return true;
            }
            return false;
        } catch (Exception e) {
            log.error("Lingkai SMS exception [{}]", e.getMessage());
            throw new BaseException(SmsCodeEnum.SEND_FAILED);
        } finally {
            closeInputStreamReader(isr);
            closeBufferedReader(br);
        }
    }

    private void closeInputStreamReader(InputStreamReader isr) {
        if (Objects.nonNull(isr)) {
            try {
                isr.close();
            } catch (IOException e) {
                log.error("IOException [{}]", e.getMessage());
            }
        }
    }

    private void closeBufferedReader(BufferedReader br) {
        if (Objects.nonNull(br)) {
            try {
                br.close();
            } catch (IOException e) {
                log.error("IOException [{}]", e.getMessage());
            }
        }
    }
}