/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.sms.domain.enumerate;

import com.iwindplus.boot.web.exception.CommonException;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 业务编码返回值枚举.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
@Getter
@Accessors(fluent = true)
public enum SmsCodeEnum implements CommonException {
    /**
     * 手机不存在.
     */
    MOBILE_NOT_EXIST("mobile_not_exist", "手机不存在"),

    /**
     * 验证码错误.
     */
    CAPTCHA_ERROR("captcha_error", "验证码错误"),

    /**
     * 验证码过期.
     */
    CAPTCHA_EXPIRED("captcha_expired", "验证码过期"),

    /**
     * 验证码未过期或未使用.
     */
    CAPTCHA_NOT_EXPIRED("captcha_not_expired", "验证码未过期或未使用，请勿重新发送"),

    /**
     * 验证码只能使用一次.
     */
    CAPTCHA_CAN_USE_ONCE("captcha_can_user_once", "验证码只能使用一次"),

    /**
     * 限制验证码每天发送次数.
     */
    CAPTCHA_LIMIT_EVERYDAY("captcha_limit_every_day", "限制验证码每天发送次数"),

    /**
     * 限制验证码每小时发送次数.
     */
    CAPTCHA_LIMIT_HOUR("captcha_limit_hour", "限制验证码每小时发送次数"),

    /**
     * 发送失败.
     */
    SEND_FAILED("send_failed", "发送失败"),

    /**
     * 使用同一个签名，对同一个手机号码发送短信验证码，支持1条/分钟，5条/小时 ，累计10条/天。
     */
    SEND_FREQUENCY_LIMIT("send_frequency_limit", "对同一个手机号码发送短信验证码，支持1条/分钟，5条/小时 ，累计10条/天"),

    /**
     * 模板参数不支持url.
     */
    TEMPLATE_PARAM_NOT_SUPPORT_URL("template_param_not_support_url", "模板参数不支持url"),

    /**
     * 账户余额不足.
     */
    AMOUNT_NOT_ENOUGH("amout_not_enough", "账户余额不足"),

    /**
     * 手机号码格式错误.
     */
    MOBILE_FORMAT_ERROR("mobile_format_error", "手机号码格式错误"),
    ;

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 构造方法.
     *
     * @param value 值
     * @param desc  描述
     */
    SmsCodeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return SmsCodeEnum
     */
    public static SmsCodeEnum valueOfDesc(String desc) {
        for (SmsCodeEnum val : SmsCodeEnum.values()) {
            if (Objects.equals(desc, val.desc())) {
                return val;
            }
        }
        return null;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return SmsCodeEnum
     */
    public static SmsCodeEnum valueOfValue(String value) {
        for (SmsCodeEnum val : SmsCodeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.value;
    }

    @Override
    public String getMessage() {
        return this.desc;
    }
}
