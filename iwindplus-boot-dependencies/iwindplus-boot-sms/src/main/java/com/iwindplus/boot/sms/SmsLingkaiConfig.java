/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms;

import com.iwindplus.boot.sms.service.SmsLingkaiService;
import com.iwindplus.boot.sms.service.impl.SmsLingkaiServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 凌凯短信配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsLingkaiConfig {
    /**
     * 创建 SmsLingkaiService.
     *
     * @return SmsLingkaiService
     */
    @Bean
    public SmsLingkaiService smsLingkaiService() {
        SmsLingkaiServiceImpl smsService = new SmsLingkaiServiceImpl();
        log.info("SmsLingkaiService [{}]", smsService);
        return smsService;
    }
}
