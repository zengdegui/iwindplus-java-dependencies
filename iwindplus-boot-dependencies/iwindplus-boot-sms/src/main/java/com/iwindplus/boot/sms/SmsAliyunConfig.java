/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms;

import com.iwindplus.boot.sms.service.SmsAliyunService;
import com.iwindplus.boot.sms.service.impl.SmsAliyunServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 阿里云短信配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsAliyunConfig {
    /**
     * 创建 SmsAliyunService.
     *
     * @return SmsAliyunService
     */
    @Bean
    public SmsAliyunService smsAliyunService() {
        SmsAliyunServiceImpl smsService = new SmsAliyunServiceImpl();
        log.info("SmsAliyunService [{}]", smsService);
        return smsService;
    }
}
