/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.RandomUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.sms.domain.enumerate.SmsCodeEnum;
import com.iwindplus.boot.sms.domain.vo.SmsLogVO;
import com.iwindplus.boot.sms.service.SmsQiniuService;
import com.iwindplus.boot.web.exception.BaseException;
import com.qiniu.http.Response;
import com.qiniu.sms.SmsManager;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 七牛云短信业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsQiniuServiceImpl extends AbstractSmsServiceImpl implements SmsQiniuService {
    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Override
    public SmsLogVO smsSendCaptcha(String accessKey, String secretKey, String phoneNumber, String templateCode, Integer captchaLength, Integer captchaTimeout) {
        List<String> phoneNumbers = new ArrayList<>(10);
        phoneNumbers.add(phoneNumber);
        List<String> templateParamValue = new ArrayList<>(10);
        String captcha = RandomUtil.randomNumbers(Objects.isNull(captchaLength) ? CAPTCHA_LENGTH : captchaLength);
        templateParamValue.add(captcha);
        String bizId = this.getBizId(accessKey, secretKey, phoneNumbers, templateCode, templateParamValue);
        LocalDateTime expireTime = LocalDateTime.now().plusMinutes(Objects.isNull(captchaTimeout) ? CAPTCHA_TIMEOUT : captchaTimeout);
        return SmsLogVO
                .builder()
                .bizId(bizId)
                .mobile(phoneNumber)
                .captcha(captcha)
                .expireTime(expireTime)
                .build();
    }

    @Override
    public List<String> smsSend(String accessKey, String secretKey, List<String> phoneNumbers, String templateCode, List<String> templateParamValue, Integer phoneNumberGroupSize) {
        List<String> list = new ArrayList<>(10);
        int subLength = Objects.isNull(phoneNumberGroupSize) ? GROUP_SIZE : phoneNumberGroupSize;
        for (int i = 0; i < phoneNumbers.size(); i += subLength) {
            List<String> subPhoneNumbers = phoneNumbers.stream().skip(i).limit(subLength).collect(Collectors.toList());
            // 发送短信
            String bizId = this.getBizId(accessKey, secretKey, subPhoneNumbers, templateCode, templateParamValue);
            list.add(bizId);
        }
        return list;
    }

    /**
     * 发送短信，成功返回流水号.
     *
     * @param accessKey          访问key
     * @param secretKey          密钥
     * @param phoneNumbers       手机号集合
     * @param templateCode       短信模板（短信要发送的内容，可包含变量，变量示例：${name}）
     * @param templateParamValue 模板参数的值（可选）
     * @return String
     */
    private String getBizId(String accessKey, String secretKey, List<String> phoneNumbers,
                            String templateCode, List<String> templateParamValue) {
        Auth auth = Auth.create(accessKey, secretKey);
        SmsManager smsManager = new SmsManager(auth);
        String[] mobiles = phoneNumbers.stream().toArray(String[]::new);
        Response response = null;
        try {
            Map<String, String> parameters = null;
            Map<String, Object> templateParam = super.getTemplateParamMap(templateCode, templateParamValue);
            if (MapUtils.isNotEmpty(templateParam)) {
                parameters = Convert.convert(new TypeReference<Map<String, String>>() {
                }, templateParam);
            }
            response = smsManager.sendMessage(templateCode,
                    mobiles, parameters);
            log.info("Qiniu SMS sending return value [{}]", this.objectMapper.writeValueAsString(response));
            if (Objects.nonNull(response) && response.isOK()) {
                JsonNode data = this.objectMapper.readTree(response.bodyString());
                if (Objects.nonNull(data)) {
                    return data.get("job_id").asText();
                }
            }
            return null;
        } catch (Exception ex) {
            log.error("Exception [{}]", ex.getMessage());
            throw new BaseException(SmsCodeEnum.SEND_FAILED);
        } finally {
            if (Objects.nonNull(response)) {
                response.close();
            }
        }
    }
}