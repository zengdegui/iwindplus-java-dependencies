/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service;

import com.iwindplus.boot.sms.domain.vo.SmsLogVO;

import java.util.List;

/**
 * 阿里云短信业务层接口类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface SmsAliyunService {
    /**
     * 发送短信验证码.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param phoneNumber    手机（必填）
     * @param signName       签名名称（必填）
     * @param templateCode   模板CODE（必填）
     * @param captchaLength  短信验证码长度（默认：6）
     * @param captchaTimeout 短信验证码有效时间（单位：分钟，默认：10）
     * @return SmsLogVO
     */
    SmsLogVO smsSendCaptcha(String accessKey, String secretKey, String phoneNumber, String signName,
                            String templateCode, Integer captchaLength, Integer captchaTimeout);

    /**
     * 发送短信，成功返回流水号.
     *
     * @param accessKey            访问key（必填）
     * @param secretKey            密钥（必填）
     * @param phoneNumbers         手机号集合（必填）
     * @param signName             签名名称（必填）
     * @param templateCode         模板CODE（必填）
     * @param templateParamValue   模板参数的值（可选）
     * @param smsUpExtendCode      上行短信扩展码，上行短信，指发送给通信服务提供商的短信，用于定制某种服务、完成查询，或是办理某种业务等，需要收费的，按运营商普通短信资费进行扣费。（可选）
     * @param phoneNumberGroupSize 每个分组的手机个数（可选，默认：100）
     * @return List<String>
     */
    List<String> smsSend(String accessKey, String secretKey, List<String> phoneNumbers, String signName, String templateCode,
                         List<String> templateParamValue, String smsUpExtendCode, Integer phoneNumberGroupSize);
}
