/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.sms.domain.enumerate.SmsCodeEnum;
import com.iwindplus.boot.sms.domain.vo.SmsLogVO;
import com.iwindplus.boot.sms.service.SmsMxtongService;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 麦讯通短信业务层接口实现类
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsMxtongServiceImpl extends AbstractSmsServiceImpl implements SmsMxtongService {
    /**
     * 请求路径常数.
     */
    public static final String DEFAULT_URL = "http://www.weiwebs.cn/msg/HttpBatchSendSM";

    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Override
    public SmsLogVO smsSendCaptcha(String username, String password, String phoneNumber, String templateCode, Integer captchaLength, Integer captchaTimeout) {
        List<String> phoneNumbers = new ArrayList<>(10);
        phoneNumbers.add(phoneNumber);
        List<String> templateParamValue = new ArrayList<>(10);
        String captcha = RandomUtil.randomNumbers(Objects.isNull(captchaLength) ? CAPTCHA_LENGTH : captchaLength);
        templateParamValue.add(captcha);
        String bizId = this.getSmsResponse(username, password, phoneNumbers, templateCode, templateParamValue);
        LocalDateTime expireTime = LocalDateTime.now().plusMinutes(Objects.isNull(captchaTimeout) ? CAPTCHA_TIMEOUT : captchaTimeout);
        return SmsLogVO
                .builder()
                .bizId(bizId)
                .mobile(phoneNumber)
                .captcha(captcha)
                .expireTime(expireTime)
                .build();
    }

    @Override
    public List<String> smsSend(String username, String password, List<String> phoneNumbers, String templateCode, List<String> templateParamValue, Integer phoneNumberGroupSize) {
        List<String> list = new ArrayList<>(10);
        int subLength = Objects.isNull(phoneNumberGroupSize) ? GROUP_SIZE : phoneNumberGroupSize;
        for (int i = 0; i < phoneNumbers.size(); i += subLength) {
            List<String> subPhoneNumbers = phoneNumbers.stream().skip(i).limit(subLength).collect(Collectors.toList());
            String bizId = this.getSmsResponse(username, password, subPhoneNumbers, templateCode, templateParamValue);
            list.add(bizId);
        }
        return list;
    }

    private String getSmsResponse(String username, String password, List<String> phoneNumbers,
                                  String templateCode, List<String> templateParamValue) {
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            String path = new StringBuilder(DEFAULT_URL)
                    .append("?account=").append(username)
                    .append("&pswd=").append(password)
                    .append("&mobile=").append(StringUtils.join(phoneNumbers.toArray(), ","))
                    .append("&msg=").append(this.getTemplateContent(templateCode, templateParamValue))
                    .append("&needstatus=true")
                    .append("&resptype=json")
                    .toString();
            isr = new InputStreamReader(new URL(path).openStream());
            br = new BufferedReader(isr);
            String response = br.readLine();
            log.info("Mxtong SMS sending return value [{}]", this.objectMapper.writeValueAsString(response));
            JsonNode data = this.objectMapper.readTree(response);
            if (Objects.nonNull(data) && data.get("result").asInt() == 0) {
                return data.get("msgid").asText();
            }
            return null;
        } catch (Exception e) {
            log.error("Mxtong SMS exception [{}]", e.getMessage());
            throw new BaseException(SmsCodeEnum.SEND_FAILED);
        } finally {
            closeInputStreamReader(isr);
            closeBufferedReader(br);
        }
    }

    private void closeInputStreamReader(InputStreamReader isr) {
        if (Objects.nonNull(isr)) {
            try {
                isr.close();
            } catch (IOException e) {
                log.error("IOException [{}]", e.getMessage());
            }
        }
    }

    private void closeBufferedReader(BufferedReader br) {
        if (Objects.nonNull(br)) {
            try {
                br.close();
            } catch (IOException e) {
                log.error("IOException [{}]", e.getMessage());
            }
        }
    }
}
