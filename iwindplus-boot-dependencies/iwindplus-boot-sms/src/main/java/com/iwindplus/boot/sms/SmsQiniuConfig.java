/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms;

import com.iwindplus.boot.sms.service.SmsQiniuService;
import com.iwindplus.boot.sms.service.impl.SmsQiniuServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 七牛云短信配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class SmsQiniuConfig {
    /**
     * 创建 SmsQiniuService.
     *
     * @return SmsQiniuService
     */
    @Bean
    public SmsQiniuService smsQiniuService() {
        SmsQiniuServiceImpl smsService = new SmsQiniuServiceImpl();
        log.info("SmsQiniuService [{}]", smsService);
        return smsService;
    }
}
