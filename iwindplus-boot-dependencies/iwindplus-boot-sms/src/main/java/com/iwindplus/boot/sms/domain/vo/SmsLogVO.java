/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 短信日志对象.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsLogVO implements Serializable {
	private static final long serialVersionUID = 5677984894610398895L;

	/**
	 * 业务流水号.
	 */
	private String bizId;

	/**
	 * 手机.
	 */
	private String mobile;

	/**
	 * 验证码.
	 */
	private String captcha;

	/**
	 * 过期时间.
	 */
	private LocalDateTime expireTime;
}