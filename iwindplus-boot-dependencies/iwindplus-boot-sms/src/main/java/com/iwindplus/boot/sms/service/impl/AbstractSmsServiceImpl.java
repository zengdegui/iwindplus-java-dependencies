/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.sms.service.impl;

import com.iwindplus.boot.util.TemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 短信业务抽象类.
 *
 * @author zengdegui
 * @since 2020/3/13
 */
@Slf4j
public abstract class AbstractSmsServiceImpl {
    /**
     * 每个分组的手机个数.
     */
    protected static final int GROUP_SIZE = 100;

    /**
     * 短信验证码长度.
     */
    protected static final int CAPTCHA_LENGTH = 6;

    /**
     * 短信验证码有效时间（单位：分钟，默认：10）.
     */
    protected static final int CAPTCHA_TIMEOUT = 10;

    /**
     * 获取模板参数，模板参数替换，返回map结构.
     *
     * @param templateContent    模板内容
     * @param templateParamValue 模板参数值
     * @return Map<String, Object>
     */
    protected Map<String, Object> getTemplateParamMap(String templateContent, List<String> templateParamValue) {
        String[] templateParam = StringUtils.substringsBetween(templateContent, "${", "}");
        // 判断模板内容是否有参数，有则替换
        if (ArrayUtils.isNotEmpty(templateParam) && CollectionUtils.isNotEmpty(templateParamValue)) {
            Map<String, Object> templateParamAndValue = new HashMap<>(16);
            for (int ii = 0; ii < templateParam.length; ii++) {
                templateParamAndValue.put(templateParam[ii], templateParamValue.get(ii));
            }
            return templateParamAndValue;
        }
        return null;
    }

    /**
     * 有参数就替换，无参数不替换，返回内容.
     *
     * @param templateContent    模板内容
     * @param templateParamValue 模板参数的值
     * @return String
     */
    protected String getTemplateContent(String templateContent, List<String> templateParamValue) {
        Map<String, Object> templateParam = this.getTemplateParamMap(templateContent, templateParamValue);
        if (MapUtils.isNotEmpty(templateParam)) {
            try {
                return TemplateUtil.getTemplateContent(templateParam, templateContent);
            } catch (Exception e) {
                log.error("exception [{}]", e.getMessage());
            }
        }
        return templateContent;
    }
}
