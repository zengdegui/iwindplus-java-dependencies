/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mybatis.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2020/10/17
 */
public class MybatisConstant {
    /**
     * 创建时间.
     */
    public static final String CREATE_TIME = "createTime";

    /**
     * 更新时间.
     */
    public static final String UPDATE_TIME = "updateTime";

    /**
     * 创建人.
     */
    public static final String CREATE_BY = "createBy";

    /**
     * 更新人.
     */
    public static final String UPDATE_BY = "updateBy";

    /**
     * 是否删除.
     */
    public static final String FLAG_DELETE = "flagDelete";

    /**
     * 乐观锁.
     */
    public static final String VERSION = "version";

    /**
     * 操作人.
     */
    public static final String OPERATE_BY = "operateBy";

    /**
     * 用户主键.
     */
    public static final String USER_ID = "userId";

    /**
     * 应用主键.
     */
    public static final String APP_ID = "appId";
}
