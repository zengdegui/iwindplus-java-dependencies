/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mybatis.service;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.mybatis.domain.constant.MybatisConstant;
import com.iwindplus.boot.web.xss.XssHttpServletRequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

/**
 * 公共字段自动填充.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class MyBatisAutoFillHandler implements MetaObjectHandler {
    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    /**
     * HttpServletRequest.
     */
    @Autowired
    protected HttpServletRequest request;

    /**
     * 添加填充.
     *
     * @param metaObject metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("Mybatis auto fill when public fields are added");
        // 设置是否删除.
        String flagDelete = MybatisConstant.FLAG_DELETE;
        if (metaObject.hasGetter(flagDelete) && Objects.isNull(metaObject.getValue(flagDelete))) {
            this.strictInsertFill(metaObject, flagDelete, Boolean.class, false);
        }
        // 设置乐观锁.
        String version = MybatisConstant.VERSION;
        if (metaObject.hasGetter(version) && Objects.isNull(metaObject.getValue(version))) {
            this.strictInsertFill(metaObject, version, Integer.class, 0);
        }
        LocalDateTime now = LocalDateTime.now();
        // 设置创建时间.
        String createTime = MybatisConstant.CREATE_TIME;
        if (metaObject.hasGetter(createTime) && Objects.isNull(metaObject.getValue(createTime))) {
            this.strictInsertFill(metaObject, createTime, LocalDateTime.class, now);
        }
        // 设置创建人.
        String createBy = MybatisConstant.CREATE_BY;
        if (metaObject.hasGetter(createBy) && Objects.isNull(metaObject.getValue(createBy))) {
            try {
                String operateBy = this.getOperateBy(this.request);
                if (StringUtils.isNotBlank(operateBy)) {
                    this.strictInsertFill(metaObject, createBy, String.class, operateBy);
                }
            } catch (Exception e) {
                log.error("Exception [{}]", e.getMessage());
            }
        }
        // 设置更新时间.
        String updateTime = MybatisConstant.UPDATE_TIME;
        if (metaObject.hasGetter(updateTime) && Objects.isNull(metaObject.getValue(updateTime))) {
            this.strictInsertFill(metaObject, updateTime, LocalDateTime.class, now);
        }
        // 设置更新人.
        String updateBy = MybatisConstant.UPDATE_BY;
        if (metaObject.hasGetter(updateBy) && Objects.isNull(metaObject.getValue(updateBy))) {
            try {
                String operateBy = this.getOperateBy(this.request);
                if (StringUtils.isNotBlank(operateBy)) {
                    this.strictUpdateFill(metaObject, updateBy, String.class, operateBy);
                }
            } catch (Exception e) {
                log.error("Exception [{}]", e.getMessage());
            }
        }
    }

    /**
     * 更新填充.
     *
     * @param metaObject metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("Mybatis auto fill when editing public fields");
        LocalDateTime now = LocalDateTime.now();
        // 设置更新时间.
        String updateTime = MybatisConstant.UPDATE_TIME;
        if (metaObject.hasGetter(updateTime) && Objects.isNull(metaObject.getValue(updateTime))) {
            this.strictUpdateFill(metaObject, updateTime, LocalDateTime.class, now);
        }
        // 设置更新人.
        String updateBy = MybatisConstant.UPDATE_BY;
        if (metaObject.hasGetter(updateBy) && Objects.isNull(metaObject.getValue(updateBy))) {
            try {
                String operateBy = this.getOperateBy(this.request);
                if (StringUtils.isNotBlank(operateBy)) {
                    this.strictUpdateFill(metaObject, updateBy, String.class, operateBy);
                }
            } catch (Exception e) {
                log.error("Exception [{}]", e.getMessage());
            }
        }
    }

    /**
     * 获取操作人.
     *
     * @param request 请求
     * @return String
     * @throws Exception
     */
    protected String getOperateBy(HttpServletRequest request) throws Exception {
        String operateBy;
        XssHttpServletRequestWrapper requestWrapper = new XssHttpServletRequestWrapper(request);
        // Cookie不为空返回
        Cookie[] cookies = requestWrapper.getCookies();
        if (ArrayUtil.isNotEmpty(cookies)) {
            for (Cookie cookie : cookies) {
                if (StringUtils.equals(MybatisConstant.OPERATE_BY, cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        // 请求头不为空返回
        operateBy = requestWrapper.getHeader(MybatisConstant.OPERATE_BY);
        if (StringUtils.isNotBlank(operateBy)) {
            return operateBy;
        }
        // 请求参数不为空返回
        operateBy = requestWrapper.getParameter(MybatisConstant.OPERATE_BY);
        if (StringUtils.isNotBlank(operateBy)) {
            return operateBy;
        }
        // 请求body体不为空返回
        byte[] body = requestWrapper.getBody();
        if (ArrayUtils.isNotEmpty(body)) {
            Map<String, Object> map = this.objectMapper.readValue(new String(body, Charset.defaultCharset()), new TypeReference<Map<String, Object>>() {
            });
            if (MapUtils.isNotEmpty(map)) {
                Object data = map.get(MybatisConstant.OPERATE_BY);
                if (Objects.nonNull(data)) {
                    return data.toString();
                }
            }
        }
        return null;
    }
}
