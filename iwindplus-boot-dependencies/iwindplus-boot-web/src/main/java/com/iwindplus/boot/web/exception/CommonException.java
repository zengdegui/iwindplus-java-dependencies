/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.exception;

/**
 * 公共的异常接口.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface CommonException {
    /**
     * 错误编码.
     *
     * @return
     */
    String getCode();

    /**
     * 错误信息.
     *
     * @return
     */
    String getMessage();
}
