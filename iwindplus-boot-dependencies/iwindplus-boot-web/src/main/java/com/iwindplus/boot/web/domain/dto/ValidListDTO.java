/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.web.domain.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * 可被校验的List数据传输对象.
 *
 * @param <E> 元素类型
 * @author zengdegui
 * @since 2021/7/8
 */
@Data
public class ValidListDTO<E> implements List<E> {
    @Valid
    @NotEmpty(message = "{entities.notEmpty}")
    private List<E> entities;

    public ValidListDTO() {
        this.entities = new LinkedList<>();
    }

    public ValidListDTO(List<E> entities) {
        this.entities = entities;
    }

    @Override
    public int size() {
        return entities.size();
    }

    @Override
    public boolean isEmpty() {
        return entities.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return entities.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return entities.iterator();
    }

    @Override
    public Object[] toArray() {
        return entities.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return entities.toArray(a);
    }

    @Override
    public boolean add(E e) {
        return entities.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return entities.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return entities.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return entities.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return entities.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return entities.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return entities.retainAll(c);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E get(int index) {
        return entities.get(index);
    }

    @Override
    public E set(int index, E element) {
        return entities.set(index, element);
    }

    @Override
    public void add(int index, E element) {
        entities.add(index, element);
    }

    @Override
    public E remove(int index) {
        return entities.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return entities.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return entities.lastIndexOf(o);
    }

    @Override
    public ListIterator<E> listIterator() {
        return entities.listIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return entities.listIterator(index);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return entities.subList(fromIndex, toIndex);
    }
}
