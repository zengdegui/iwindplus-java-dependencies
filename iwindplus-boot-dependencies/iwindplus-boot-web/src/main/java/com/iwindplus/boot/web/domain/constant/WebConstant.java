/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.domain.constant;

/**
 * 常数.
 *
 * @author zengdegui
 * @since 2018/12/27
 */
public class WebConstant {
    /**
     * 错误状态码.
     */
    public static final String ERROR_STATUS = "status";

    /**
     * 错误编码.
     */
    public static final String ERROR_CODE = "error";

    /**
     * 错误信息.
     */
    public static final String ERROR_MSG = "error_description";
}
