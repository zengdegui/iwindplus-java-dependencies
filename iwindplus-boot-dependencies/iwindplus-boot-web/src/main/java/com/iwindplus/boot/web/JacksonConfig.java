/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.Formatter;

import java.math.BigInteger;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

/**
 * jackson相关配置.
 *
 * @author zengdegui
 * @since 2020/11/8
 */
@Slf4j
@Configuration
public class JacksonConfig {
    /**
     * 创建 Jackson2ObjectMapperBuilderCustomizer.
     * 处理期序列化，long类型数据丢失精度等问题
     *
     * @return Jackson2ObjectMapperBuilderCustomizer
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN, LocaleContextHolder.getLocale());
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN, LocaleContextHolder.getLocale());
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN, LocaleContextHolder.getLocale());
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Shanghai");
        Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer = builder -> builder
                .serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(dateTimeFormatter))
                .serializerByType(LocalDate.class, new LocalDateSerializer(dateFormatter))
                .serializerByType(LocalTime.class, new LocalTimeSerializer(timeFormatter))
                .deserializerByType(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter))
                .deserializerByType(LocalDate.class, new LocalDateDeserializer(dateFormatter))
                .deserializerByType(LocalTime.class, new LocalTimeDeserializer(timeFormatter))
                .serializerByType(Long.class, ToStringSerializer.instance)
                .serializerByType(Long.TYPE, ToStringSerializer.instance)
                .serializerByType(BigInteger.class, ToStringSerializer.instance)
                .failOnUnknownProperties(false)
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .timeZone(timeZone);
        log.info("Jackson2ObjectMapperBuilderCustomizer [{}]", jackson2ObjectMapperBuilderCustomizer);
        return jackson2ObjectMapperBuilderCustomizer;
    }

    /**
     * LocalDateTime转换器，用于接收期格式数据.
     *
     * @return Formatter<LocalDateTime>
     */
    @Bean
    public Formatter<LocalDateTime> localDateTimeFormatter() {
        Formatter<LocalDateTime> localDateTimeFormatter = new Formatter<LocalDateTime>() {
            @Override
            public String print(LocalDateTime localDateTime, Locale locale) {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN, locale);
                return dateTimeFormatter.format(localDateTime);
            }

            @Override
            public LocalDateTime parse(String text, Locale locale) throws ParseException {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN, locale);
                return LocalDateTime.parse(text, dateTimeFormatter);
            }
        };
        log.info("Formatter<LocalDateTime> [{}]", localDateTimeFormatter);
        return localDateTimeFormatter;
    }

    /**
     * LocalDate期转换器，用于接收期格式数据.
     *
     * @return Formatter<LocalDate>
     */
    @Bean
    public Formatter<LocalDate> localDateFormatter() {
        Formatter<LocalDate> localDateFormatter = new Formatter<LocalDate>() {
            @Override
            public String print(LocalDate localDate, Locale locale) {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN, locale);
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate parse(String text, Locale locale) throws ParseException {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN, locale);
                return LocalDate.parse(text, dateTimeFormatter);
            }
        };
        log.info("Formatter<LocalDate> [{}]", localDateFormatter);
        return localDateFormatter;
    }

    /**
     * LocalTime转换器，用于接收期格式数据.
     *
     * @return Formatter<LocalTime>
     */
    @Bean
    public Formatter<LocalTime> localTimeFormatter() {
        Formatter<LocalTime> localTimeFormatter = new Formatter<LocalTime>() {
            @Override
            public String print(LocalTime localTime, Locale locale) {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN, locale);
                return dateTimeFormatter.format(localTime);
            }

            @Override
            public LocalTime parse(String text, Locale locale) throws ParseException {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN, locale);
                return LocalTime.parse(text, dateTimeFormatter);
            }
        };
        log.info("Formatter<LocalTime> [{}]", localTimeFormatter);
        return localTimeFormatter;
    }
}
