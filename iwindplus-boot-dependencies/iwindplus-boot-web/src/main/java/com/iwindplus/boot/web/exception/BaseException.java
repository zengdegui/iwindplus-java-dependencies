/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 自定义运行时异常.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
public class BaseException extends RuntimeException implements CommonException {
    private static final long serialVersionUID = -6783551049743441442L;

    /**
     * 状态码.
     */
    protected HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

    /**
     * 错误编码.
     */
    protected String code;

    /**
     * 构造方法.
     *
     * @param exceptionEnum 异常枚举
     */
    public BaseException(CommonException exceptionEnum) {
        super(exceptionEnum.getMessage());
        this.code = exceptionEnum.getCode();
    }

    /**
     * 构造方法.
     *
     * @param status        状态码
     * @param exceptionEnum 异常枚举
     */
    public BaseException(HttpStatus status, CommonException exceptionEnum) {
        this(exceptionEnum);
        this.status = status;
    }

    /**
     * 构造方法.
     *
     * @param exceptionEnum 异常枚举
     * @param cause         堆栈信息
     */
    public BaseException(CommonException exceptionEnum, Throwable cause) {
        super(exceptionEnum.getMessage(), cause);
        this.code = exceptionEnum.getCode();
    }

    /**
     * 构造方法.
     *
     * @param status        状态码
     * @param exceptionEnum 异常枚举
     * @param cause         堆栈信息
     */
    public BaseException(HttpStatus status, CommonException exceptionEnum, Throwable cause) {
        this(exceptionEnum, cause);
        this.status = status;
    }

    /**
     * 构造方法.
     *
     * @param code    错误编码
     * @param message 错误消息
     */
    public BaseException(String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 构造方法.
     *
     * @param status  状态码
     * @param code    错误编码
     * @param message 错误消息
     */
    public BaseException(HttpStatus status, String code, String message) {
        this(code, message);
        this.status = status;
    }

    /**
     * 构造方法.
     *
     * @param code    错误编码
     * @param message 错误消息
     * @param cause   堆栈信息
     */
    public BaseException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    /**
     * 构造方法.
     *
     * @param code    错误编码
     * @param message 错误消息
     * @param cause   堆栈信息
     */
    public BaseException(HttpStatus status, String code, String message, Throwable cause) {
        this(code, message, cause);
        this.status = status;
    }
}
