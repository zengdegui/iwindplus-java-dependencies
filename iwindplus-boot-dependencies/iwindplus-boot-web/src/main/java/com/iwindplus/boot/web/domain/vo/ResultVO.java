/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.iwindplus.boot.web.domain.constant.WebConstant;
import com.iwindplus.boot.web.domain.enumerate.WebCodeEnum;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 结果视图对象.
 *
 * @author zengdegui
 * @since 2020/10/31
 */
@Data
@Builder
public class ResultVO implements Serializable {
    private static final long serialVersionUID = 7869786563361406291L;

    private static final GsonBuilder INSTANCE = new GsonBuilder();

    /**
     * 状态码.
     */
    @JsonProperty(WebConstant.ERROR_STATUS)
    @SerializedName(WebConstant.ERROR_STATUS)
    private Integer status;

    /**
     * 错误编码.
     */
    @JsonProperty(WebConstant.ERROR_CODE)
    @SerializedName(WebConstant.ERROR_CODE)
    private String code;

    /**
     * 错误信息.
     */
    @JsonProperty(WebConstant.ERROR_MSG)
    @SerializedName(WebConstant.ERROR_MSG)
    private String message;

    /**
     * 数据.
     */
    private Object data;

    /**
     * 成功.
     *
     * @param data 数据
     * @return ResultVO
     */
    public static ResultVO success(Object data) {
        return ResultVO
                .builder()
                .status(HttpStatus.OK.value())
                .code(WebCodeEnum.SUCCESS.getCode())
                .message(WebCodeEnum.SUCCESS.getMessage())
                .data(data)
                .build();
    }

    /**
     * json转ResultVO.
     *
     * @param json json转ResultVO
     * @return ResultVO
     */
    public static ResultVO fromJson(String json) {
        return INSTANCE.create().fromJson(json, ResultVO.class);
    }
}
