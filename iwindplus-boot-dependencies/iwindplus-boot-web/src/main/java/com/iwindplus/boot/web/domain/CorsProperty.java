/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 跨域配置相关属性.
 *
 * @author zengdegui
 * @since 2019/6/12
 */
@Data
@ConfigurationProperties(prefix = "cors", ignoreUnknownFields = true)
public class CorsProperty {
    /**
     * 是否发送cookie信息.
     */
    private Boolean allowCredentials = true;

    /**
     * 允许访问的客户端域名.
     */
    private List<String> allowedOrigins;

    /**
     * 允许服务端访问的客户端请求头.
     */
    private List<String> allowedHeaders;

    /**
     * 允许访问的方法名.
     */
    private List<String> allowedMethods;

    /**
     * 有效时长.
     */
    private Long maxAge = 1800L;
}
