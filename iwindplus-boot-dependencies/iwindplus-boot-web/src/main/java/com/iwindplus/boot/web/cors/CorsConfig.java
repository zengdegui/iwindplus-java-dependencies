/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.web.cors;

import com.iwindplus.boot.web.domain.CorsProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

/**
 * 跨域相关配置.
 *
 * @author zengdegui
 * @since 2019/6/12
 */
@Slf4j
public class CorsConfig {
    @Autowired
    private CorsProperty corsProperty;

    /**
     * 创建 CorsFilter.
     *
     * @return CorsFilter
     */
    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 是否发送cookie信息
        corsConfiguration.setAllowCredentials(this.corsProperty.getAllowCredentials());
        // 有效时长
        corsConfiguration.setMaxAge(this.corsProperty.getMaxAge());
        // 允许访问的客户端域名
        List<String> allowedOrigins = this.corsProperty.getAllowedOrigins();
        if (CollectionUtils.isEmpty(allowedOrigins)) {
            corsConfiguration.addAllowedOrigin("*");
        } else {
            corsConfiguration.setAllowedOrigins(allowedOrigins);
        }
        // 允许服务端访问的客户端请求头
        List<String> allowedHeaders = this.corsProperty.getAllowedHeaders();
        if (CollectionUtils.isEmpty(allowedHeaders)) {
            corsConfiguration.addAllowedHeader("*");
        } else {
            corsConfiguration.setAllowedHeaders(allowedHeaders);
        }
        // 允许访问的方法名,GET POST等
        List<String> allowedMethods = this.corsProperty.getAllowedMethods();
        if (CollectionUtils.isEmpty(allowedMethods)) {
            corsConfiguration.addAllowedMethod("*");
        } else {
            corsConfiguration.setAllowedMethods(allowedMethods);
        }
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        CorsFilter corsFilter = new CorsFilter(urlBasedCorsConfigurationSource);
        log.info("CorsFilter [{}]", corsFilter);
        return corsFilter;
    }
}
