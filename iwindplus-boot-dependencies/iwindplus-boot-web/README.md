web模块
1.跨域配置，默认不启用，需要开启请在程序中引用@EnableCors开启功能
2.ResultVO为统一响应对象
3.统一全局异常处理，程序中需要抛出异常，只需throws new BaseException出去即可,不需要try catch	
4.国际化配置(默认根据部署应用服务器系统语言来),并集成入参校验国际化,通过以下配置指定国际化配置文件路径,文件名后缀默认properties
    spring:
        messages:
            basename: i18n/messages
5.xss过滤器，请求参数的过滤,在微服务中有时需要请求参数传递到下一个服务:
	可通过以下方式获取:
	XssHttpServletRequestWrapper requestWrapper = new XssHttpServletRequestWrapper(this.request);
6.BaseController -- Controller基类，可用于继承
