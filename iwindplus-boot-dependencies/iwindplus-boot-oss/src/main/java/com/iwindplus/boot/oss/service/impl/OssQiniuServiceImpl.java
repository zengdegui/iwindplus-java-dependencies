/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service.impl;

import com.iwindplus.boot.oss.domain.enumerate.OssCodeEnum;
import com.iwindplus.boot.oss.domain.vo.ResultQiniuVO;
import com.iwindplus.boot.oss.domain.vo.UploadVO;
import com.iwindplus.boot.oss.service.OssQiniuService;
import com.iwindplus.boot.util.FileUtil;
import com.iwindplus.boot.web.exception.BaseException;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * 七牛云对象存储操作业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/9
 */
@Slf4j
public class OssQiniuServiceImpl extends AbstractOssServiceImpl implements OssQiniuService {
    /**
     * MultipartProperties.
     */
    @Autowired
    protected MultipartProperties multipartProperties;

    @Override
    public UploadVO uploadFile(String accessKey, String secretKey, Region region, Boolean flagBreakpoint, String accessDomain,
                               String bucketName, MultipartFile file, String prefix, Boolean flagRename, String fileName) {
        if (Objects.isNull(file)) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.getSize(), this.multipartProperties.getMaxFileSize().toBytes());
        return getUploadVO(accessKey, secretKey, region, flagBreakpoint, accessDomain, bucketName, file, null, prefix, flagRename, fileName);
    }

    @Override
    public UploadVO uploadFile(String accessKey, String secretKey, Region region, Boolean flagBreakpoint, String accessDomain,
                               String bucketName, String absolutePath, String prefix, Boolean flagRename, String fileName) {
        File file = new File(absolutePath);
        if (!file.exists()) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.length(), this.multipartProperties.getMaxFileSize().toBytes());
        return getUploadVO(accessKey, secretKey, region, flagBreakpoint, accessDomain, bucketName, null, file, prefix, flagRename, fileName);

    }

    @Override
    public String getSignUrl(String accessKey, String secretKey, String accessDomain, String relativePath, Long timeout) {
        String absolutePath = new StringBuilder(accessDomain)
                .append("/").append(relativePath).toString();
        Auth auth = Auth.create(accessKey, secretKey);
        Long expires = Objects.isNull(timeout) ? URL_TIMEOUT * 60 : timeout * 60;
        return auth.privateDownloadUrl(absolutePath, expires);
    }

    @Override
    public void downloadFile(String accessKey, String secretKey, String accessDomain, HttpServletResponse response,
                             String relativePath, Boolean flagRename, String fileName) {
        String finalUrl = this.getSignUrl(accessKey, secretKey, accessDomain, relativePath, null);
        InputStream inputStream = null;
        try {
            URL url = new URL(finalUrl);
            inputStream = new BufferedInputStream(url.openStream());
            String sourceFileName = FileUtil.getName(relativePath);
            String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
            super.downloadFile(inputStream, newFileName, response);
        } catch (IOException e) {
            log.error("IOException [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        } finally {
            closeInputStream(inputStream);
        }
    }

    private UploadVO getUploadVO(String accessKey, String secretKey, Region region, Boolean flagBreakpoint, String accessDomain, String bucketName,
                                 MultipartFile multipartFile, File file, String prefix, Boolean flagRename, String fileName) {
        InputStream inputStream = null;
        Response response = null;
        String sourceFileName = null;
        long fileSize = 0;
        try {
            if (Objects.nonNull(multipartFile)) {
                sourceFileName = multipartFile.getOriginalFilename();
                fileSize = multipartFile.getSize();
                inputStream = multipartFile.getInputStream();
            }
            if (Objects.nonNull(file)) {
                sourceFileName = file.getName();
                fileSize = file.length();
                inputStream = new FileInputStream(file);
            }
            String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
            String relativePath = super.getRelativePath(prefix, newFileName);
            UploadManager uploadManager = this.getUploadManager(region, flagBreakpoint, bucketName);
            String upToken = this.getUpToken(accessKey, secretKey, bucketName);
            response = uploadManager.put(inputStream, relativePath, upToken, null, null);
            return buildResult(accessKey, secretKey, accessDomain, fileSize, sourceFileName, newFileName, response);
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        } finally {
            closeInputStream(inputStream);
            closeResponse(response);
        }
    }

    private UploadVO buildResult(String accessKey, String secretKey, String accessDomain, long fileSize,
                                 String sourceFileName, String fileName, Response response) throws QiniuException {
        if (Objects.nonNull(response) && response.isOK()) {
            ResultQiniuVO qiniuResultVO = response.jsonToObject(ResultQiniuVO.class);
            if (Objects.nonNull(qiniuResultVO)) {
                String relativePath = qiniuResultVO.getFileName();
                String absolutePath = this.getSignUrl(accessKey, secretKey, accessDomain, relativePath, null);
                UploadVO data = UploadVO.builder()
                        .sourceFileName(sourceFileName)
                        .fileName(fileName)
                        .fileSize(fileSize)
                        .relativePath(relativePath)
                        .absolutePath(absolutePath)
                        .build();
                return data;
            }
        }
        return null;
    }

    private void closeResponse(Response response) {
        if (Objects.nonNull(response)) {
            response.close();
        }
    }

    private UploadManager getUploadManager(Region region, Boolean flagBreakpoint, String bucketName) throws Exception {
        Configuration cfg = new Configuration(region);
        // 指定分片上传版本
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;
        cfg.resumableUploadMaxConcurrentTaskCount = 5;
        if (Objects.nonNull(flagBreakpoint) && flagBreakpoint) {
            Path tempPath = Paths.get(System.getenv(TMP_DIR), bucketName);
            if (!Files.exists(tempPath)) {
                Files.createDirectories(tempPath);
            }
            // 设置断点续传文件进度保存目录
            FileRecorder fileRecorder = new FileRecorder(tempPath.toString());
            return new UploadManager(cfg, fileRecorder);
        } else {
            return new UploadManager(cfg);
        }
    }

    private String getUpToken(String accessKey, String secretKey, String bucketName) {
        Auth auth = Auth.create(accessKey, secretKey);
        StringMap putPolicy = new StringMap();
        putPolicy.put("callbackBodyType", "application/json");
        putPolicy.put("returnBody",
                "{\"fileName\":\"$(key)\",\"hash\":\"$(etag)\",\"bucket\":\"$(bucket)\",\"fileSize\":$(fsize)}");
        long expireSeconds = 3600L;
        return auth.uploadToken(bucketName, null, expireSeconds, putPolicy);
    }
}
