/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss;

import com.iwindplus.boot.oss.service.OssQiniuService;
import com.iwindplus.boot.oss.service.impl.OssQiniuServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 七牛云对象存储操作配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OssQiniuConfig {
    /**
     * 创建 OssQiniuService.
     *
     * @return OssQiniuService
     */
    @Bean
    public OssQiniuService ossQiniuService() {
        OssQiniuServiceImpl qiniuOssService = new OssQiniuServiceImpl();
        log.info("OssQiniuService [{}]", qiniuOssService);
        return qiniuOssService;
    }
}
