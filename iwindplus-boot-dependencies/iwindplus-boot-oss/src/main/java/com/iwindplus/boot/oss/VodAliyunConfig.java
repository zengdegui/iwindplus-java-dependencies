/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss;

import com.iwindplus.boot.oss.service.VodAliyunService;
import com.iwindplus.boot.oss.service.impl.VodAliyunServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 阿里云视频点播操作配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class VodAliyunConfig {
    /**
     * 创建 VodAliyunService.
     *
     * @return VodAliyunService
     */
    @Bean
    public VodAliyunService vodAliyunService() {
        VodAliyunServiceImpl vodAliyunService = new VodAliyunServiceImpl();
        log.info("VodAliyunService [{}]", vodAliyunService);
        return vodAliyunService;
    }
}
