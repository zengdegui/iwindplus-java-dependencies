/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service;

import com.iwindplus.boot.oss.domain.vo.UploadVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件操作业务层接口类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface FileService {
    /**
     * 文件上传.
     *
     * @param file       文件（必填）
     * @param prefix     存储目录前缀（可选）
     * @param flagRename 是否重命名文件名（可选）
     * @param fileName   新文件名，不包含文件后缀（可选）
     * @return UploadVO
     */
    UploadVO uploadFile(MultipartFile file, String prefix, Boolean flagRename, String fileName);

    /**
     * 文件下载.
     *
     * @param response     响应（必填）
     * @param relativePath 相对路径（必填）
     * @param flagRename   是否重命名文件名（可选）
     * @param fileName     新文件名，不包含文件后缀（可选）
     * @param flagRemote   是否远程获取（可选，默认：从src/main/resources下获取）
     */
    void downloadFile(HttpServletResponse response, String relativePath, Boolean flagRename, String fileName, Boolean flagRemote);
}
