/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss;

import com.iwindplus.boot.oss.service.OssAliyunService;
import com.iwindplus.boot.oss.service.impl.OssAliyunServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * 阿里云对象存储操作配置.
 *
 * @author zengdegui
 * @since 2019/8/13
 */
@Slf4j
public class OssAliyunConfig {
	/**
	 * 创建 OssAliyunService.
	 *
	 * @return OssAliyunService
	 */
	@Bean
	public OssAliyunService ossAliyunService() {
		OssAliyunServiceImpl alibabaOssService = new OssAliyunServiceImpl();
		log.info("OssAliyunService [{}]", alibabaOssService);
		return alibabaOssService;
	}
}
