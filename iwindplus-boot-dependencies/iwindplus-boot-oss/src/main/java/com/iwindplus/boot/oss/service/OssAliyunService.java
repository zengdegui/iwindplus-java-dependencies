/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service;

import com.iwindplus.boot.oss.domain.vo.UploadVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 阿里云对象存储业务层接口类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
public interface OssAliyunService {
    /**
     * 文件上传（当文件大于等于4M，开启分片或断点上传，否则为简单上传）.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param region         服务器区域（必填）
     * @param securityToken  上传授权安全令牌（可选）
     * @param partSize       分片大小（可选，单位：兆，默认：200M）
     * @param flagBreakpoint 是否开启断点上传（可选，默认：false）
     * @param accessDomain   访问域名（可选，自定义域名）
     * @param bucketName     空间名（必填）
     * @param file           文件（必填）
     * @param prefix         存储目录前缀（可选）
     * @param flagRename     是否重命名文件名（可选）
     * @param fileName       新文件名，不包含文件后缀（可选）
     * @return UploadVO
     */
    UploadVO uploadFile(String accessKey, String secretKey, String region, String securityToken,
                        Long partSize, Boolean flagBreakpoint, String accessDomain, String bucketName,
                        MultipartFile file, String prefix, Boolean flagRename, String fileName);

    /**
     * 文件上传（当文件大于等于4M，开启分片或断点上传，否则为简单上传）.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param region         服务器区域（必填）
     * @param securityToken  上传授权安全令牌（可选）
     * @param partSize       分片大小（可选，单位：兆，默认：200M）
     * @param flagBreakpoint 是否开启断点上传（可选，默认：false）
     * @param accessDomain   访问域名（可选，自定义域名）
     * @param bucketName     空间名（必填）
     * @param absolutePath   绝对路径（必填）
     * @param prefix         存储目录前缀（可选）
     * @param flagRename     是否重命名文件名（可选）
     * @param fileName       新文件名，不包含文件后缀（可选）
     * @return UploadVO
     */
    UploadVO uploadFile(String accessKey, String secretKey, String region, String securityToken,
                        Long partSize, Boolean flagBreakpoint, String accessDomain, String bucketName,
                        String absolutePath, String prefix, Boolean flagRename, String fileName);

    /**
     * 文件上传（当文件大于等于4M，开启分片或断点上传，否则为简单上传）.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param region         服务器区域（必填）
     * @param securityToken  上传授权安全令牌（可选）
     * @param partSize       分片大小（可选，单位：兆，默认：200M）
     * @param flagBreakpoint 是否开启断点上传（可选，默认：false）
     * @param bucketName     空间名（必填）
     * @param file           文件（必填）
     * @param relativePath   相对路径（必填）
     * @return boolean
     */
    boolean uploadFile(String accessKey, String secretKey, String region, String securityToken,
                       Long partSize, Boolean flagBreakpoint, String bucketName,
                       MultipartFile file, String relativePath);

    /**
     * 文件上传（当文件大于等于4M，开启分片或断点上传，否则为简单上传）.
     *
     * @param accessKey      访问key（必填）
     * @param secretKey      密钥（必填）
     * @param region         服务器区域（必填）
     * @param securityToken  上传授权安全令牌（可选）
     * @param partSize       分片大小（可选，单位：兆，默认：200M）
     * @param flagBreakpoint 是否开启断点上传（可选，默认：false）
     * @param bucketName     空间名（必填）
     * @param absolutePath   绝对路径（必填）
     * @param relativePath   相对路径（必填）
     * @return boolean
     */
    boolean uploadFile(String accessKey, String secretKey, String region, String securityToken,
                       Long partSize, Boolean flagBreakpoint, String bucketName,
                       String absolutePath, String relativePath);

    /**
     * 简单上传.
     *
     * @param accessKey     访问key（必填）
     * @param secretKey     密钥（必填）
     * @param region        服务器区域（必填）
     * @param securityToken 上传授权安全令牌（可选）
     * @param bucketName    空间名（必填）
     * @param file          文件（必填）
     * @param relativePath  相对路径（必填）
     * @return boolean
     */
    boolean getUploadBySimple(String accessKey, String secretKey, String region, String securityToken,
                              String bucketName, MultipartFile file, String relativePath);

    /**
     * 文件预签名访问路径（如果空间名为私有）.
     *
     * @param accessKey     访问key（必填）
     * @param secretKey     密钥（必填）
     * @param region        服务器区域（必填）
     * @param securityToken 上传授权安全令牌（可选）
     * @param accessDomain  访问域名（可选，自定义域名）
     * @param bucketName    空间名（必填）
     * @param relativePath  相对路径（必填）
     * @param timeout       过期时间（可选，单位：分钟，默认：60）
     * @return String
     */
    String getSignUrl(String accessKey, String secretKey, String region, String securityToken,
                      String accessDomain, String bucketName, String relativePath, Long timeout);

    /**
     * 文件下载.
     *
     * @param accessKey     访问key（必填）
     * @param secretKey     密钥（必填）
     * @param region        服务器区域（必填）
     * @param securityToken 上传授权安全令牌（可选）
     * @param bucketName    空间名（必填）
     * @param response      响应（必填）
     * @param relativePath  相对路径（必填）
     * @param flagRename    是否重命名文件名（可选）
     * @param fileName      新文件名，不包含文件后缀（可选）
     */
    void downloadFile(String accessKey, String secretKey, String region, String securityToken,
                      String bucketName, HttpServletResponse response, String relativePath,
                      Boolean flagRename, String fileName);
}
