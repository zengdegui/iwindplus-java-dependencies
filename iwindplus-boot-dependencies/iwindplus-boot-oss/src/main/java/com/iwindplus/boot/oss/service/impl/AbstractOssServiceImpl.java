/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.IdUtil;
import com.iwindplus.boot.oss.domain.enumerate.OssCodeEnum;
import com.iwindplus.boot.util.DateUtil;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * 对象存储业务抽象类.
 *
 * @author zengdegui
 * @since 2020/3/13
 */
@Slf4j
public abstract class AbstractOssServiceImpl {
    /**
     * 上传目录.
     */
    protected static final String UPLOAD_DIR = "upload";

    /**
     * 用户目录.
     */
    protected static final String USER_DIR = "user.dir";

    /**
     * 缓存目录.
     */
    protected static final String TMP_DIR = "java.io.tmpdir";

    /**
     * url有效时间（单位：分钟，默认：60）.
     */
    protected static final int URL_TIMEOUT = 60;

    /**
     * 播放凭证有效时间（单位：分钟，默认：30）.
     */
    protected static final int PLAY_AUTH_TIMEOUT = 30;

    /**
     * 获取新文件名（包含后缀）.
     *
     * @param sourceFileName 原文件（包含后缀）
     * @param flagRename     是否重命名文件名
     * @param fileName       新文件名，不包含文件后缀
     * @return String
     */
    protected String getNewFileName(String sourceFileName, Boolean flagRename, String fileName) {
        if (Objects.nonNull(flagRename) && flagRename) {
            String suffix = FileNameUtil.getSuffix(sourceFileName);
            if (StringUtils.isBlank(fileName)) {
                return new StringBuilder(IdUtil.fastSimpleUUID()).append(".").append(suffix).toString();
            } else {
                return new StringBuilder(fileName).append(".").append(suffix).toString();
            }
        }
        return sourceFileName;
    }

    /**
     * 拼接相对路径（包含后缀）.
     *
     * @param prefix   存储目录前缀
     * @param fileName 文件名（包含后缀）
     * @return String
     */
    protected String getRelativePath(String prefix, String fileName) {
        String path = StringUtils.isNotBlank(prefix) ? prefix : UPLOAD_DIR;
        return new StringBuilder(path).append("/")
                .append(DateUtil.getStringDate(DatePattern.PURE_DATE_PATTERN))
                .append("/").append(fileName)
                .toString();
    }

    /**
     * 校验文件大小.
     *
     * @param fileSize    文件大小
     * @param maxFileSize 最大文件大小
     */
    protected void checkFile(long fileSize, long maxFileSize) {
        if (maxFileSize > 0 & fileSize > maxFileSize) {
            log.error("The file is too large,the file size [{}]", fileSize);
            String message = new StringBuilder("文件太大，不能超过").append(maxFileSize / (1024 * 1024)).append("兆").toString();
            throw new BaseException(OssCodeEnum.FILE_TOO_BIG.value(), message);
        }
    }

    /**
     * 浏览器下载文件.
     *
     * @param inputStream 输入流
     * @param fileName    下载文件名（包含后缀）
     * @param response    响应
     */
    protected void downloadFile(InputStream inputStream, String fileName, HttpServletResponse response) {
        ServletOutputStream servletOutputStream = null;
        try {
            response.setContentType("application/octet-stream");
            response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.addHeader("charset", "utf-8");
            response.addHeader("Pragma", "no-cache");
            String encodeName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + encodeName + "\"; filename*=utf-8''" + encodeName);
            servletOutputStream = response.getOutputStream();
            IOUtils.copy(inputStream, servletOutputStream);
            response.flushBuffer();
        } catch (IOException ex) {
            log.error("Data is suspended or interrupted [{}]", ex.getMessage());
            throw new BaseException(OssCodeEnum.FILE_DOWNLOAD_FAILED);
        } finally {
            closeInputStream(inputStream);
            closeServletOutputStream(servletOutputStream);
        }
    }

    /**
     * 文件删除.
     *
     * @param file 文件
     */
    protected void fileDelete(File file) {
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * 关闭InputStream流.
     *
     * @param inputStream 输入流
     */
    protected void closeInputStream(InputStream inputStream) {
        if (Objects.nonNull(inputStream)) {
            try {
                inputStream.close();
            } catch (IOException e) {
                log.error("Closing input stream exception [{}]", e.getMessage());
            }
        }
    }

    /**
     * 关闭servletOutputStream流.
     *
     * @param servletOutputStream 输出流
     */
    protected void closeServletOutputStream(ServletOutputStream servletOutputStream) {
        if (Objects.nonNull(servletOutputStream)) {
            try {
                servletOutputStream.close();
            } catch (IOException e) {
                log.error("Closing servlet output stream exception [{}]", e.getMessage());
            }
        }
    }
}
