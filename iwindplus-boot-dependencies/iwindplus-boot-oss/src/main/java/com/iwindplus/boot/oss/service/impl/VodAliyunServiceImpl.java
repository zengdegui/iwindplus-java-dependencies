/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.file.FileNameUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.vod.model.v20170321.CreateAuditRequest;
import com.aliyuncs.vod.model.v20170321.CreateUploadVideoRequest;
import com.aliyuncs.vod.model.v20170321.CreateUploadVideoResponse;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.aliyuncs.vod.model.v20170321.GetMezzanineInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetMezzanineInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.aliyuncs.vod.model.v20170321.SubmitAIMediaAuditJobRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwindplus.boot.oss.domain.enumerate.OssCodeEnum;
import com.iwindplus.boot.oss.domain.vo.UploadVideoVO;
import com.iwindplus.boot.oss.service.OssAliyunService;
import com.iwindplus.boot.oss.service.VodAliyunService;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 阿里云视频点播业务层接口实现类.
 *
 * @author zengdegui
 * @since 2022/1/14
 */
@Slf4j
public class VodAliyunServiceImpl extends AbstractOssServiceImpl implements VodAliyunService {
    /**
     * ObjectMapper.
     */
    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private OssAliyunService ossAliyunService;

    private DefaultAcsClient initVodClient(String accessKey, String secretKey, String region) {
        // 根据点播接入服务所在的Region填写，例如：接入服务在上海，则填cn-shanghai；其他区域请参见点播中心和访问域名。
        DefaultProfile profile = DefaultProfile.getProfile(region, accessKey, secretKey);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }

    @Override
    public UploadVideoVO uploadVideo(String accessKey, String secretKey, String region, MultipartFile file,
                                     Boolean flagRename, String fileName) {
        DefaultAcsClient acsClient = null;
        try {
            String sourceFileName = file.getOriginalFilename();
            String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
            String title = FileNameUtil.getPrefix(newFileName);
            CreateUploadVideoRequest request = new CreateUploadVideoRequest();
            request.setTitle(title);
            request.setFileName(newFileName);
            request.setFileSize(file.getSize());
            acsClient = this.initVodClient(accessKey, secretKey, region);
            CreateUploadVideoResponse acsResponse = acsClient.getAcsResponse(request);
            if (Objects.nonNull(acsResponse)) {
                String videoId = acsResponse.getVideoId();
                String uploadAuthStr = Base64.decodeStr(acsResponse.getUploadAuth()).replaceAll("\n", "").replaceAll("\r", "");
                String uploadAddressStr = Base64.decodeStr(acsResponse.getUploadAddress()).replaceAll("\n", "").replaceAll("\r", "");
                log.info("uploadAuth [{}]", uploadAuthStr);
                log.info("uploadAddress [{}]", uploadAddressStr);
                JsonNode uploadAuth = this.objectMapper.readTree(uploadAuthStr);
                JsonNode uploadAddress = this.objectMapper.readTree(uploadAddressStr);
                String accessKeyId = uploadAuth.get("AccessKeyId").asText();
                String accessKeySecret = uploadAuth.get("AccessKeySecret").asText();
                String securityToken = uploadAuth.get("SecurityToken").asText();
                String endpoint = uploadAddress.get("Endpoint").asText();
                String bucketName = uploadAddress.get("Bucket").asText();
                String objectName = uploadAddress.get("FileName").asText();
                boolean result = this.ossAliyunService.getUploadBySimple(accessKeyId, accessKeySecret, endpoint, securityToken,
                        bucketName, file, objectName);
                if (result) {
                    return UploadVideoVO
                            .builder()
                            .fileName(title)
                            .fileSize(file.getSize())
                            .videoId(videoId)
                            .build();
                }
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
        return null;
    }

    @Override
    public String getPlayAuth(String accessKey, String secretKey, String region, String videoId, Long timeout) {
        DefaultAcsClient acsClient = null;
        try {
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(videoId);
            request.setAuthInfoTimeout(Objects.isNull(timeout) ? PLAY_AUTH_TIMEOUT * 60 : timeout);
            acsClient = this.initVodClient(accessKey, secretKey, region);
            GetVideoPlayAuthResponse response = acsClient.getAcsResponse(request);
            if (Objects.nonNull(response)) {
                return response.getPlayAuth();
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.GET_PLAY_AUTH_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
        return null;
    }

    @Override
    public GetVideoInfoResponse.Video getVideoInfo(String accessKey, String secretKey, String region, String videoId) {
        DefaultAcsClient acsClient = null;
        try {
            GetVideoInfoRequest request = new GetVideoInfoRequest();
            request.setVideoId(videoId);
            acsClient = this.initVodClient(accessKey, secretKey, region);
            GetVideoInfoResponse response = acsClient.getAcsResponse(request);
            if (Objects.nonNull(response)) {
                return response.getVideo();
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.GET_VIDEO_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
        return null;
    }

    @Override
    public GetMezzanineInfoResponse.Mezzanine getSourceVideoInfo(String accessKey, String secretKey, String region, String videoId) {
        DefaultAcsClient acsClient = null;
        try {
            GetMezzanineInfoRequest request = new GetMezzanineInfoRequest();
            request.setVideoId(videoId);
            request.setAuthTimeout(3600L);
            acsClient = this.initVodClient(accessKey, secretKey, region);
            GetMezzanineInfoResponse response = acsClient.getAcsResponse(request);
            if (Objects.nonNull(response)) {
                return response.getMezzanine();
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.GET_SOURCE_VIDEO_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
        return null;
    }

    @Override
    public boolean removeVideo(String accessKey, String secretKey, String region, List<String> videoIds) {
        DefaultAcsClient acsClient = null;
        try {
            DeleteVideoRequest request = new DeleteVideoRequest();
            String ids = StringUtils.join(videoIds.toArray(), ",");
            request.setVideoIds(ids);
            acsClient = this.initVodClient(accessKey, secretKey, region);
            DeleteVideoResponse response = acsClient.getAcsResponse(request);
            if (Objects.nonNull(response)) {
                return true;
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.DELETE_VIDEO_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
        return false;
    }

    @Override
    public void auditVideoByAi(String accessKey, String secretKey, String region, String videoId) {
        DefaultAcsClient acsClient = null;
        try {
            SubmitAIMediaAuditJobRequest request = new SubmitAIMediaAuditJobRequest();
            request.setMediaId(videoId);
            acsClient = this.initVodClient(accessKey, secretKey, region);
            acsClient.getAcsResponse(request);
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.SUBMIT_AI_AUDIT_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
    }

    @Override
    public void auditVideoByManual(String accessKey, String secretKey, String region, String videoId) {
        DefaultAcsClient acsClient = null;
        try {
            CreateAuditRequest request = new CreateAuditRequest();
            List<JSONObject> auditContents = new ArrayList<>(10);
            JSONObject auditContent = new JSONObject();
            auditContent.put("VideoId", videoId);
            auditContent.put("Status", "Normal");
            auditContents.add(auditContent);
            request.setAuditContent(auditContents.toString());
            acsClient = this.initVodClient(accessKey, secretKey, region);
            acsClient.getAcsResponse(request);
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.SUBMIT_MANUAL_AUDIT_FAILED);
        } finally {
            closeAcsClient(acsClient);
        }
    }

    private void closeAcsClient(DefaultAcsClient acsClient) {
        if (Objects.nonNull(acsClient)) {
            acsClient.shutdown();
        }
    }
}
