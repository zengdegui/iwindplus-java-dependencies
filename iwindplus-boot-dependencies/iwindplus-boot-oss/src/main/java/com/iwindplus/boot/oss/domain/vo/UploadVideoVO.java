/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 视频点播视频文件上传视图对象
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadVideoVO implements Serializable {
    private static final long serialVersionUID = 7180768190452534872L;

    /**
     * 文件名称（不包含后缀）.
     */
    private String fileName;

    /**
     * 文件大小.
     */
    private Long fileSize;

    /**
     * 视频标识.
     */
    private String videoId;
}
