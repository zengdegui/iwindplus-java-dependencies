/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */
package com.iwindplus.boot.oss.domain.enumerate;

import com.iwindplus.boot.web.exception.CommonException;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * 业务编码返回值枚举.
 *
 * @author zengdegui
 * @since 2020/6/13
 */
@Getter
@Accessors(fluent = true)
public enum OssCodeEnum implements CommonException {
    /**
     * 文件找不到.
     */
    FILE_NOT_FOUND("file_not_found", "文件找不到"),

    /**
     * 文件太大.
     */
    FILE_TOO_BIG("file_too_big", "文件太大"),

    /**
     * 文件上传失败.
     */
    FILE_UPLOAD_FAILED("file_upload_failed", "文件上传失败"),

    /**
     * 文件下载失败.
     */
    FILE_DOWNLOAD_FAILED("file_download_failed", "文件下载失败"),

    /**
     * 创建目录失败.
     */
    CREATE_DIR_FAILED("create_dir_failed", "创建目录失败"),

    /**
     * 文件分片太小.
     */
    FILE_PART_FAILED("file_part_failed", "分片不能超过10000，请修改每片大小"),

    /**
     * 获取播放凭证失败.
     */
    GET_PLAY_AUTH_FAILED("get_play_auth_failed", "获取播放凭证失败"),

    /**
     * 获取视频信息失败.
     */
    GET_VIDEO_FAILED("get_video_failed", "获取视频信息失败"),

    /**
     * 获取源视频信息失败.
     */
    GET_SOURCE_VIDEO_FAILED("get_source_video_failed", "获取源视频信息失败"),

    /**
     * 删除视频信息失败.
     */
    DELETE_VIDEO_FAILED("delete_video_failed", "删除视频信息失败"),

    /**
     * 提交智能AI审核失败.
     */
    SUBMIT_AI_AUDIT_FAILED("submit_ai_audit_failed", "提交智能AI审核失败"),

    /**
     * 提交人工审核失败.
     */
    SUBMIT_MANUAL_AUDIT_FAILED("submit_manual_audit_failed", "提交人工审核失败");

    /**
     * 值.
     */
    private final String value;

    /**
     * 描述.
     */
    private final String desc;

    /**
     * 构造方法.
     *
     * @param value 值
     * @param desc  描述
     */
    OssCodeEnum(final String value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 通过描述查找枚举.
     *
     * @param desc 描述
     * @return OssCodeEnum
     */
    public static OssCodeEnum valueOfDesc(String desc) {
        for (OssCodeEnum val : OssCodeEnum.values()) {
            if (Objects.equals(desc, val.desc())) {
                return val;
            }
        }
        return null;
    }

    /**
     * 通过值查找枚举.
     *
     * @param value 值
     * @return OssCodeEnum
     */
    public static OssCodeEnum valueOfValue(String value) {
        for (OssCodeEnum val : OssCodeEnum.values()) {
            if (Objects.equals(value, val.value())) {
                return val;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.value;
    }

    @Override
    public String getMessage() {
        return this.desc;
    }
}
