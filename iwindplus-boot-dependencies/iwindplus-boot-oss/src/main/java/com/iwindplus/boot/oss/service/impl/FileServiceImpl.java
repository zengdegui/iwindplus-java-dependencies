/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service.impl;

import cn.hutool.core.date.DatePattern;
import com.iwindplus.boot.oss.domain.enumerate.OssCodeEnum;
import com.iwindplus.boot.oss.domain.vo.UploadVO;
import com.iwindplus.boot.oss.service.FileService;
import com.iwindplus.boot.util.DateUtil;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * 文件操作业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/14
 */
@Slf4j
public class FileServiceImpl extends AbstractOssServiceImpl implements FileService {
    /**
     * MultipartProperties.
     */
    @Autowired
    protected MultipartProperties multipartProperties;

    /**
     * ResourceLoader.
     */
    @Autowired
    protected ResourceLoader resourceLoader;

    @Override
    public UploadVO uploadFile(MultipartFile file, String prefix, Boolean flagRename, String fileName) {
        if (Objects.isNull(file)) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        long fileSize = file.getSize();
        super.checkFile(fileSize, this.multipartProperties.getMaxFileSize().toBytes());
        String rootPath;
        if (StringUtils.isNotBlank(this.multipartProperties.getLocation())) {
            rootPath = this.multipartProperties.getLocation();
        } else {
            rootPath = System.getProperty(USER_DIR);
        }
        String path = StringUtils.isNotBlank(prefix) ? prefix : UPLOAD_DIR;
        Path relativeDir = Paths.get(path, DateUtil.getStringDate(DatePattern.PURE_DATE_PATTERN));
        Path absoluteDir = Paths.get(rootPath).resolve(relativeDir);
        this.createDir(absoluteDir);
        String sourceFileName = file.getOriginalFilename();
        String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
        Path relativePath = relativeDir.resolve(newFileName);
        Path absolutePath = absoluteDir.resolve(newFileName);
        try {
            // 上传
            file.transferTo(absolutePath.toFile());
            UploadVO data = UploadVO.builder().sourceFileName(sourceFileName).fileName(newFileName).fileSize(fileSize)
                    .absolutePath(absolutePath.toString()).relativePath(relativePath.toString()).build();
            return data;
        } catch (IOException e) {
            log.error("IOException [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        }
    }

    @Override
    public void downloadFile(HttpServletResponse response, String relativePath, Boolean flagRename, String fileName, Boolean flagRemote) {
        InputStream inputStream = null;
        String newFileName;
        try {
            if (Objects.nonNull(flagRemote) && flagRemote) {
                File file = getFile(relativePath);
                inputStream = new FileInputStream(file);
                newFileName = super.getNewFileName(file.getName(), flagRename, fileName);
            } else {
                Resource resource = getResource(relativePath);
                inputStream = resource.getInputStream();
                newFileName = super.getNewFileName(resource.getFilename(), flagRename, fileName);
            }
            super.downloadFile(inputStream, newFileName, response);
        } catch (IOException e) {
            log.error("IOException [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_DOWNLOAD_FAILED);
        } finally {
            closeInputStream(inputStream);
        }
    }

    private File getFile(String relativePath) {
        Path path = Paths.get(this.multipartProperties.getLocation(), relativePath);
        File file = path.toFile();
        if (!file.exists()) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        return file;
    }

    private Resource getResource(String relativePath) {
        StringBuilder sb = new StringBuilder("classpath:").append(relativePath);
        Resource resource = this.resourceLoader.getResource(sb.toString());
        if (Objects.isNull(resource)) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        return resource;
    }

    private void createDir(Path path) {
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                throw new BaseException(OssCodeEnum.CREATE_DIR_FAILED);
            }
        }
    }
}
