/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service.impl;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.Credentials;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.common.auth.DefaultCredentials;
import com.aliyun.oss.common.comm.Protocol;
import com.aliyun.oss.model.AccessControlList;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CompleteMultipartUploadRequest;
import com.aliyun.oss.model.CompleteMultipartUploadResult;
import com.aliyun.oss.model.InitiateMultipartUploadRequest;
import com.aliyun.oss.model.InitiateMultipartUploadResult;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PartETag;
import com.aliyun.oss.model.PutObjectResult;
import com.aliyun.oss.model.UploadFileRequest;
import com.aliyun.oss.model.UploadFileResult;
import com.aliyun.oss.model.UploadPartRequest;
import com.aliyun.oss.model.UploadPartResult;
import com.iwindplus.boot.oss.domain.enumerate.OssCodeEnum;
import com.iwindplus.boot.oss.domain.vo.UploadVO;
import com.iwindplus.boot.oss.service.OssAliyunService;
import com.iwindplus.boot.util.DateUtil;
import com.iwindplus.boot.util.FileUtil;
import com.iwindplus.boot.web.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 阿里云对象存储业务层接口实现类.
 *
 * @author zengdegui
 * @since 2019/8/9
 */
@Slf4j
public class OssAliyunServiceImpl extends AbstractOssServiceImpl implements OssAliyunService {
    /**
     * MultipartProperties.
     */
    @Autowired
    protected MultipartProperties multipartProperties;

    @Override
    public UploadVO uploadFile(String accessKey, String secretKey, String region, String securityToken,
                               Long partSize, Boolean flagBreakpoint, String accessDomain, String bucketName,
                               MultipartFile file, String prefix, Boolean flagRename, String fileName) {
        if (Objects.isNull(file)) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.getSize(), this.multipartProperties.getMaxFileSize().toBytes());
        return getUploadVO(accessKey, secretKey, region, securityToken, partSize, flagBreakpoint,
                accessDomain, bucketName, file, null, prefix, flagRename, fileName);
    }

    @Override
    public UploadVO uploadFile(String accessKey, String secretKey, String region, String securityToken,
                               Long partSize, Boolean flagBreakpoint, String accessDomain, String bucketName,
                               String absolutePath, String prefix, Boolean flagRename, String fileName) {
        File file = new File(absolutePath);
        if (!file.exists()) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        super.checkFile(file.length(), this.multipartProperties.getMaxFileSize().toBytes());
        return getUploadVO(accessKey, secretKey, region, securityToken, partSize, flagBreakpoint,
                accessDomain, bucketName, null, file, prefix, flagRename, fileName);
    }

    @Override
    public boolean uploadFile(String accessKey, String secretKey, String region, String securityToken,
                              Long partSize, Boolean flagBreakpoint, String bucketName,
                              MultipartFile file, String relativePath) {
        if (Objects.isNull(file)) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        return getUploadResult(accessKey, secretKey, region, securityToken,
                partSize, flagBreakpoint, bucketName, file, null, relativePath);
    }

    @Override
    public boolean uploadFile(String accessKey, String secretKey, String region, String securityToken,
                              Long partSize, Boolean flagBreakpoint, String bucketName,
                              String absolutePath, String relativePath) {
        File file = new File(absolutePath);
        if (!file.exists()) {
            throw new BaseException(OssCodeEnum.FILE_NOT_FOUND);
        }
        return getUploadResult(accessKey, secretKey, region, securityToken,
                partSize, flagBreakpoint, bucketName, null, file, relativePath);
    }

    @Override
    public boolean getUploadBySimple(String accessKey, String secretKey, String region, String securityToken, String bucketName, MultipartFile file, String relativePath) {
        return getUploadBySimple(accessKey, secretKey, region, securityToken,
                bucketName, file, null, relativePath);
    }

    @Override
    public String getSignUrl(String accessKey, String secretKey, String region, String securityToken,
                             String accessDomain, String bucketName, String relativePath, Long timeout) {
        OSSClient ossClient = this.getOssClient(accessKey, secretKey, region, securityToken);
        AccessControlList bucketAcl = ossClient.getBucketAcl(bucketName);
        if (CannedAccessControlList.Private.compareTo(bucketAcl.getCannedACL()) == 0) {
            Date expiration = DateUtil.
                    offsetMinute(new Date(), Objects.isNull(timeout) ? URL_TIMEOUT : timeout.intValue())
                    .toJdkDate();
            URL signUrl = ossClient.generatePresignedUrl(bucketName, relativePath, expiration);
            String url = signUrl.toString();
            closeOssClient(ossClient);
            if (StringUtils.isNotBlank(accessDomain)) {
                String str = StringUtils.substringBefore(StringUtils.substringAfter(url, "https://"), "/");
                return StringUtils.replace(url, str, StringUtils.substringAfter(accessDomain, "https://"));
            }
            return url;
        } else {
            if (StringUtils.isNotBlank(accessDomain)) {
                return new StringBuilder(accessDomain).append("/")
                        .append(relativePath)
                        .toString();
            } else {
                return new StringBuilder("https://").append(bucketName)
                        .append(".").append(region).append("/")
                        .append(relativePath).toString();
            }
        }
    }

    @Override
    public void downloadFile(String accessKey, String secretKey, String region, String securityToken,
                             String bucketName, HttpServletResponse response, String relativePath,
                             Boolean flagRename, String fileName) {
        OSSClient ossClient = this.getOssClient(accessKey, secretKey, region, securityToken);
        OSSObject ossObject = ossClient.getObject(bucketName, relativePath);
        InputStream inputStream = ossObject.getObjectContent();
        if (Objects.nonNull(inputStream)) {
            String sourceFileName = FileUtil.getName(relativePath);
            String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
            super.downloadFile(inputStream, newFileName, response);
            closeOssClient(ossClient);
        }
    }

    public boolean getUploadResult(String accessKey, String secretKey, String region, String securityToken,
                                   Long partSize, Boolean flagBreakpoint, String bucketName,
                                   MultipartFile multipartFile, File file, String relativePath) {
        long fileSize = getFileSize(multipartFile, file);
        return getResult(accessKey, secretKey, region, securityToken, partSize, flagBreakpoint, bucketName, multipartFile, file, fileSize, relativePath);
    }

    private UploadVO getUploadVO(String accessKey, String secretKey, String region, String securityToken,
                                 Long partSize, Boolean flagBreakpoint, String accessDomain, String bucketName,
                                 MultipartFile multipartFile, File file, String prefix, Boolean flagRename, String fileName) {
        String sourceFileName = null;
        long fileSize = 0;
        if (Objects.nonNull(multipartFile)) {
            sourceFileName = multipartFile.getOriginalFilename();
            fileSize = multipartFile.getSize();
        }
        if (Objects.nonNull(file)) {
            sourceFileName = file.getName();
            fileSize = file.length();
        }
        String newFileName = super.getNewFileName(sourceFileName, flagRename, fileName);
        String relativePath = super.getRelativePath(prefix, newFileName);
        boolean result = getResult(accessKey, secretKey, region, securityToken, partSize, flagBreakpoint, bucketName, multipartFile, file, fileSize, relativePath);
        if (result) {
            return getResponse(accessKey, secretKey, region, securityToken, accessDomain,
                    bucketName, fileSize, sourceFileName, relativePath, fileName);
        }
        return null;
    }

    private boolean getResult(String accessKey, String secretKey, String region, String securityToken,
                              Long partSize, Boolean flagBreakpoint, String bucketName, MultipartFile multipartFile,
                              File file, long fileSize, String relativePath) {
        boolean result;
        if (fileSize >= 4194304) {
            Long sliceSize = Objects.isNull(partSize) ? 200 * 1024 * 1024 : partSize * 1024 * 1024;
            if (Objects.nonNull(flagBreakpoint) && flagBreakpoint) {
                result = getUploadByBreakpoint(accessKey, secretKey, region, securityToken, sliceSize, bucketName, multipartFile, file, relativePath);
            } else {
                result = getUploadByPart(accessKey, secretKey, region, securityToken, sliceSize, bucketName, multipartFile, file, relativePath);
            }
        } else {
            result = getUploadBySimple(accessKey, secretKey, region, securityToken, bucketName, multipartFile, file, relativePath);
        }
        return result;
    }

    private boolean getUploadBySimple(String accessKey, String secretKey, String region, String securityToken,
                                      String bucketName, MultipartFile multipartFile, File file, String relativePath) {
        OSSClient ossClient = null;
        InputStream inputStream = null;
        try {
            inputStream = getInputStream(multipartFile, file);
            ossClient = this.getOssClient(accessKey, secretKey, region, securityToken);
            PutObjectResult response = ossClient.putObject(bucketName, relativePath, inputStream);
            if (Objects.nonNull(response)) {
                return true;
            }
        } catch (IOException e) {
            log.error("IOException [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        } finally {
            closeInputStream(inputStream);
            closeOssClient(ossClient);
        }
        return false;
    }

    private boolean getUploadByBreakpoint(String accessKey, String secretKey, String region, String securityToken,
                                          Long partSize, String bucketName, MultipartFile multipartFile, File file, String relativePath) {
        OSSClient ossClient = null;
        File tempFile = null;
        String uploadFile = null;
        try {
            if (Objects.nonNull(multipartFile)) {
                tempFile = FileUtil.multipartFileToFile(multipartFile);
                uploadFile = tempFile.getAbsolutePath();
            }
            if (Objects.nonNull(file)) {
                uploadFile = file.getAbsolutePath();
            }
            Long sliceSize = partSize * 1024 * 1024;
            UploadFileRequest uploadFileRequest = new UploadFileRequest(bucketName, relativePath, uploadFile,
                    sliceSize, 5, true);
            ossClient = this.getOssClient(accessKey, secretKey, region, securityToken);
            UploadFileResult uploadFileResult = ossClient.uploadFile(uploadFileRequest);
            CompleteMultipartUploadResult completeMultipartUploadResult = uploadFileResult.getMultipartUploadResult();
            if (Objects.nonNull(completeMultipartUploadResult)) {
                return true;
            }
        } catch (Throwable e) {
            log.error("Throwable [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        } finally {
            super.fileDelete(tempFile);
            closeOssClient(ossClient);
        }
        return false;
    }

    private boolean getUploadByPart(String accessKey, String secretKey, String region, String securityToken,
                                    Long partSize, String bucketName, MultipartFile multipartFile, File file, String relativePath) {
        OSSClient ossClient = null;
        long fileSize = getFileSize(multipartFile, file);
        try {
            ossClient = this.getOssClient(accessKey, secretKey, region, securityToken);
            // 创建InitiateMultipartUploadRequest对象。
            InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, relativePath);
            // 初始化分片。
            InitiateMultipartUploadResult upResult = ossClient.initiateMultipartUpload(request);
            // 返回uploadId，它是分片上传事件的唯一标识，您可以根据这个uploadId发起相关的操作，如取消分片上传、查询分片上传等。
            String uploadId = upResult.getUploadId();
            // 返回uploadId，它是分片上传事件的唯一标识，可以根据这个ID来发起相关操作，如取消分片上传、查询分片上传等
            List<PartETag> partTags = listPartETag(bucketName, multipartFile, file, relativePath, ossClient, fileSize, uploadId, partSize);
            // 在执行完成分片上传操作时，需要提供所有有效的partETags。OSS收到提交的partETags后，会逐一验证每个分片的有效性。当所有的数据分片验证通过后，OSS将把这些分片组合成一个完整的文件。
            CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucketName, relativePath, uploadId, partTags);
            // 完成上传。
            CompleteMultipartUploadResult completeMultipartUploadResult = ossClient.completeMultipartUpload(completeMultipartUploadRequest);
            if (Objects.nonNull(completeMultipartUploadResult)) {
                return true;
            }
        } catch (Exception e) {
            log.error("Exception [{}]", e.getMessage());
            throw new BaseException(OssCodeEnum.FILE_UPLOAD_FAILED);
        } finally {
            closeOssClient(ossClient);
        }
        return false;
    }

    private List<PartETag> listPartETag(String bucketName, MultipartFile multipartFile, File file, String relativePath,
                                        OSSClient ossClient, long fileSize, String uploadId, Long partSize) throws IOException {
        // 每个分片的大小，用于计算文件有多少个分片。单位为字节。
        Long sliceSize = partSize * 1024 * 1024;
        int partCount = (int) (fileSize / sliceSize);
        if (fileSize % sliceSize != 0) {
            partCount++;
        }
        if (partCount > 10000) {
            throw new BaseException(OssCodeEnum.FILE_PART_FAILED);
        }
        InputStream inputStream = null;
        try {
            List<PartETag> partTags = new ArrayList<>(10);
            for (int ii = 0; ii < partCount; ii++) {
                long startPos = ii * sliceSize;
                int partNumber = ii + 1;
                long curPartSize = (partNumber == partCount) ? (fileSize - startPos) : sliceSize;
                inputStream = getInputStream(multipartFile, file);
                // 跳过已经上传的分片
                inputStream.skip(startPos);
                UploadPartRequest uploadPartRequest = new UploadPartRequest();
                uploadPartRequest.setBucketName(bucketName);
                uploadPartRequest.setKey(relativePath);
                uploadPartRequest.setUploadId(uploadId);
                uploadPartRequest.setInputStream(inputStream);
                // 设置分片大小。除了最后一个分片没有大小限制，其他分片最小为100KB
                uploadPartRequest.setPartSize(curPartSize);
                // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出这个范围,OSS将返回InvalidArgum的错误码
                uploadPartRequest.setPartNumber(partNumber);
                // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会根据分片号排序组成完整的文件。
                UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
                // 每次上传分片之后，OSS的返回结果会包含一个PartETag。PartETag将被保存到PartETags中。
                partTags.add(uploadPartResult.getPartETag());
            }
            return partTags;
        } finally {
            closeInputStream(inputStream);
        }
    }

    private InputStream getInputStream(MultipartFile multipartFile, File file) throws IOException {
        if (Objects.nonNull(multipartFile)) {
            return multipartFile.getInputStream();
        }
        if (Objects.nonNull(file)) {
            return new FileInputStream(file);
        }
        return null;
    }

    private long getFileSize(MultipartFile multipartFile, File file) {
        if (Objects.nonNull(multipartFile)) {
            return multipartFile.getSize();
        }
        if (Objects.nonNull(file)) {
            return file.length();
        }
        return 0;
    }

    private OSSClient getOssClient(String accessKey, String secretKey, String region, String securityToken) {
        ClientConfiguration conf = new ClientConfiguration();
        conf.setIdleConnectionTime(1000);
        conf.setSupportCname(true);
        conf.setProtocol(Protocol.HTTPS);
        Credentials credentials = new DefaultCredentials(accessKey, secretKey, securityToken);
        return new OSSClient(region, new DefaultCredentialProvider(credentials), conf);
    }

    private void closeOssClient(OSSClient ossClient) {
        if (Objects.nonNull(ossClient)) {
            ossClient.shutdown();
        }
    }

    private UploadVO getResponse(String accessKey, String secretKey, String region, String securityToken,
                                 String accessDomain, String bucketName, long fileSize,
                                 String sourceFileName, String relativePath, String fileName) {
        String absolutePath = this.getSignUrl(accessKey, secretKey, region, securityToken,
                accessDomain, bucketName, relativePath, null);
        return UploadVO.builder()
                .sourceFileName(sourceFileName)
                .fileName(fileName)
                .fileSize(fileSize)
                .relativePath(relativePath)
                .absolutePath(absolutePath)
                .build();
    }
}
