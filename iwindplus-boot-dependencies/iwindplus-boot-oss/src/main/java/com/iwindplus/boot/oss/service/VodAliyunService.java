/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.oss.service;

import com.aliyuncs.vod.model.v20170321.GetMezzanineInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoInfoResponse;
import com.iwindplus.boot.oss.domain.vo.UploadVideoVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 阿里云视频点播业务层接口类.
 *
 * @author zengdegui
 * @since 2022/1/14
 */
public interface VodAliyunService {
    /**
     * 视频文件上传.
     *
     * @param accessKey  访问key（必填）
     * @param secretKey  密钥（必填）
     * @param region     服务器区域（必填）
     * @param file       文件（必填）
     * @param flagRename 是否重命名文件名（可选）
     * @param fileName   新文件名，不包含文件后缀（可选）
     * @return UploadVideoVO
     */
    UploadVideoVO uploadVideo(String accessKey, String secretKey, String region, MultipartFile file,
                              Boolean flagRename, String fileName);

    /**
     * 获取播放凭证.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoId   视频标识（必填）
     * @param timeout   过期时间（可选，单位：分钟，默认：60）
     * @return String
     */
    String getPlayAuth(String accessKey, String secretKey, String region, String videoId, Long timeout);

    /**
     * 获取视频信息.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoId   视频标识（必填）
     * @return GetVideoInfoResponse.Video
     */
    GetVideoInfoResponse.Video getVideoInfo(String accessKey, String secretKey, String region, String videoId);

    /**
     * 获取源视频信息.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoId   视频标识（必填）
     * @return GetMezzanineInfoResponse.Mezzanine
     */
    GetMezzanineInfoResponse.Mezzanine getSourceVideoInfo(String accessKey, String secretKey, String region, String videoId);

    /**
     * 删除视频.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoIds  视频标识集合（必填）
     * @return boolean
     */
    boolean removeVideo(String accessKey, String secretKey, String region, List<String> videoIds);

    /**
     * 提交智能AI审核作业.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoId   视频标识（必填）
     */
    void auditVideoByAi(String accessKey, String secretKey, String region, String videoId);

    /**
     * 提交人工审核作业.
     *
     * @param accessKey 访问key（必填）
     * @param secretKey 密钥（必填）
     * @param region    服务器区域（必填）
     * @param videoId   视频标识（必填）
     */
    void auditVideoByManual(String accessKey, String secretKey, String region, String videoId);
}
