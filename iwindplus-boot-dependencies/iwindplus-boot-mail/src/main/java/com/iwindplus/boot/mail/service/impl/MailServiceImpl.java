/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.mail.service.impl;

import com.iwindplus.boot.mail.domain.dto.MailDTO;
import com.iwindplus.boot.mail.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

/**
 * 邮箱业务层接口实现类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Slf4j
public class MailServiceImpl implements MailService {
    @Override
    public void send(MailDTO entity) throws Exception {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(entity.getHost());
        sender.setUsername(entity.getUsername());
        sender.setPassword(entity.getPassword());
        sender.setDefaultEncoding("UTF-8");
        Properties props = new Properties();
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.starttls.required", "true");
        // ssl配置
        if (entity.getSslEnable()) {
            sender.setPort(entity.getPort());
            sender.setProtocol(entity.getProtocol());
            props.setProperty("mail.smtp.ssl.enable", "true");
            props.setProperty("mail.smtp.socketFactory.port", entity.getPort().toString());
            props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }
        sender.setJavaMailProperties(props);

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(entity.getTo().stream().toArray(String[]::new));
        if (CollectionUtils.isNotEmpty(entity.getCc())) {
            helper.setCc(entity.getCc().stream().toArray(String[]::new));
        }
        if (CollectionUtils.isNotEmpty(entity.getBcc())) {
            helper.setBcc(entity.getBcc().stream().toArray(String[]::new));
        }
        helper.setFrom(new InternetAddress(sender.getUsername(), entity.getNickName(), "UTF-8"));
        helper.setSubject(entity.getSubject());
        helper.setText(entity.getContent(), true);
        this.addAttachments(helper, entity.getAttachments());
        sender.send(message);
    }

    /**
     * 添加附件.
     *
     * @param helper      helper
     * @param attachments 附件
     */
    private void addAttachments(MimeMessageHelper helper, List<MultipartFile> attachments) {
        if (CollectionUtils.isNotEmpty(attachments)) {
            attachments.forEach(attachment -> {
                InputStreamSource inputStreamSource = () -> attachment.getInputStream();
                try {
                    helper.addAttachment(attachment.getOriginalFilename(), inputStreamSource);
                } catch (MessagingException e) {
                    log.error("Messaging Exception [{}]", e.getMessage());
                }
            });
        }
    }
}
