/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.enums.RowTypeEnum;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.holder.ReadRowHolder;
import com.iwindplus.boot.util.ValidationUtil;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * easyexcel表格导入监听器.
 *
 * @param <T>
 * @author zengdegui
 * @since 2021/8/5
 */
public class BaseEventListener<T> extends AnalysisEventListener<T> {
    /**
     * 解析数据的临时存储容器.
     */
    private final List<T> rightList = new ArrayList<>(10);

    /**
     * 错误数据.
     */
    private final List<ExcelData<T>> errorList = new ArrayList<>(10);

    /**
     * 是否校验必填.
     */
    private final Boolean flagValid;

    /**
     * 国际化.
     */
    private final Validator validator;

    /**
     * 校验分组.
     */
    private final Class<?>[] groups;

    /**
     * 校验通过消费解析得到的excel数据.
     */
    private final Consumer<List<T>> rightConsumer;

    /**
     * 校验错误消费解析得到的excel数据.
     */
    private final Consumer<List<ExcelData<T>>> errorConsumer;

    /**
     * 构造方法.
     *
     * @param flagValid     是否校验必填
     * @param validator     国际化
     * @param groups        校验分组
     * @param rightConsumer 校验通过消费解析得到的excel数据
     * @param errorConsumer 校验错误消费解析得到的excel数据
     */
    public BaseEventListener(Boolean flagValid, Validator validator, Class<?>[] groups, Consumer<List<T>> rightConsumer, Consumer<List<ExcelData<T>>> errorConsumer) {
        this.flagValid = flagValid;
        this.validator = validator;
        this.groups = groups;
        this.rightConsumer = rightConsumer;
        this.errorConsumer = errorConsumer;
    }

    @Override
    public void invoke(T data, AnalysisContext context) {
        ReadRowHolder readRowHolder = context.readRowHolder();
        if (RowTypeEnum.EMPTY == readRowHolder.getRowType()) {
            ExcelData<T> errorData = new ExcelData<>();
            errorData.setMsg("表格数据不能为空");
            this.errorList.add(errorData);
            this.rightList.clear();
            return;
        }
        Integer rowIndex = readRowHolder.getRowIndex() + 1;
        if (Objects.nonNull(this.flagValid) && this.flagValid) {
            String validateMsg = ValidationUtil.validateEntity(data, this.validator, this.groups);
            if (StringUtils.isNotBlank(validateMsg)) {
                String msg = new StringBuilder("第").append(rowIndex).append("行，触发约束：").append(validateMsg).toString();
                ExcelData<T> excelData = new ExcelData<>();
                excelData.setData(data);
                excelData.setMsg(msg);
                this.errorList.add(excelData);
                this.rightList.clear();
                return;
            }
        }
        this.rightList.add(data);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        if (CollectionUtils.isNotEmpty(this.errorList)) {
            this.errorConsumer.accept(this.errorList);
            this.errorList.clear();
            return;
        }
        if (CollectionUtils.isNotEmpty(this.rightList)) {
            this.rightConsumer.accept(this.rightList);
            this.rightList.clear();
        }
    }

    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        if (CollectionUtils.isNotEmpty(this.errorList)) {
            this.errorList.clear();
        }
        if (CollectionUtils.isNotEmpty(this.rightList)) {
            this.rightList.clear();
        }
        throw exception;
    }

    /**
     * 数据对象.
     *
     * @param <T> 对象
     */
    @Data
    public static class ExcelData<T> {
        private T data;
        private String msg;
    }
}
