/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.excel.listener;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.iwindplus.boot.util.ValidationUtil;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Validator;

/**
 * easypoi表格导入校验.
 *
 * @param <T> 对象
 * @author zengdegui
 * @since 2021/8/2
 */
public class BaseVerifyHandler<T> implements IExcelVerifyHandler<T> {
	/**
	 * 国际化.
	 */
	private final Validator validator;

	/**
	 * 校验分组.
	 */
	private final Class<?>[] groups;

	/**
	 * 构造方法.
	 *
	 * @param validator 国际化
	 * @param groups    校验分组
	 */
	public BaseVerifyHandler(Validator validator, Class<?>[] groups) {
		this.validator = validator;
		this.groups = groups;
	}

	@Override
	public ExcelVerifyHandlerResult verifyHandler(T data) {
		String errorMsg = ValidationUtil.validateEntity(data, this.validator, this.groups);
		if (StringUtils.isNotBlank(errorMsg)) {
			return new ExcelVerifyHandlerResult(false, errorMsg);
		}
		return new ExcelVerifyHandlerResult(true);
	}
}
