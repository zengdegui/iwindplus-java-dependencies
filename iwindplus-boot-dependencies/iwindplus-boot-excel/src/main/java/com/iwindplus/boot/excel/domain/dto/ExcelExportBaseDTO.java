/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.boot.excel.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * excel导出基础通用实体类.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelExportBaseDTO implements Serializable {
	private static final long serialVersionUID = 5705091198076903621L;

	/**
	 * 单sheet最大值（03版本默认6W行,07默认100W）.
	 */
	private Integer maxNum;

	/**
	 * 表格标题.
	 */
	private String title;

	/**
	 * 表格sheet名称.
	 */
	private String sheetName;

	/**
	 * 导出文件名（可选）.
	 */
	private String fileName;

	/**
	 * 响应（必填）.
	 */
	private HttpServletResponse response;
}
