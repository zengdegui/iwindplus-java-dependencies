redis模块

一、显性对接
    1、在配置文件（yml,properties）中配置redis相关属性
    2、注入并调用RedisTemplate
一、注解缓存对接
    1、在配置文件（yml,properties）中配置redis相关属性
    2、在需要使用缓存的方法上加@Cacheable @CacheEvict @CachePut注解即可,表达式规则请百度
	