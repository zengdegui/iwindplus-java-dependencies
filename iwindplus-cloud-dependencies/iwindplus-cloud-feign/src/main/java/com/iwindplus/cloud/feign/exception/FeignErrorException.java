/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.feign.exception;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * 自定义运行时异常.
 *
 * @author zengdegui
 * @since 2018/9/1
 */
@Getter
public class FeignErrorException extends HystrixBadRequestException {
    private static final long serialVersionUID = -6783551049743441442L;

    /**
     * 状态码.
     */
    protected HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

    /**
     * 错误编码.
     */
    protected String code;

    /**
     * 数据.
     */
    protected Object data;

    /**
     * 构造方法.
     *
     * @param code    错误编码
     * @param message 错误消息
     */
    public FeignErrorException(String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 构造方法.
     *
     * @param code    错误编码
     * @param message 错误消息
     * @param data    数据
     */
    public FeignErrorException(String code, String message, Object data) {
        this(code, message);
        this.data = data;
    }
}
