oauth2认证模块
该模块功能主要封装了oauth2认证授权相关功能:
1、程序中获取用户信息，调用Oauth2Util相关方法

一、oauth2认证服务端搭建对接流程
   1、启动类开启注解@EnableOauth2Server
   2、在配置文件（yml,properties）中配置Oauth2Property类中相关属性
   3、程序中实现CustomClientDetailsService，CustomUserDetailsService
一、oauth2客户端对接（针对项目需要单个校验token等资源权限，如果统一校验可通过gateway去处理）
   1、启动类开启注解@EnableOauth2ClientResouce，即可对访问资源权限校验
   2、在配置文件（yml,properties）中配置Oauth2Property类中相关属性
   