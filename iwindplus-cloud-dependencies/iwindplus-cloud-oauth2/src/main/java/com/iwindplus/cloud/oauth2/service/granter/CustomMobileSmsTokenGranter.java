/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service.granter;

import com.iwindplus.cloud.oauth2.domain.constant.Oauth2Constant;
import com.iwindplus.cloud.oauth2.domain.vo.UserDetailsVO;
import com.iwindplus.cloud.oauth2.service.CustomUserDetailsService;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.Map;

/**
 * 自定义手机短信登录方式.
 *
 * @author zengdegui
 * @since 2020/5/8
 */
public class CustomMobileSmsTokenGranter extends AbstractCustomTokenGranter {
    private CustomUserDetailsService userDetailsService;

    public CustomMobileSmsTokenGranter(CustomUserDetailsService userDetailsService,
            AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
            OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory, Oauth2Constant.SMS);
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected UserDetailsVO getCustomUser(Map<String, String> parameters) {
        String mobile = parameters.get("mobile");
        String captcha = parameters.get("captcha");
        String clientId = parameters.get(Oauth2Constant.CLIENT_ID);
        return this.userDetailsService.loadUserByMobileAndCaptcha(clientId, mobile, captcha);
    }
}
