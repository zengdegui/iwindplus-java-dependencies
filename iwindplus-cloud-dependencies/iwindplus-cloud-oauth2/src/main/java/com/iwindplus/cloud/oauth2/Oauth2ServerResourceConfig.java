/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2;

import com.iwindplus.cloud.oauth2.domain.Oauth2Property;
import com.iwindplus.cloud.oauth2.domain.constant.Oauth2Constant;
import com.iwindplus.cloud.oauth2.exception.CustomAccessDeniedHandler;
import com.iwindplus.cloud.oauth2.exception.CustomAuthenticationEntryPoint;
import com.iwindplus.cloud.oauth2.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;

import javax.sql.DataSource;

/**
 * oauth2服务端资源配置.
 *
 * @author zengdegui
 * @since 2020/3/19
 */
public class Oauth2ServerResourceConfig extends ResourceServerConfigurerAdapter {
	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;

	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private Oauth2Property oauth2Property;

	@Autowired
	private DataSource dataSource;

	/**
	 * 创建tokenRepository.
	 *
	 * @return JdbcTokenRepositoryImpl
	 */
	@Bean
	public JdbcTokenRepositoryImpl tokenRepository() {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(this.dataSource);
		return tokenRepository;
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenStore(this.tokenStore)
				.authenticationEntryPoint(this.customAuthenticationEntryPoint)
				.accessDeniedHandler(this.customAccessDeniedHandler);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
				.and()
				.authorizeRequests()
				.antMatchers(HttpMethod.OPTIONS).permitAll()
				// 监控端点内部放行
				.requestMatchers(EndpointRequest.toAnyEndpoint())
				.permitAll()
				.antMatchers(this.oauth2Property.getWhiteList())
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.formLogin()
				.defaultSuccessUrl("/index")
				.permitAll()
				.and()
				.logout()
				// logout退出清除cookie
				.addLogoutHandler(
						new CookieClearingLogoutHandler(Oauth2Constant.ACCESS_TOKEN, Oauth2Constant.REMEMBER_ME_KEY))
				.and()
				.rememberMe()
				.key(Oauth2Constant.REMEMBER_ME_KEY)
				.tokenRepository(this.tokenRepository())
				.tokenValiditySeconds(60 * 60 * 24 * 7)
				.userDetailsService(this.userDetailsService)
				.and()
				.exceptionHandling()
				.accessDeniedHandler(this.customAccessDeniedHandler)
				.authenticationEntryPoint(this.customAuthenticationEntryPoint)
				.and()
				.cors()
				.and()
				.csrf()
				.disable()
				// 禁用httpBasic
				.httpBasic()
				.disable();
	}
}
