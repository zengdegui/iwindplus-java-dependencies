/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2;

import com.iwindplus.cloud.oauth2.exception.CustomAccessDeniedHandler;
import com.iwindplus.cloud.oauth2.exception.CustomAuthenticationEntryPoint;
import com.iwindplus.cloud.oauth2.service.Md5PasswordEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

/**
 * oauth2配置.
 *
 * @author zengdegui
 * @since 2020/11/8
 */
@Slf4j
@Configuration
public class Oauth2Config {
	@Autowired
	private RedisConnectionFactory connectionFactory;

	/**
	 * 默认加密配置.
	 *
	 * @return PasswordEncoder
	 */
	@Bean
	@ConditionalOnMissingBean(Md5PasswordEncoder.class)
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new Md5PasswordEncoder();
		log.info("Md5PasswordEncoder [{}]", encoder);
		return encoder;
	}

	/**
	 * 创建CustomAccessDeniedHandler.
	 *
	 * @return CustomAccessDeniedHandler
	 */
	@Bean
	@ConditionalOnMissingBean
	public CustomAccessDeniedHandler customAccessDeniedHandler() {
		CustomAccessDeniedHandler customAccessDeniedHandler = new CustomAccessDeniedHandler();
		log.info("CustomAccessDeniedHandler [{}]", customAccessDeniedHandler);
		return customAccessDeniedHandler;
	}

	/**
	 * 创建CustomAuthenticationEntryPoint.
	 *
	 * @return CustomAuthenticationEntryPoint
	 */
	@Bean
	@ConditionalOnMissingBean
	public CustomAuthenticationEntryPoint customAuthenticationEntryPoint() {
		CustomAuthenticationEntryPoint customAuthenticationEntryPoint = new CustomAuthenticationEntryPoint();
		log.info("CustomAuthenticationEntryPoint [{}]", customAuthenticationEntryPoint);
		return customAuthenticationEntryPoint;
	}

	/**
	 * 配置token存储，这个配置token存到redis中.
	 *
	 * @return TokenStore
	 */
	@Bean
	@ConditionalOnMissingBean
	public TokenStore tokenStore() {
		RedisTokenStore redisTokenStore = new RedisTokenStore(this.connectionFactory);
		log.info("RedisTokenStore [{}]", redisTokenStore);
		return redisTokenStore;
	}

	@Bean
	@ConditionalOnMissingBean
	public DefaultTokenServices tokenServices() {
		final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(this.tokenStore());
		log.info("DefaultTokenServices [{}]", defaultTokenServices);
		return defaultTokenServices;
	}
}
