/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service;

import org.springframework.security.oauth2.provider.ClientDetailsService;

/**
 * 自定义客户端详情业务层接口.
 *
 * @author zengdegui
 * @since 2021/11/17
 */
public interface CustomClientDetailsService extends ClientDetailsService {
}
