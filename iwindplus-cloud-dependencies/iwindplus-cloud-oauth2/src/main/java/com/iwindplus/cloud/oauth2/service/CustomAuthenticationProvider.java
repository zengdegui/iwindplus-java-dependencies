/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service;

import com.iwindplus.boot.web.i18n.I18nConfig;
import com.iwindplus.cloud.oauth2.domain.enumerate.Oauth2CodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;

/**
 * 自定义DaoAuthenticationProvider服务.
 *
 * @author zengdegui
 * @since 2020/4/24
 */
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {
    @Autowired
    private I18nConfig i18nConfig;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (Objects.isNull(authentication.getCredentials())) {
            this.logger.debug("Authentication failed: no credentials provided");
            throw new BadCredentialsException(this.i18nConfig.getMessage(Oauth2CodeEnum.INVALID_ACCOUNT_OR_CLIENT.desc()));
        } else {
            String presentedPassword = authentication.getCredentials().toString();
            if (!super.getPasswordEncoder().matches(presentedPassword, userDetails.getPassword())) {
                this.logger.debug("Authentication failed: password does not match stored value");
                throw new BadCredentialsException(this.i18nConfig.getMessage(Oauth2CodeEnum.INVALID_CREDENTIAL.desc()));
            }
        }
    }
}
