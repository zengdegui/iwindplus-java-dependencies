/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义oauth2配置.
 *
 * @author zengdegui
 * @since 2020/4/24
 */
@Data
@ConfigurationProperties(prefix = "oauth2", ignoreUnknownFields = true)
public class Oauth2Property {
    /**
     * refresh_token有两种使用方式：重复使用(true)、非重复使用(false)，默认为true
     * 1.重复使用：access_token过期刷新时， refresh token过期时间未改变，仍以初次生成的时间为准
     * 2.非重复使用：access_token过期刷新时， refresh_token过期时间延续，在refresh_token有效期内刷新而无需失效再次登录
     */
    private Boolean reuseRefreshToken = true;

    /**
     * 白名单（url地址）.
     */
    private String[] whiteList;
}
