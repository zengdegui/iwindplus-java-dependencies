/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service.granter;

import com.iwindplus.boot.web.exception.BaseException;
import com.iwindplus.cloud.oauth2.domain.vo.UserDetailsVO;
import com.iwindplus.cloud.web.domain.enumerate.CloudWebCodeEnum;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Map;
import java.util.Objects;

/**
 * 抽象oauth2验证方式.
 *
 * @author zengdegui
 * @since 2020/9/20
 */
public abstract class AbstractCustomTokenGranter extends AbstractTokenGranter {
    private final OAuth2RequestFactory requestFactory;

    protected AbstractCustomTokenGranter(AuthorizationServerTokenServices tokenServices,
                                         ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, String grantType) {
        super(tokenServices, clientDetailsService, requestFactory, grantType);
        this.requestFactory = requestFactory;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = tokenRequest.getRequestParameters();
        UserDetailsVO customUser = this.getCustomUser(parameters);
        if (Objects.isNull(customUser)) {
            throw new BaseException(CloudWebCodeEnum.DATA_NOT_EXIST);
        }
        OAuth2Request storedOauth2Request = this.requestFactory.createOAuth2Request(client, tokenRequest);
        PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(customUser, null, customUser.getAuthorities());
        authentication.setDetails(customUser);
        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(storedOauth2Request, authentication);
        return oAuth2Authentication;
    }

    /**
     * 查询用户信息
     *
     * @param parameters map参数
     * @return UserDetailsVO
     */
    protected abstract UserDetailsVO getCustomUser(Map<String, String> parameters);
}
