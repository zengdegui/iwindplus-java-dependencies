package com.iwindplus.cloud.oauth2.domain.vo;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * 自定义认证用户信息.
 *
 * @author zengdegui
 * @since 2020/4/3
 */
public class UserDetailsVO implements UserDetails, CredentialsContainer {
	private static final long serialVersionUID = -123308657146774881L;

	/**
	 * 用户唯一标识.
	 */
	private String openid;

	/**
	 * 登录名.
	 */
	private String username;

	/**
	 * 密码.
	 */
	private String password;

	/**
	 * 客户端.
	 */
	private String clientId;

	/**
	 * 用户权限.
	 */
	private Collection<GrantedAuthority> authorities;

	/**
	 * 是否已锁定.
	 */
	private Boolean accountNonLocked;

	/**
	 * 是否已过期.
	 */
	private Boolean accountNonExpired;

	/**
	 * 是否启用.
	 */
	private Boolean enabled;

	/**
	 * 密码是否已过期.
	 */
	private Boolean credentialsNonExpired;

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}

	public void setAccountNonExpired(Boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public void eraseCredentials() {
		this.password = null;
	}

	@Override
	public String toString() {
		return "UserDetailsVO{" +
				"openid='" + openid + '\'' +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", clientId='" + clientId + '\'' +
				", authorities=" + authorities +
				", accountNonLocked=" + accountNonLocked +
				", accountNonExpired=" + accountNonExpired +
				", enabled=" + enabled +
				", credentialsNonExpired=" + credentialsNonExpired +
				'}';
	}
}
