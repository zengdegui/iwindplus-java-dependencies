/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2;

import com.iwindplus.cloud.oauth2.domain.Oauth2Property;
import com.iwindplus.cloud.oauth2.exception.CustomAccessDeniedHandler;
import com.iwindplus.cloud.oauth2.exception.CustomAuthenticationEntryPoint;
import com.iwindplus.cloud.oauth2.exception.CustomWebResponseExceptionTranslator;
import com.iwindplus.cloud.oauth2.service.CustomClientDetailsService;
import com.iwindplus.cloud.oauth2.service.CustomUserDetailsService;
import com.iwindplus.cloud.oauth2.service.RedisAuthorizationCodeServices;
import com.iwindplus.cloud.oauth2.service.granter.CustomBindGrantTokenGranter;
import com.iwindplus.cloud.oauth2.service.granter.CustomMobileSmsTokenGranter;
import com.iwindplus.cloud.oauth2.service.granter.CustomUsernamePasswordTokenGranter;
import com.iwindplus.cloud.oauth2.token.CustomTokenEnhancer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.ApprovalStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 认证服务端配置.
 *
 * @author zengdegui
 * @since 2020/3/24
 */
@EnableAuthorizationServer
public class Oauth2ServerConfig extends AuthorizationServerConfigurerAdapter {
	@Autowired
	private RedisConnectionFactory connectionFactory;

	@Autowired
	private CustomClientDetailsService clientDetailsService;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;

	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private Oauth2Property oauth2Property;

	/**
	 * 授权store.
	 *
	 * @return ApprovalStore
	 */
	@Primary
	@Bean
	public ApprovalStore approvalStore() {
		TokenApprovalStore approvalStore = new TokenApprovalStore();
		approvalStore.setTokenStore(this.tokenStore);
		return approvalStore;
	}

	/**
	 * 令牌信息拓展.
	 *
	 * @return TokenEnhancer
	 */
	@Primary
	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	/**
	 * 配置授权码模式授权码服务,不配置默认为内存模式.
	 *
	 * @return AuthorizationCodeServices
	 */
	@Primary
	@Bean
	public AuthorizationCodeServices authorizationCodeServices() {
		RedisAuthorizationCodeServices redisAuthorizationCodeServices = new RedisAuthorizationCodeServices(this.connectionFactory);
		return redisAuthorizationCodeServices;
	}

	/**
	 * 授权码模式授权信息.
	 *
	 * @return UserApprovalHandler
	 */
	@Primary
	@Bean
	public UserApprovalHandler userApprovalHandler() {
		ApprovalStoreUserApprovalHandler userApprovalHandler = new ApprovalStoreUserApprovalHandler();
		userApprovalHandler.setApprovalStore(this.approvalStore());
		userApprovalHandler.setClientDetailsService(this.clientDetailsService);
		userApprovalHandler.setRequestFactory(new DefaultOAuth2RequestFactory(this.clientDetailsService));
		return userApprovalHandler;
	}

	/**
	 * 异常捕获.
	 *
	 * @return CustomWebResponseExceptionTranslator
	 */
	@Primary
	@Bean
	public CustomWebResponseExceptionTranslator customWebResponseExceptionTranslator() {
		return new CustomWebResponseExceptionTranslator();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.withClientDetails(this.clientDetailsService);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints
				// refresh_token有两种使用方式：重复使用(true)、非重复使用(false)，默认为true
				// 1.重复使用：access_token过期刷新时， refresh token过期时间未改变，仍以初次生成的时间为准
				// 2.非重复使用：access_token过期刷新时， refresh_token过期时间延续，在refresh_token有效期内刷新而无需失效再次登录
				.reuseRefreshTokens(this.oauth2Property.getReuseRefreshToken())
				.tokenStore(this.tokenStore)
				// 授权信息保存策略
				.approvalStore(this.approvalStore())
				// 授权码模式数据来源
				.authorizationCodeServices(this.authorizationCodeServices())
				.userApprovalHandler(this.userApprovalHandler())
				.tokenEnhancer(this.tokenEnhancer())
				.userDetailsService(this.userDetailsService)
				// 自定义异常转换类
				.exceptionTranslator(this.customWebResponseExceptionTranslator());
		endpoints.setClientDetailsService(this.clientDetailsService);
		List<TokenGranter> tokenGranters = getTokenGranters(
				endpoints.getAuthorizationCodeServices(),
				endpoints.getTokenServices(),
				endpoints.getClientDetailsService(),
				endpoints.getOAuth2RequestFactory());
		endpoints.tokenGranter(new CompositeTokenGranter(tokenGranters));
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security
				.tokenKeyAccess("isAuthenticated()")
				.checkTokenAccess("isAuthenticated()")
				.allowFormAuthenticationForClients()
				.accessDeniedHandler(this.customAccessDeniedHandler)
				.authenticationEntryPoint(this.customAuthenticationEntryPoint);
	}

	/**
	 * 获取token的模式（密码模式，短信验证码，第三方授权等）.
	 *
	 * @param authorizationCodeServices authorizationCodeServices
	 * @param tokenServices             tokenServices
	 * @param clientDetailsService      clientDetailsService
	 * @param requestFactory            requestFactory
	 * @return List<TokenGranter>
	 */
	private List<TokenGranter> getTokenGranters(
			AuthorizationCodeServices authorizationCodeServices,
			AuthorizationServerTokenServices tokenServices,
			ClientDetailsService clientDetailsService,
			OAuth2RequestFactory requestFactory) {
		return new ArrayList<>(Arrays.asList(
				new AuthorizationCodeTokenGranter(tokenServices, authorizationCodeServices, clientDetailsService,
						requestFactory),
				new ClientCredentialsTokenGranter(tokenServices, clientDetailsService, requestFactory),
				new CustomUsernamePasswordTokenGranter(this.userDetailsService, tokenServices, clientDetailsService,
						requestFactory),
				new ImplicitTokenGranter(tokenServices, clientDetailsService, requestFactory),
				new RefreshTokenGranter(tokenServices, clientDetailsService, requestFactory),
				new CustomMobileSmsTokenGranter(this.userDetailsService, tokenServices, clientDetailsService,
						requestFactory),
				new CustomBindGrantTokenGranter(this.userDetailsService, tokenServices, clientDetailsService,
						requestFactory)));
	}
}
