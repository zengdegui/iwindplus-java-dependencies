/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2;

import com.iwindplus.cloud.oauth2.domain.Oauth2Property;
import com.iwindplus.cloud.oauth2.exception.CustomAccessDeniedHandler;
import com.iwindplus.cloud.oauth2.exception.CustomAuthenticationEntryPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * oauth2客户端资源配置.
 *
 * @author zengdegui
 * @since 2020年4月1日
 */
@Slf4j
public class Oauth2ClientResourceConfig extends ResourceServerConfigurerAdapter {
	@Autowired
	private RedisConnectionFactory connectionFactory;

	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;

	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

	@Autowired
	private Oauth2Property oauth2Property;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private DefaultTokenServices tokenServices;

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenStore(this.tokenStore).tokenServices(this.tokenServices)
				.authenticationEntryPoint(this.customAuthenticationEntryPoint)
				.accessDeniedHandler(this.customAccessDeniedHandler);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and().authorizeRequests()
				// 监控端点内部放行
				.requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
				// feign访问或无需身份认证
				.antMatchers(this.oauth2Property.getWhiteList()).permitAll().anyRequest().authenticated().and()
				// 认证鉴权错误处理,为了统一异常处理。每个资源服务器都应该加上。
				.exceptionHandling().accessDeniedHandler(this.customAccessDeniedHandler)
				.authenticationEntryPoint(this.customAuthenticationEntryPoint).and().csrf().disable();
	}
}
