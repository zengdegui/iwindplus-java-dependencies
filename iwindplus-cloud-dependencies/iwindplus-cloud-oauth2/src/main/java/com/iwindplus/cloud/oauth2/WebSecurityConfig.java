/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2;

import com.iwindplus.cloud.oauth2.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Web服务配置类.
 *
 * @author zengdegui
 * @since 2020/3/25
 */
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * 密码模式下必须注入的bean authenticationManagerBean 认证是由.
	 * AuthenticationManager 来管理的，
	 * 但是真正进行认证的是 AuthenticationManager
	 * 中定义的AuthenticationProvider。
	 * AuthenticationManager 中可以定义有多个
	 * AuthenticationProvider
	 *
	 * @return AuthenticationManager
	 * @throws Exception
	 */
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(this.authenticationProvider())
				.parentAuthenticationManager(this.authenticationManagerBean());
	}

	/**
	 * 创建DaoAuthenticationProvider.
	 *
	 * @return DaoAuthenticationProvider
	 */
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setHideUserNotFoundExceptions(false);
		provider.setUserDetailsService(this.userDetailsService);
		provider.setPasswordEncoder(this.passwordEncoder);
		return provider;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// 解决静态资源被拦截的问题
		web.ignoring()
				.antMatchers("/error", "/static/**", "/v2/api-docs/**", "/swagger-resources/**", "/webjars/**",
						"/favicon.ico");
	}
}
