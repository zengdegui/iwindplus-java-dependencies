/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service.granter;

import com.iwindplus.cloud.oauth2.domain.constant.Oauth2Constant;
import com.iwindplus.cloud.oauth2.domain.vo.UserDetailsVO;
import com.iwindplus.cloud.oauth2.service.CustomUserDetailsService;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.Map;

/**
 * 自定义绑定授权登录方式.
 *
 * @author zengdegui
 * @since 2020/5/8
 */
public class CustomBindGrantTokenGranter extends AbstractCustomTokenGranter {
	private CustomUserDetailsService userDetailsService;

	public CustomBindGrantTokenGranter(CustomUserDetailsService userDetailsService,
			AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService,
			OAuth2RequestFactory requestFactory) {
		super(tokenServices, clientDetailsService, requestFactory, Oauth2Constant.BIND_GRANT);
		this.userDetailsService = userDetailsService;
	}

	@Override
	protected UserDetailsVO getCustomUser(Map<String, String> parameters) {
		String skey = parameters.get(Oauth2Constant.SKEY);
		String clientId = parameters.get(Oauth2Constant.CLIENT_ID);
		return this.userDetailsService.loadUserBySkey(clientId, skey);
	}
}
