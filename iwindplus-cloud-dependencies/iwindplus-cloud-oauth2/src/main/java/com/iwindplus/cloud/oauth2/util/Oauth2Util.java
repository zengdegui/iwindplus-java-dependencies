/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.iwindplus.boot.web.domain.enumerate.WebCodeEnum;
import com.iwindplus.boot.web.exception.BaseException;
import com.iwindplus.cloud.oauth2.domain.vo.UserDetailsVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;

import java.util.Map;
import java.util.Objects;

/**
 * 认证信息帮助类.
 *
 * @author zengdegui
 * @since 2020/4/1
 */
@Slf4j
public class Oauth2Util {
    /**
     * 根据访问token获取用户信息
     *
     * @param tokenStore  token存储
     * @param accessToken 访问token
     * @return UserDetailsVO
     */
    @SuppressWarnings({"rawtypes"})
    public static UserDetailsVO getUserByToken(TokenStore tokenStore, String accessToken) {
        if (Objects.nonNull(tokenStore) || StringUtils.isBlank(accessToken)) {
            return null;
        }
        OAuth2Authentication oAuth2Authentication = tokenStore.readAuthentication(accessToken);
        if (Objects.nonNull(oAuth2Authentication)) {
            Authentication authentication = oAuth2Authentication.getUserAuthentication();
            if (Objects.nonNull(authentication) && authentication.isAuthenticated()) {
                if (authentication.getPrincipal() instanceof UserDetailsVO) {
                    UserDetailsVO userDetails = (UserDetailsVO) authentication.getPrincipal();
                    return userDetails;
                }
                if (authentication.getPrincipal() instanceof Map) {
                    Map principal = (Map) authentication.getPrincipal();
                    UserDetailsVO userDetails = BeanUtil.mapToBean(principal, UserDetailsVO.class, false,
                            CopyOptions.create().setIgnoreError(false));
                    return userDetails;
                }
            }
        }
        throw new BaseException(WebCodeEnum.NOT_LOGIN);
    }

    /**
     * 获取认证用户信息.
     *
     * @return UserDetailsVO
     */
    @SuppressWarnings({"rawtypes"})
    public static UserDetailsVO getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.nonNull(authentication) && authentication.isAuthenticated()) {
            if (authentication.getPrincipal() instanceof UserDetailsVO) {
                UserDetailsVO userDetails = (UserDetailsVO) authentication.getPrincipal();
                return userDetails;
            }
            if (authentication.getPrincipal() instanceof Map) {
                Map principal = (Map) authentication.getPrincipal();
                UserDetailsVO userDetails = BeanUtil.mapToBean(principal, UserDetailsVO.class, false,
                        CopyOptions.create().setIgnoreError(false));
                return userDetails;
            }
        }
        return null;
    }

    /**
     * 获取用户主键
     *
     * @return String
     */
    public static String getUserId() {
        UserDetailsVO data = getUser();
        if (Objects.nonNull(data)) {
            return data.getOpenid();
        }
        return null;
    }

    /**
     * 获取应用主键
     *
     * @return String
     */
    public static String getAppId() {
        UserDetailsVO data = getUser();
        if (Objects.nonNull(data)) {
            return data.getClientId();
        }
        return null;
    }

    /**
     * 更新用户信息.
     *
     * @param entity 用户信息
     */
    public static void updateUser(UserDetailsVO entity) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UsernamePasswordAuthenticationToken newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal()
                , auth.getCredentials(), entity.getAuthorities());
        newAuth.setDetails(entity);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }
}
