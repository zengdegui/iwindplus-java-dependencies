/**
 * Copyright (c) iwindplus Technologies Co., Ltd.2011-2020, All rights reserved.
 */

package com.iwindplus.cloud.oauth2.service;

import com.iwindplus.cloud.oauth2.domain.vo.UserDetailsVO;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 获取用户信息接口.
 *
 * @author zengdegui
 * @since 2020/5/8
 */
public interface CustomUserDetailsService extends UserDetailsService {
	/**
	 * 通过用户名密码获取.
	 *
	 * @param clientId 客户端（应用主键）
	 * @param username 用户名
	 * @param password 密码
	 * @return UserDetailsVO
	 */
	UserDetailsVO loadUserByUsernameAndPassword(String clientId, String username, String password);

	/**
	 * 通过手机短信获取.
	 *
	 * @param clientId 客户端（应用主键）
	 * @param mobile   手机
	 * @param captcha  验证码
	 * @return UserDetailsVO
	 */
	UserDetailsVO loadUserByMobileAndCaptcha(String clientId, String mobile, String captcha);

	/**
	 * 通过绑定key获取.
	 *
	 * @param clientId 客户端（应用主键）
	 * @param skey     绑定key
	 * @return UserDetailsVO
	 */
	UserDetailsVO loadUserBySkey(String clientId, String skey);
}
